# BoxxLibrary

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

## Generate Library
Run `ng g lib <library-name> [--prefix=<component's-prefix>]`

## Generate Component inside library

Run `ng g c components/<component-name> --project=<library-name>`

## Compile library

Run `ng build <library-name> [--prod]`