import { Component, Input, OnInit } from '@angular/core';
import { ContactModel } from '../../models/contact.model';

@Component({
  selector: 'boxx-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss'],
})
export class ContactItemComponent implements OnInit {
    @Input() showDetail = true;
    @Input() contact: ContactModel;
    @Input() showEmail = false;

  constructor() { }

  ngOnInit() {}

}
