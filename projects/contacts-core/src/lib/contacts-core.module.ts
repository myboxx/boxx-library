import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { ContactsRepository } from './repositories/contacts.repository';
import { CONTACTS_REPOSITORY } from './repositories/IContact.repository';
import { ContactsService } from './services/contacts.service';
import { CONTACTS_SERVICE } from './services/IContact.service';
import { ContactsEffects } from './state/contact.effects';
import { contactsReducer } from './state/contact.reducer';
import { ContactStore } from './state/contact.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
  declarations: [
    ContactItemComponent
  ],
  imports: [
    HttpClientModule,
    StoreModule.forFeature('contacts', contactsReducer),
    EffectsModule.forFeature([ContactsEffects]),
    CommonModule,
    IonicModule
  ],
  exports: [
    ContactItemComponent
  ]
})
export class ContactsCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<ContactsCoreModule> {

        return {
            ngModule: ContactsCoreModule,
            providers: [
                { provide: CONTACTS_SERVICE, useClass: ContactsService },
                { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository },
                ...config.providers,
                Contacts,
                ContactStore
            ]
        };
    }
}
