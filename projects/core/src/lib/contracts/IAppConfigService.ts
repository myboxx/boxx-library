import { InjectionToken } from '@angular/core';

export abstract class AbstractAppConfigService {
    constructor() {}

    protected instanceName: string;
    protected apiUrl: string;

    instance(): string {
        return this.instanceName;
    }
    baseUrl(){
        return this.apiUrl;
    }
}

export const APP_CONFIG_SERVICE = new InjectionToken<AbstractAppConfigService>('appConfigService');
