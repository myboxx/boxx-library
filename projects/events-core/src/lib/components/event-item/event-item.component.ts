import { Component, Input, OnInit } from '@angular/core';
import { EventModel } from '../../models/events.model';


@Component({
  selector: 'boxx-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss']
})
export class EventItemComponent implements OnInit {
    @Input() showDetail = true;
    @Input() event: EventModel;
    @Input() eventDatetimeFrom: string;
    @Input() maxAttendeeToshow = 5;

    constructor() { }

    ngOnInit() { }

}
