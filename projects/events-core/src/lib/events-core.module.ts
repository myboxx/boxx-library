import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { EventItemComponent } from './components/event-item/event-item.component';
import { EventsRepository } from './repositories/events.repository';
import { EVENTS_REPOSITORY } from './repositories/IEvents.repository';
import { EventsService } from './services/events.service';
import { EVENTS_SERVICE } from './services/IEvents.service';
import { EventsEffects } from './state/events.effects';
import { eventsReducer } from './state/events.reducer';
import { EventsStore } from './state/events.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
    declarations: [EventItemComponent],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('events', eventsReducer),
        EffectsModule.forFeature([EventsEffects]),
        TranslateModule.forChild(),
        CommonModule,
        FormsModule,
        IonicModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        EventItemComponent
    ]
})
export class EventsCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<EventsCoreModule> {
        return {
            ngModule: EventsCoreModule,
            providers: [
                { provide: EVENTS_SERVICE, useClass: EventsService },
                { provide: EVENTS_REPOSITORY, useClass: EventsRepository },
                ...config.providers,
                EventsStore
            ]
        };
    }
}
