import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
import { IEventsApiProps, IEventsCommonProps, IEventsRepository } from './IEvents.repository';

@Injectable()
export class EventsRepository implements IEventsRepository {

    constructor(
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient,
    ) { }

    getEvents(): Observable<IHttpBasicResponse<IEventsApiProps[]>> {
        return this.httpClient.get<IHttpBasicResponse<IEventsApiProps[]>>(`${this.getBaseUrl()}`);
    }

    createEvent(payload: IEventsCommonProps): Observable<IHttpBasicResponse<IEventsApiProps>> {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                    params = params.append(key, payload[key]);
            }
        }

        const body = params.toString();

        return this.httpClient.post<IHttpBasicResponse<IEventsApiProps>>(`${this.getBaseUrl()}/create`, body);
    }

    updateEvent(id: number, payload: IEventsCommonProps): Observable<IHttpBasicResponse<IEventsApiProps>> {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                    params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();

        return this.httpClient.post<IHttpBasicResponse<IEventsApiProps>>(`${this.getBaseUrl()}/update/${id}`, body);
    }

    deleteEvent(id: any): Observable<IHttpBasicResponse<null>> {
        return this.httpClient.delete<IHttpBasicResponse<null>>(`${this.getBaseUrl()}/delete/${id}`);
    }

    private getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/schedule`;
    }
}
