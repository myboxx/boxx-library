import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { EventModel } from '../models/events.model';
import { IEventsCommonProps } from '../repositories/IEvents.repository';


export interface IEventsService<T1> {
    getEvents(): Observable<T1[]>;
    createEvent(payload: IEventsCommonProps): Observable<T1>;
    updateEvent(id: number, payload: any): Observable<T1>;
    deleteEvent(payload: number): Observable<boolean>;
}

export const EVENTS_SERVICE = new InjectionToken<IEventsService<EventModel>>('eventsService');
