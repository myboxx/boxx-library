import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LOGIN_REPOSITORY } from './repositories/ILogin.repository';
import { LoginRepository } from './repositories/login.repository';
import { LOGIN_SERVICE } from './services/ILogin.service';
import { LoginService } from './services/login.service';
import { LoginEffects } from './state/login.effects';
import { loginReducer } from './state/login.reducer';
import { LoginStore } from './state/login.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('login', loginReducer),
        EffectsModule.forFeature([LoginEffects]),
    ],
    exports: []
})
export class LoginCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<LoginCoreModule> {

        return {
            ngModule: LoginCoreModule,
            providers: [
                { provide: LOGIN_SERVICE, useClass: LoginService },
                { provide: LOGIN_REPOSITORY, useClass: LoginRepository },
                ...config.providers,
                LoginStore
            ]
        };
    }
 }
