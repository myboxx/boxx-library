import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
    ILoginDataResponse,
    ILoginForm,
    ILoginRepository,
    ILogSessionPayload,
    ISaveTokenDataResponse,
    ISocialLoginDataResponse,
    ISocialLoginForm
} from './ILogin.repository';
import { IHttpBasicResponse, APP_CONFIG_SERVICE, AbstractAppConfigService } from '@boxx/core';

@Injectable()
export class LoginRepository implements ILoginRepository {
    readonly REFRESH_TOKEN_URL: string = '/api/v1/auth/refresh_token';

    constructor(
        private http: HttpClient,
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService
    ) { }

    login(credentials: ILoginForm | ISocialLoginForm): Observable<IHttpBasicResponse<ILoginDataResponse>> {

        let params = new HttpParams();

        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }

        const body = params.toString();

        return this.http.post<IHttpBasicResponse<ILoginDataResponse>>(`${this.appSettings.baseUrl()}/api/v2/auth/login`, body);
    }

    socialLogin(credentials: ISocialLoginForm): Observable<IHttpBasicResponse<ISocialLoginDataResponse>> {

        let params = new HttpParams();

        for (const property in credentials) {
            if (credentials.hasOwnProperty(property)) {
                params = params.append(property, credentials[property]);
            }
        }

        const body = params.toString();

        return this.http.post<IHttpBasicResponse<ISocialLoginDataResponse>>(`${this.appSettings.baseUrl()}/api/v2/auth/add_social`, body);
    }

    saveToken(deviceId: string, fcmRegistrationId: string): Observable<IHttpBasicResponse<ISaveTokenDataResponse>> {
        const data = { device_id: deviceId, fcm_registration_id: fcmRegistrationId };

        let params = new HttpParams();

        for (const property in data) {
            if (data.hasOwnProperty(property)) {
                params = params.append(property, data[property]);
            }
        }

        const body = params.toString();

        return this.http.post<any>(`${this.getBaseUrl()}/fcm/devices`, body);
    }

    logSessionStart(payload: ILogSessionPayload) {

        const params = new HttpParams()
            .append('action_type', payload.action_type);
        // .append('action_desciption', payload.action_description || 'NO_DESCRIPTION_SUPPLIED');


        const urlSearchParams = new URLSearchParams();
        urlSearchParams.append('action_type', payload.action_type);

        const body = params.toString();

        return this.http.post<IHttpBasicResponse<any>>(
            `${this.appSettings.baseUrl()}/api/v1/profileUser/check_app_login`, body
        );
    }

    private getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1`;
    }
}
