import { createAction, props } from '@ngrx/store';
import { UserModel } from '../models/user.model';
import { ILoginForm, ISocialLoginForm } from '../repositories/ILogin.repository';

export enum LoginActionTypes {
    LoginBegin = '[Login] Login begin',
    LoginSuccess = '[Login] Login success',
    LoginFail = '[Login] Login failure',

    SocialLoginBegin = '[Login] Social Login begin',
    SocialLoginSuccess = '[Login] Social Login success',
    SocialLoginFail = '[Login] Social Login failure',

    LogoutBegin = '[Login] Logout begin',
    LogoutSuccess = '[Login] Logout success',
    LogoutFail = '[Login] Logout failure',

    SetUserData = '[Login] Set User Data',
}

// LOGIN...
export const LoginBeginAction = createAction(
    LoginActionTypes.LoginBegin,
    props<{ credentials: ILoginForm | ISocialLoginForm }>()
);

export const SocialLoginBeginAction = createAction(
    LoginActionTypes.SocialLoginBegin,
    props<{ credentials: ISocialLoginForm }>()
);

export const LoginSuccessAction = createAction(
    LoginActionTypes.LoginSuccess,
    props<{ user: UserModel }>()
);

export const SocialLoginSuccessAction = createAction(
    LoginActionTypes.SocialLoginSuccess,
    props<{ user: UserModel }>()
);

export const LoginFailAction = createAction(
    LoginActionTypes.LoginFail,
    props<{ errors: any }>()
);

export const SocialLoginFailAction = createAction(
    LoginActionTypes.SocialLoginFail,
    props<{ errors: any }>()
);

// LOGOUT...
export const LogoutBeginAction = createAction(
    LoginActionTypes.LogoutBegin
);

export const LogoutSuccessAction = createAction(
    LoginActionTypes.LogoutSuccess
);

export const LogoutFailAction = createAction(
    LoginActionTypes.LogoutFail,
    props<{ errors: any }>()
);

export const SetUserData = createAction(
    LoginActionTypes.SetUserData,
    props<{ user: UserModel }>()
);
