import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-message-item',
    templateUrl: './message-item.component.html',
    styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit {
    @Input() detail = true;

    constructor() { }

    ngOnInit(): void {
    }

}
