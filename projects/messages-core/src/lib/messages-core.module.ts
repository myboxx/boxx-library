import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { MESSAGES_REPOSITORY } from './repositories/IMessages.repository';
import { MessagesRepository } from './repositories/messages.repository';
import { MESSAGES_SERVICE } from './services/IMessages.service';
import { MessagesService } from './services/messages.service';
import { MessagesEffects } from './state/messages.effects';
import { messagesReducer } from './state/messages.reducer';
import { MessagesStore } from './state/messages.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
    declarations: [MessageItemComponent],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('messages', messagesReducer),
        EffectsModule.forFeature([MessagesEffects]),
        CommonModule,
        FormsModule,
        IonicModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        MessageItemComponent,
    ]
})
export class MessagesCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<MessagesCoreModule> {
        return {
            ngModule: MessagesCoreModule,
            providers: [
                { provide: MESSAGES_SERVICE, useClass: MessagesService },
                { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository},
                ...config.providers,
                MessagesStore
            ]
        };
    }
 }
