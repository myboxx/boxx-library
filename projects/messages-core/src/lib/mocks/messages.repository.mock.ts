import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IHttpBasicResponse } from '@boxx/core';
import { IMessagesRepository } from '../repositories/IMessages.repository';
import { MOCK_REVIEWS_API_LIST, TEST_MESSAGES_API_LIST } from './messages.data';
import { ILeadApi, ILeadsApi } from '../models/Leads-api.properties';
import { IReviewApi, IReviewsApi, IReviewUpsertApi } from '../models/Reviews-api.properties';

@Injectable()
export class MockMessagesRepository implements IMessagesRepository {

    constructor(){}

    readonly responseError: IHttpBasicResponse<null> = {
        status: 'error',
        message: 'Some bad error!',
        statusCode: 500
    };

    getLeads(): Observable<IHttpBasicResponse<ILeadsApi>> {
        const data: ILeadsApi = {
            messages: TEST_MESSAGES_API_LIST as Array<ILeadApi>,
            periods: null,
            totals: {
                total: 0,
                seen: 0,
                new: 0,
            },
            chart: []
        };

        const responseOk: IHttpBasicResponse<ILeadsApi> = {
            data,
            status: 'success'
        };

        return of(responseOk);
    }
    setLeadAsRead(id: number): Observable<IHttpBasicResponse<ILeadApi>> {
        const data = TEST_MESSAGES_API_LIST[0] as ILeadApi;
        data.read_status = '1';

        const responseOk: IHttpBasicResponse<ILeadApi> = {
            data,
            status: 'success'
        };

        return of(responseOk);
    }

    getReviews(){
        const data = MOCK_REVIEWS_API_LIST as IReviewsApi;

        const responseOk: IHttpBasicResponse<IReviewsApi> = {
            data,
            status: 'success'
        };

        return of(responseOk);
    }

    getReview(){
        const data = MOCK_REVIEWS_API_LIST.reviews[0] as IReviewApi;

        const responseOk: IHttpBasicResponse<IReviewApi> = {
            data,
            status: 'success'
        };

        return of(responseOk);
    }

    upsertReviewReply(id, comment){
        const data = {
            comment,
            updateTime: new Date().toUTCString()
        } as IReviewUpsertApi;

        const responseOk: IHttpBasicResponse<IReviewUpsertApi> = {
            data,
            status: 'success'
        };

        return of(responseOk);
    }

    deleteReviewReply(id){
        const responseOk: IHttpBasicResponse<null> = {
            data: null,
            status: 'success'
        };

        return of(responseOk);
    }
}
