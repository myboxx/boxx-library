import { ILeadApi, ILeadsApi, ILeadsTotalsApi, IPeriodsApi } from './Leads-api.properties';
import { IMessageBaseProps, MessageBaseModel, MessageSource } from './Message.model';

export interface LeadsModelProps {
    totals: ILeadsTotalsApi;
    periods: IPeriodsApi;
    leads: LeadModel[];
    chart: number[];
}

export class LeadsModel implements LeadsModelProps {
    constructor(data: LeadsModelProps) {
        this.totals = data.totals;
        this.periods = data.periods;
        this.leads = data.leads;
        this.chart = data.chart;
    }

    totals: ILeadsTotalsApi;
    periods: IPeriodsApi;
    leads: LeadModel[];
    chart: number[];

    static fromApiResponse(data: ILeadsApi) {
        const leads = data.messages.map(lead => LeadModel.fromApiResponse(lead));

        return new LeadsModel({
            totals: data.totals,
            periods: data.periods,
            leads,
            chart: data.chart
        });
    }

    static empty(){
        return new LeadsModel({
            totals: {
                total: 0,
                seen: 0,
                new: 0
            },
            periods: null,
            leads: [],
            chart: []
        });
    }
}

export interface ILeadModelProps extends IMessageBaseProps {
    email: string;
    phone: string;
    formattedDate: string;
    date: string;
    hour: string;
    timeAgo: string;
    leadTimeAgo: string;
    readStatus: boolean;
    shortMessage: string;
    favorite?: boolean;
}

export class LeadModel extends MessageBaseModel implements ILeadModelProps {
    email: string;
    phone: string;
    formattedDate: string;
    date: string;
    hour: string;
    timeAgo: string;
    leadTimeAgo: string;
    readStatus: boolean;
    shortMessage: string;
    favorite?: boolean;

    constructor(data: ILeadModelProps) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });

        this.email = data.email;
        this.phone = data.phone;
        this.formattedDate = data.formattedDate;
        this.date = data.date;
        this.hour = data.hour;
        this.timeAgo = data.timeAgo;
        this.leadTimeAgo = data.leadTimeAgo;
        this.readStatus = data.readStatus;

        const shortMsg = (data.shortMessage || data.message.substring(0, 30));
        this.shortMessage = shortMsg.length >= 30 ? shortMsg.concat('...') : shortMsg;

        this.source = data.source;
    }

    static fromApiResponse(data: ILeadApi): LeadModel {
        return new LeadModel({
            id: +data.id,
            senderName: data.name || data.data_name,
            message: data.message || data.data_message,
            email: data.mail || data.data_email,
            phone: data.phone || data.data_phone,
            createTime: data.timestamp,
            formattedDate: data.formatted_date,
            date: data.date || data.created_at,
            hour: data.hour,
            timeAgo: data.time_ago,
            leadTimeAgo: data.lead_time_ago,
            readStatus: data.read_status === '1',
            shortMessage: data.short_message,
            source: MessageSource.EMAIL
        });
    }

    static empty(): LeadModel {
        return new LeadModel({
            id: null,
            senderName: '',
            createTime: null,
            message: '',
            email: null,
            phone: null,
            formattedDate: null,
            date: null,
            hour: null,
            timeAgo: null,
            leadTimeAgo: null,
            readStatus: null,
            shortMessage: null,
            source: MessageSource.UNKNOWN,
            favorite: false
        });
    }
}
