import { MessageBaseModel } from './Message.model';

export interface IMessagesPageProps {
    stats: {
        leads: {
            error?: any;
            total: number
            read: number
            new: number
        },
        reviews: {
            error?: any;
            averageRating: number;
            totalReviewCount: number;
        }
    };
    messages: MessageBaseModel[];
}

export class MessagesPageModel implements IMessagesPageProps {
    stats: {
        leads: {
            error?: any;
            total: number
            read: number
            new: number
        },
        reviews: {
            error?: any;
            averageRating: number;
            totalReviewCount: number;
        }
    };
    messages: MessageBaseModel[];

    constructor(data: IMessagesPageProps) {
        this.stats = data.stats;
        this.messages = data.messages;
    }

    static empty() {
        return new MessagesPageModel({
            stats: {
                leads: {
                    total: 0,
                    read: 0,
                    new: 0
                },
                reviews: {
                    averageRating: 0,
                    totalReviewCount: 0
                }
            },
            messages: [],
        });
    }
}

