import { IMessageBaseProps, MessageBaseModel, MessageSource } from './Message.model';
import { IReviewApi, IReviewReplyApi, IReviewsApi } from './Reviews-api.properties';

export interface IReviewsModelProps {
    averageRating: number;
    reviews: ReviewModel[];
    totalReviewCount: number;
}

export class ReviewsModel implements IReviewsModelProps {
    constructor(data: IReviewsModelProps) {
        this.averageRating = data.averageRating;
        this.reviews = data.reviews;
        this.totalReviewCount = data.totalReviewCount;
    }

    averageRating: number;
    reviews: ReviewModel[];
    totalReviewCount: number;

    static fromApiResponse(data: IReviewsApi) {
        const reviewList: ReviewModel[] = (!Array.isArray(data.reviews) ? [] : data.reviews).map(
            review => ReviewModel.fromApiResponse({
                id: review.id,
                comment: review.comment,
                createTime: review.createTime,
                reviewer: review.reviewer,
                starRating: review.starRating,
                updateTime: review.updateTime,
                reviewReply: review.reviewReply
            })
        );

        return new ReviewsModel({
            averageRating: data.averageRating || 0,
            reviews: reviewList,
            totalReviewCount: data.totalReviewCount || 0
        });
    }

    empty() {
        return new ReviewsModel({
            averageRating: null,
            reviews: null,
            totalReviewCount: null
        });
    }
}

export interface ReviewModelProps extends IMessageBaseProps {
    senderPhotoUrl: string;
    starRating: number;
    updateTime: string;
    reviewReply?: IReviewReplyApi;
}

export class ReviewModel extends MessageBaseModel implements ReviewModelProps {
    constructor(data: ReviewModelProps) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });

        this.senderPhotoUrl = data.senderPhotoUrl;
        this.starRating = data.starRating;
        this.updateTime = data.updateTime;
        this.reviewReply = data.reviewReply;
    }

    senderPhotoUrl: string;
    starRating: number;
    updateTime: string;
    reviewReply?: IReviewReplyApi;

    static fromApiResponse(data: IReviewApi): ReviewModel {
        return new ReviewModel({
            // base class properties:
            id: data.id,
            senderName: data.reviewer.displayName,
            message: data.comment,
            createTime: data.createTime,
            source: MessageSource.GMB_REVIEW,

            // own (extended) properties:
            senderPhotoUrl: data.reviewer.profilePhotoUrl,
            starRating: data.starRating,
            updateTime: data.updateTime,
            reviewReply: data.reviewReply
        });
    }
}
