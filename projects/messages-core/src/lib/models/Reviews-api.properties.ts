
export interface IReviewsApi {
    averageRating: number;
    reviews: IReviewApi[];
    totalReviewCount: number;
}

export interface IReviewApi {
    id: string;
    comment?: string; // <-- will be treated as message in Base class
    createTime: string;
    reviewer: IReviewerApi; // will be treated as 2 separated fields: senderName & senderPhotoUrl in Base subclass
    reviewReply?: IReviewReplyApi;
    starRating: number;
    updateTime: string;
}

export interface IReviewUpsertApi {
    comment: string;
    updateTime: string;
}

export interface IReviewerApi {
    displayName: string;  // <-- will be treated as senderName in Base class
    profilePhotoUrl: string; // <-- will be treated as senderPhotoUrl in Base subclass
}

export interface IReviewReplyApi {
    comment: string;
    updateTime: string;
}
