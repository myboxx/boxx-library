
/**
 * For any type of message:
 * Lead and Review by now
 */
export interface IMessageBaseProps {
    id: number | string;
    senderName: string;
    message: string;
    createTime: string;
    source: MessageSource;
}

/**
 * Superclass for any type of message:
 * Lead and Review by now
 */
export class MessageBaseModel implements IMessageBaseProps {
    constructor(data: IMessageBaseProps) {
        this.id = data.id;
        this.senderName = data.senderName;
        this.message = data.message;
        this.createTime = data.createTime;
        this.source = data.source;
    }

    id: number | string;
    senderName: string;
    message: string;
    createTime: string;
    source: MessageSource;
}


export type MESSAGE_TYPE = 'ALL' | 'READ' | 'NEW';

export enum MessageSource {
    EMAIL,
    GMB_MESSAGES,
    GMB_REVIEW,
    CHAT_WEBSITE,
    UNKNOWN
}
