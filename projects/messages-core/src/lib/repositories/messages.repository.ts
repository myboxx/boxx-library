import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
import { Observable, of } from 'rxjs';
import { ILeadApi, ILeadsApi } from '../models/Leads-api.properties';
import { IReviewApi, IReviewsApi, IReviewUpsertApi } from '../models/Reviews-api.properties';
import { IMessagesRepository } from './IMessages.repository';

@Injectable()
export class MessagesRepository implements IMessagesRepository {

    constructor(
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient,
    ) { }

    getLeads(): Observable<IHttpBasicResponse<ILeadsApi>> {
        return this.httpClient.get<IHttpBasicResponse<ILeadsApi>>(`${this.getBaseUrl()}/leads`);
    }

    setLeadAsRead(id: number): Observable<IHttpBasicResponse<ILeadApi>> {
        const data = { lead_id: id };

        const urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach((key: string, i: number) => {
            urlSearchParams.append(key, data[key]);
        });
        const body = urlSearchParams.toString();

        return this.httpClient.post<IHttpBasicResponse<ILeadApi>>(`${this.getBaseUrl()}/leads/mark_message_as_read`, body);
    }

    getReviews(): Observable<IHttpBasicResponse<IReviewsApi>> {
        return this.httpClient.get<IHttpBasicResponse<IReviewsApi>>(`${this.getBaseUrl()}/gmb/reviews`);
    }

    getReview(reviewId: string){
        return this.httpClient.get<IHttpBasicResponse<IReviewApi>>(
            `${this.getBaseUrl()}/gmb/review/${reviewId}`
        );
    }

    upsertReviewReply(reviewId: string, comment: string){

        let params = new HttpParams();
        params = params.append('comment', comment);

        return this.httpClient.post<IHttpBasicResponse<IReviewUpsertApi>>(
            `${this.getBaseUrl()}/gmb/upsertReview/${reviewId}`, params.toString()
        );
    }

    deleteReviewReply(reviewId: string){
        return this.httpClient.delete<IHttpBasicResponse<null>>(
            `${this.getBaseUrl()}/gmb/deleteReview/${reviewId}`
        );
    }

    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1`;
    }
}
