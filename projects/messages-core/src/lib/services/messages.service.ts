import { Inject, Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { LeadModel, LeadsModel } from '../models/Lead.model';
import { MessagesPageModel } from '../models/MessagePage.model';
import { ReviewModel, ReviewsModel } from '../models/Review.model';
import { IMessagesRepository, MESSAGES_REPOSITORY } from '../repositories/IMessages.repository';
import { IMessagesService } from './IMessages.service';

@Injectable({
    providedIn: 'root'
})
export class MessagesService implements IMessagesService {
    constructor(
        @Inject(MESSAGES_REPOSITORY) private repository: IMessagesRepository
    ) { }

    getMessages(sorting?: 'ASC' | 'DESC') {
        let leadsError;
        let ReviewsError;
        return forkJoin([
            this.getLeads().pipe(catchError(err => {leadsError = err; return of(null); })),
            this.getReviews().pipe(catchError(err => {ReviewsError = err; return of(null); })),
        ]).pipe(
            map(([leadsData, reviewsData]) => {

                return new MessagesPageModel({
                    stats: {
                        leads: {
                            error: leadsError,
                            total: leadsError ? 0 : leadsData.totals.total,
                            read: leadsError ? 0 : leadsData.totals.seen,
                            new: leadsError ? 0 : leadsData.totals.new
                        },
                        reviews: {
                            error: ReviewsError,
                            averageRating: ReviewsError ? 0 : reviewsData.averageRating,
                            totalReviewCount: ReviewsError ? 0 : reviewsData.totalReviewCount
                        }
                    },
                    messages: [
                        ...(leadsError ? [] : [...leadsData.leads]),
                        ...(ReviewsError ? [] : [...reviewsData.reviews])
                    ].sort((a, b) => {
                        return (sorting === 'ASC' ?
                            b.createTime.localeCompare(a.createTime) :
                            a.createTime.localeCompare(b.createTime));
                    }),
                });
            })
        );
    }

    getLeads(): Observable<LeadsModel> {
        return this.repository.getLeads().pipe(
            map(response => {
                return LeadsModel.fromApiResponse(response.data);
            })
        );
    }

    setLeadAsRead(id: number): Observable<LeadModel> {
        return this.repository.setLeadAsRead(id).pipe(
            map(response => {
                return LeadModel.fromApiResponse(response.data);
            })
        );
    }

    getReviews(): Observable<ReviewsModel> {
        return this.repository.getReviews().pipe(
            map(response => {
                return ReviewsModel.fromApiResponse(response.data);
            })
        );
    }

    getReview(reviewId: string) {
        return this.repository.getReview(reviewId).pipe(
            map(response => {
                return ReviewModel.fromApiResponse(response.data);
            })
        );
    }

    upsertReviewReply(review: ReviewModel, comment: string) {
        return this.repository.upsertReviewReply(review.id as string, comment).pipe(
            map(response => {
                return new ReviewModel({
                    id: review.id,
                    senderName: review.senderName,
                    message: review.message,
                    createTime: review.createTime,
                    source: review.source,

                    senderPhotoUrl: review.senderPhotoUrl,
                    starRating: review.starRating,
                    updateTime: review.updateTime,
                    reviewReply: {
                        comment,
                        updateTime: response.data.updateTime,
                    }
                });
            })
        );
    }

    deleteReviewReply(review: ReviewModel) {
        return this.repository.deleteReviewReply(review.id as string).pipe(
            map(response => {
                if (response.status === 'success') {
                    return new ReviewModel({
                        id: review.id,
                        senderName: review.senderName,
                        message: review.message,
                        createTime: review.createTime,
                        source: review.source,

                        senderPhotoUrl: review.senderPhotoUrl,
                        starRating: review.starRating,
                        updateTime: review.updateTime
                    });
                }
                else {
                    throw Error(response.message || 'Unknown error');
                }
            })
        );
    }
}
