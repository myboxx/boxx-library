import { createAction, props } from '@ngrx/store';
import { LeadModel } from '../models/Lead.model';
import { MessageBaseModel, MessageSource, MESSAGE_TYPE } from '../models/Message.model';
import { MessagesPageModel } from '../models/MessagePage.model';
import { ReviewModel } from '../models/Review.model';

export enum MessagesActionTypes {
    GetMessagesBegin = '[Messages] Get Messages begin',
    GetMessagesSuccess = '[Messages] Get Messages success',
    GetMessagesFail = '[Messages] Get Messages failure',

    SetLeadAsReadBegin = '[Messages] Set Lead as Read begin',
    SetLeadAsReadSuccess = '[Messages] Set Messages as Read success',
    SetLeadAsReadFail = '[Messages] Set Messages as Read failure',

    UpsertReviewReplyBegin = '[Messages] Upsert Review Reply begin',
    UpsertReviewReplySuccess = '[Messages] Upsert Review Reply success',
    UpsertReviewReplyFail = '[Messages] Upsert Review Reply failure',

    DeleteReviewReplyBegin = '[Messages] Delete Review Reply begin',
    DeleteReviewReplySuccess = '[Messages] Delete Review Reply success',
    DeleteReviewReplyFail = '[Messages] Delete Review Reply failure',

    FilterMessagesByReadStatusBegin = '[Messages] Filter message by Read Status begin',
    FilterMessagesByReadStatusSuccess = '[Messages] Filter message success',

    FilterMessagesBySourceBegin = '[Messages] Filter message by Source begin',
    FilterMessagesSourceSuccess = '[Messages] Filter message by Source success',

    SortMessagesByDateBegin = '[Messages] Sort message by Date begin',
    SortMessagesByDateSuccess = '[Messages] Sort message by Date success',

    SelectMessage = '[Messages] Select message',
}

// GET Messages from remote API
export const GetMessagesBeginAction = createAction(
    MessagesActionTypes.GetMessagesBegin,
    props<{ sorting?: 'ASC' | 'DESC' }>()
);

export const GetMessagesSuccessAction = createAction(
    MessagesActionTypes.GetMessagesSuccess,
    props<{ data: MessagesPageModel }>()
);

export const GetMessagesFailAction = createAction(
    MessagesActionTypes.GetMessagesFail,
    props<{ errors: any }>()
);

// SET AS READ
export const SetLeadAsReadBeginAction = createAction(
    MessagesActionTypes.SetLeadAsReadBegin,
    props<{ id: number }>()
);

export const SetLeadAsReadSuccessAction = createAction(
    MessagesActionTypes.SetLeadAsReadSuccess,
    props<{ message: LeadModel }>()
);

export const SetLeadAsReadFailAction = createAction(
    MessagesActionTypes.SetLeadAsReadFail,
    props<{ errors: any }>()
);

export const UpsertReviewReplyBeginAction = createAction(
    MessagesActionTypes.UpsertReviewReplyBegin,
    props<{ review: ReviewModel, comment: string }>()
);

export const UpsertReviewReplySuccessAction = createAction(
    MessagesActionTypes.UpsertReviewReplySuccess,
    props<{ review: ReviewModel }>()
);

export const UpsertReviewReplyFaliAction = createAction(
    MessagesActionTypes.UpsertReviewReplyFail,
    props<{ errors: any }>()
);

export const DeleteReviewReplyBeginAction = createAction(
    MessagesActionTypes.DeleteReviewReplyBegin,
    props<{ review: ReviewModel }>()
);

export const DeleteReviewReplySuccessAction = createAction(
    MessagesActionTypes.DeleteReviewReplySuccess,
    props<{ review: ReviewModel }>()
);

export const DeleteReviewReplyFaliAction = createAction(
    MessagesActionTypes.DeleteReviewReplyFail,
    props<{ errors: any }>()
);

// FILTERING
export const FilterMessagesByReadStatusBeginAction = createAction(
    MessagesActionTypes.FilterMessagesByReadStatusBegin,
    props<{ messageType: MESSAGE_TYPE }>()
);

export const FilterMessagesByReadStatusSuccessAction = createAction(
    MessagesActionTypes.FilterMessagesByReadStatusSuccess,
    props<{ messageList: MessageBaseModel[] }>()
);

export const FilterMessagesBySourceBeginAction = createAction(
    MessagesActionTypes.FilterMessagesBySourceBegin,
    props<{ sorting: 'ASC' | 'DESC', sources: MessageSource[] }>()
);

export const FilterMessagesBySourceSuccessAction = createAction(
    MessagesActionTypes.FilterMessagesSourceSuccess,
    props<{
        sorting: 'ASC' | 'DESC',
        sources: MessageSource[],
        filteredMessages: MessageBaseModel[]
    }>()
);


export const SelectMessageAction = createAction(
    MessagesActionTypes.SelectMessage,
    props<{ messageId: number }>()
);
