import { Observable, of, throwError } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { AppState, MessagesEffects } from './messages.effects';
import * as fromReducer from './messages.reducer';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MessagesActionTypes } from './messages.actions';
import { IMessagesService, MESSAGES_SERVICE } from '../services/IMessages.service';
import { MessagesStore } from './messages.store';
import { LeadModel } from '../models/Lead.model';
import { MessagesPageModel } from '../models/MessagePage.model';

class MockMessagesService {
    getMessages() { }

    setLeadAsRead() { }
}

describe('MessagesEffects', () => {
    let actions$ = new Observable<Action>();
    let effects: MessagesEffects;
    let store: MockStore<AppState>;
    let messagesService: IMessagesService;

    const initialState = { messages: {} };

    const errorString = 'some bad error';

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MessagesStore,
                MessagesEffects,
                provideMockActions(() => actions$),
                provideMockStore({ initialState }),
                { provide: MESSAGES_SERVICE, useClass: MockMessagesService }
            ]
        });

        store = TestBed.inject(MockStore);
        effects = TestBed.inject(MessagesEffects);
        messagesService = TestBed.inject(MESSAGES_SERVICE);
    });

    it('Should be created', () => {
        expect(effects).toBeTruthy('MessagesEffects was not created');
    });

    it('load$ should return a GetMessagesSuccess action with the Messages List on success', (done: DoneFn) => {
        const fakeMessagesPageModel = {
            stats: {
                leads: {
                    total: 0,
                    read: 0,
                    new: 0,
                },
                reviews: {
                    averageRating: 0,
                    totalReviewCount: 0
                }
            },
            messages: [LeadModel.empty(), LeadModel.empty(), LeadModel.empty(), LeadModel.empty()],
        };

        const spy = spyOn(messagesService, 'getMessages').and.returnValue(of(fakeMessagesPageModel));

        actions$ = of({ type: MessagesActionTypes.GetMessagesBegin });

        effects.load$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.GetMessagesSuccess);
            expect(((response as any).data as MessagesPageModel).messages.length).toEqual(fakeMessagesPageModel.messages.length);
            expect(spy).toHaveBeenCalledTimes(1);
            done();
        });
    });
    it('load$ should return a GetMessagesFail action with the error object on failure', (done: DoneFn) => {

        const spy = spyOn(messagesService, 'getMessages').and.returnValue(throwError(errorString));

        actions$ = of({ type: MessagesActionTypes.GetMessagesBegin });

        effects.load$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.GetMessagesFail);
            expect((response as any).errors).toBeDefined();
            expect((response as any).errors).toBe(errorString);
            expect(spy).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('setAsRead$ should return a SetLeadAsReadSuccess action with the Message on success', (done: DoneFn) => {

        const spy = spyOn(messagesService, 'setLeadAsRead').and.returnValue(of(LeadModel.empty()));

        actions$ = of({ type: MessagesActionTypes.SetLeadAsReadBegin });

        effects.setAsRead$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.SetLeadAsReadSuccess);
            expect((response as any).message instanceof LeadModel).toBeTruthy();
            expect(spy).toHaveBeenCalledTimes(1);
            done();
        });
    });
    it('setAsRead$ should return a SetLeadAsReadFail action with the error object on failure', (done: DoneFn) => {

        const spy = spyOn(messagesService, 'setLeadAsRead').and.returnValue(throwError(errorString));

        actions$ = of({ type: MessagesActionTypes.SetLeadAsReadBegin });

        effects.setAsRead$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.SetLeadAsReadFail);
            expect((response as any).errors).toBeDefined();
            expect((response as any).errors).toBe(errorString);
            expect(spy).toHaveBeenCalledTimes(1);
            done();
        });
    });

    xit('filter$ should return a FilterMessagesByReadStatusSuccess action with ALL the Messages on success', (done: DoneFn) => {

        /*const expectedFilteredList = [
            { id: 123, readStatus: 0 } as LeadModel,
            { id: 456, readStatus: 1 } as LeadModel,
            { id: 789, readStatus: 1 } as LeadModel
        ];

        store.setState({
            messages: {
                ...fromReducer.initialState,
                pageData: {
                    ...fromReducer.initialState.pageData,
                    messages: expectedFilteredList
                }
            }
        });

        actions$ = of({ type: MessagesActionTypes.FilterMessagesByReadStatusBegin, messageType: 'ALL' });

        effects.filter$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.FilterMessagesByReadStatusSuccess);
            expect((response as any).messageList).toEqual(expectedFilteredList);
            done();
        });*/
    });
    xit('filter$ should return a FilterMessagesByReadStatusSuccess action with the READ Messages on success', (done: DoneFn) => {

        /*const expectedFilteredList = [
            { id: 123, readStatus: 0 } as LeadModel,
            { id: 456, readStatus: 1 } as LeadModel,
            { id: 789, readStatus: 1 } as LeadModel
        ];

        store.setState({
            messages: {
                ...fromReducer.initialState,
                pageData: {
                    ...fromReducer.initialState.pageData,
                    messages: expectedFilteredList
                }
            }
        });

        actions$ = of({ type: MessagesActionTypes.FilterMessagesByReadStatusBegin, messageType: 'READ' });

        effects.filter$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.FilterMessagesByReadStatusSuccess);
            expect((response as any).messageList.length).toBe(2);
            done();
        });*/
    });
    xit('filter$ should return a FilterMessagesByReadStatusSuccess action with the NEW Messages on success', (done: DoneFn) => {

        /*const expectedFilteredList = [
            { id: 123, readStatus: 0 } as LeadModel,
            { id: 456, readStatus: 1 } as LeadModel,
            { id: 789, readStatus: 1 } as LeadModel
        ];

        store.setState({
            messages: {
                ...fromReducer.initialState,
                pageData: {
                    ...fromReducer.initialState.pageData,
                    messages: expectedFilteredList
                }
            }
        });

        actions$ = of({ type: MessagesActionTypes.FilterMessagesByReadStatusBegin, messageType: 'NEW' });

        effects.filter$.subscribe(response => {
            expect(response.type).toEqual(MessagesActionTypes.FilterMessagesByReadStatusSuccess);
            expect((response as any).messageList.length).toBe(1);
            done();
        });*/
    });
});
