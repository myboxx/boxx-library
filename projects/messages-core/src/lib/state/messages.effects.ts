import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { MessageSource, MESSAGE_TYPE } from '../models/Message.model';
import { IMessagesService, MESSAGES_SERVICE } from '../services/IMessages.service';
import * as fromActions from './messages.actions';
import * as fromReducer from './messages.reducer';

@Injectable()
export class MessagesEffects {
    load$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.MessagesActionTypes.GetMessagesBegin),
            switchMap((action: any) => {
                return this.service.getMessages(action.sorting).pipe(
                    map((data) => fromActions.GetMessagesSuccessAction({ data })),
                    catchError(error => {
                        console.error('Couldn\'t get messages', error);
                        return of(fromActions.GetMessagesFailAction({ errors: error }));
                    })
                );
            })
        )
    );

    setAsRead$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.MessagesActionTypes.SetLeadAsReadBegin),
            switchMap((action: any) => {
                return this.service.setLeadAsRead(action.id).pipe(
                    map((message) => fromActions.SetLeadAsReadSuccessAction({ message })),
                    catchError(error => {
                        console.error('Couldn\'t set message as read', error);
                        return of(fromActions.SetLeadAsReadFailAction({ errors: error }));
                    })
                );
            })
        )
    );

    filter$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.MessagesActionTypes.FilterMessagesByReadStatusBegin),
            withLatestFrom(this.store$),
            switchMap(([action, store]) => {
                const messageType: MESSAGE_TYPE = (action as any).messageType;

                let messageList = store.messages.pageData.messages.filter(
                    item => store.messages.filteredSources.includes(item.source)
                );

                messageList = messageType === 'ALL' ? messageList :
                    messageList.filter(
                        item => {
                            if (messageType === 'READ' && (item as any).readStatus === undefined) {
                                return true;
                            }

                            return (item as any).readStatus === (messageType === 'READ' ? true : false);
                        }
                    );

                return of(
                    fromActions.FilterMessagesByReadStatusSuccessAction({ messageList })
                );
            }),
        )
    );

    sortAndFilter$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.MessagesActionTypes.FilterMessagesBySourceBegin),
            withLatestFrom(this.store$),
            switchMap(([action, store]) => {
                const sources: MessageSource[] = (action as any).sources;
                const sorting = (action as any).sorting as 'ASC' | 'DESC';

                const sortedMessages = store.messages.pageData.messages
                    .map(m => m) // <-- because pageData.messages is READ ONLY!!
                    .sort((a, b) => {
                        return (sorting === 'ASC' ?
                            b.createTime.localeCompare(a.createTime) :
                            a.createTime.localeCompare(b.createTime));
                    });

                const filteredMessages = sortedMessages.filter(item => sources.includes(item.source));

                return of(
                    fromActions.FilterMessagesBySourceSuccessAction({
                        sorting,
                        sources,
                        filteredMessages
                    })
                );
            }),
        )
    );

    upsertReviewReply$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.UpsertReviewReplyBeginAction),
            switchMap((action: any) => {
                return this.service.upsertReviewReply(action.review, action.comment).pipe(
                    map(review => fromActions.UpsertReviewReplySuccessAction({ review })),
                    catchError(error => {
                        console.error('Couldn\'t upsert review reply', error);
                        return of(fromActions.UpsertReviewReplyFaliAction({ errors: error }));
                    })
                );
            })
        )
    );

    deleteReviewReply$ = createEffect(
        () => this.actions$.pipe(
            ofType(fromActions.DeleteReviewReplyBeginAction),
            switchMap((action: any) => {
                return this.service.deleteReviewReply(action.review).pipe(
                    map(review => fromActions.DeleteReviewReplySuccessAction({ review })),
                    catchError(error => {
                        console.error('Couldn\'t delete review reply', error);
                        return of(fromActions.DeleteReviewReplyFaliAction({ errors: error }));
                    })
                );
            })
        )
    );

    constructor(
        private actions$: Actions,
        private store$: Store<AppState>,
        @Inject(MESSAGES_SERVICE) private service: IMessagesService
    ) { }
}

export interface AppState {
    messages: fromReducer.MessagesState;
}
