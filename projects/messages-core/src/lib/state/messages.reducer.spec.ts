import * as fromReducer from './messages.reducer';
import * as fromActions from './messages.actions';
import { LeadModel } from '../models/Lead.model';

describe('Messages Reducer', () => {
    const { initialState } = fromReducer;

    const thrownError = 'some bad error';

    describe('On unknown Action', () => {
        it('should return the default state', () => {

            const action = { type: 'Unknown' };

            const state = fromReducer.messagesReducer(initialState, action);

            expect(state).toBe(initialState);
        });
    });

    describe('On Begin Actions', () => {
        it('should set State.isLoading = true', () => {
            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                error: null,
                success: null,
                isLoading: true
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.GetMessagesBeginAction({ sorting: 'ASC' });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });
    });

    describe('On Succees Actions', () => {
        it('GetMessagesSuccessAction should set State.pageData', () => {
            const fakeMessagesPageModel = {
                stats: {
                    leads: {
                        total: 0,
                        read: 0,
                        new: 0
                    },
                    reviews: {
                        averageRating: 0,
                        totalReviewCount: 0
                    }
                },
                messages: [LeadModel.empty(), LeadModel.empty(), LeadModel.empty()],
                chartPoints: []
            };

            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                isLoading: false,
                hasBeenFetched: true,
                pageData: fakeMessagesPageModel,
                error: null,
                success: { after: 'GET' },
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.GetMessagesSuccessAction({ data: fakeMessagesPageModel });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });

        it('SetLeadAsReadSuccessAction should set State.pageData', () => {

            initialState.pageData = {
                stats: {
                    leads: {
                        total: 3,
                        read: 0,
                        new: 3
                    },
                    reviews: {
                        averageRating: 0,
                        totalReviewCount: 0
                    }
                },
                messages: [
                    { id: 123, readStatus: 0 } as any,
                    { id: 456, readStatus: 0 } as any, // <-- this item must be updated
                    { id: 789, readStatus: 0 } as any
                ],
            };

            const fakeMessagesModelList = [
                { id: 123, readStatus: 0 } as any,
                { id: 456, readStatus: 1 } as any, // <-- the updated item
                { id: 789, readStatus: 0 } as any
            ];

            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                pageData: {
                    stats: {
                        leads: { // <-- updated values
                            total: 3,
                            read: 1,
                            new: 2
                        },
                        reviews: {
                            averageRating: 0,
                            totalReviewCount: 0
                        }
                    },
                    messages: fakeMessagesModelList,
                },
                error: null,
                success: { after: 'SET_LEAD_READ' },
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.SetLeadAsReadSuccessAction({ message: fakeMessagesModelList[1] });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });
    });

    describe('On Fail Actions', () => {
        it('GetMessagesFailAction should set State.error { after: \'GET\', error: any }', () => {
            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                isLoading: false,
                error: { after: 'GET', error: thrownError },
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.GetMessagesFailAction({ errors: thrownError });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });

        it('SetLeadAsReadFailAction should set State.error { after: \'SET_LEAD_READ\', error: any }', () => {
            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                error: { after: 'SET_LEAD_READ', error: thrownError },
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.SetLeadAsReadFailAction({ errors: thrownError });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });
    });

    describe('On Filter and Select Actions', () => {
        it('FilterMessagesByReadStatusSuccessAction should set State.filteredItems', () => {
            const messageList = [
                { id: 134 } as LeadModel,
                { id: 456 } as LeadModel
            ];

            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                filteredItems: messageList,
                success: null
            };

            initialState.filteredItems = [
                { id: 987 } as LeadModel,
                { id: 654 } as LeadModel
            ];

            let state: fromReducer.MessagesState;

            const getAction = fromActions.FilterMessagesByReadStatusSuccessAction({ messageList });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });

        it('SelectMessageAction should set State.selectedId', () => {
            const messageId = 987;

            const expectedState: fromReducer.MessagesState = {
                ...initialState,
                selectedId: messageId,
                success: null
            };

            let state: fromReducer.MessagesState;

            const getAction = fromActions.SelectMessageAction({ messageId });
            state = fromReducer.messagesReducer(initialState, getAction);
            expect(state).toEqual(expectedState);
        });
    });
});
