import { Action, createReducer, on } from '@ngrx/store';
import { IMessagesStateError, IMessagesStateSuccess } from '../core/IStateErrorSuccess';
import { MessageBaseModel, MessageSource } from '../models/Message.model';
import { MessagesPageModel } from '../models/MessagePage.model';
import * as fromActions from './messages.actions';

export interface MessagesState {
    isLoading: boolean;
    hasBeenFetched: boolean;
    pageData: MessagesPageModel;
    filteredSources: MessageSource[];
    filteredItems: MessageBaseModel[];
    sorting: 'ASC' | 'DESC';
    selectedId: number | string;
    error: IMessagesStateError;
    success: IMessagesStateSuccess;
}

export const initialState: MessagesState = {
    isLoading: false,
    hasBeenFetched: false,
    pageData: MessagesPageModel.empty(),
    filteredSources: [],
    filteredItems: [],
    sorting: 'ASC',
    selectedId: null,
    error: null,
    success: null
};

const reducer = createReducer(
    initialState,

    // On Begin Actions
    on(fromActions.GetMessagesBeginAction,
        fromActions.UpsertReviewReplyBeginAction,
        fromActions.DeleteReviewReplyBeginAction, (state): MessagesState => ({
            ...state,
            error: null,
            success: null,
            isLoading: true
        })),

    // ON Success Actions
    on(fromActions.GetMessagesSuccessAction, (state, action): MessagesState => ({
        ...state,
        isLoading: false,
        hasBeenFetched: true,
        pageData: {
            ...state.pageData,
            stats: action.data.stats,
            messages: action.data.messages,
        },
        error: null,
        success: { after: getAfterActionType(action.type) }
    })),
    on(fromActions.UpsertReviewReplySuccessAction,
        fromActions.DeleteReviewReplySuccessAction, (state, action): MessagesState => ({
        ...state,
        isLoading: false,
        hasBeenFetched: true,
        pageData: {
            ...state.pageData,
            messages: state.pageData.messages.map(m => {
                return m.id === action.review.id ? action.review : m;
            })
        },
        error: null,
        success: { after: getAfterActionType(action.type) }
    })),
    on(fromActions.SetLeadAsReadSuccessAction, (state, action): MessagesState => ({
        ...state,
        pageData: {
            ...state.pageData,
            stats: {
                ...state.pageData.stats,
                leads: {
                    ...state.pageData.stats.leads,
                    read: state.pageData.stats.leads.read + 1,
                    new: state.pageData.stats.leads.new - 1
                }
            },
            messages: [
                ...((ml) => {
                    const tmp = [...ml];

                    const idx = ml.findIndex((m) => m.id === action.message.id);

                    if (idx !== -1) {
                        tmp.splice(idx, 1, action.message);
                    }

                    return tmp;
                })(state.pageData.messages),
            ],
        },
        error: null,
        success: { after: getAfterActionType(action.type) }
    })),

    // ON Fail Actions
    on(fromActions.GetMessagesFailAction,
        fromActions.UpsertReviewReplyFaliAction,
        fromActions.DeleteReviewReplyFaliAction, (state, action): MessagesState => ({
            ...state,
            isLoading: false,
            error: { after: getAfterActionType(action.type), error: action.errors }
        })),
    on(fromActions.SetLeadAsReadFailAction, (state, action): MessagesState => ({
        ...state,
        error: { after: getAfterActionType(action.type), error: action.errors },
    })),

    // FILTER
    on(fromActions.FilterMessagesByReadStatusSuccessAction, (state, action): MessagesState => ({
        ...state,
        filteredItems: action.messageList,
        success: null
    })),
    on(fromActions.FilterMessagesBySourceSuccessAction, (state, action): MessagesState => ({
        ...state,
        sorting: action.sorting,
        filteredSources: action.sources,
        filteredItems: action.filteredMessages,
        success: null
    })),

    // SELECT
    on(fromActions.SelectMessageAction, (state, action): MessagesState => ({
        ...state,
        selectedId: action.messageId,
        success: null
    })),
);

function getAfterActionType(type: fromActions.MessagesActionTypes) {

    let action: 'GET' | 'SET_LEAD_READ' | 'UPSERT_REVIEW' | 'DELETE_REVIEW' | 'UNKNOWN';

    switch (type) {
        case fromActions.MessagesActionTypes.GetMessagesSuccess:
        case fromActions.MessagesActionTypes.GetMessagesFail:
            action = 'GET'; break;
        case fromActions.MessagesActionTypes.SetLeadAsReadSuccess:
        case fromActions.MessagesActionTypes.SetLeadAsReadFail:
            action = 'SET_LEAD_READ'; break;
        case fromActions.MessagesActionTypes.UpsertReviewReplySuccess:
        case fromActions.MessagesActionTypes.UpsertReviewReplyFail:
            action = 'UPSERT_REVIEW'; break;
        case fromActions.MessagesActionTypes.DeleteReviewReplyFail:
        case fromActions.MessagesActionTypes.DeleteReviewReplySuccess:
            action = 'DELETE_REVIEW'; break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

export function messagesReducer(state: MessagesState | undefined, action: Action) {
    return reducer(state, action);
}
