import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MessageSource } from '../models/Message.model';
import * as fromReducer from './messages.reducer';

export const getMessagesState = createFeatureSelector<fromReducer.MessagesState>('messages');

export const stateGetPageData = (state: fromReducer.MessagesState) => state.pageData;
export const stateGetIsLoading = (state: fromReducer.MessagesState) => state.isLoading;
export const stateGetMessages = (state: fromReducer.MessagesState) => state.pageData.messages;
export const stateGetLeads = (state: fromReducer.MessagesState) => state.pageData.messages.filter(
    m => m.source === MessageSource.EMAIL
);
export const stateGetReviews = (state: fromReducer.MessagesState) => state.pageData.messages.filter(
    m => m.source === MessageSource.GMB_REVIEW
);
export const stateGetFilteredItems = (state: fromReducer.MessagesState) => state.filteredItems;

export const getMessagesPageState = createSelector(
    getMessagesState,
    state => state
);

export const getMessagesPageData = createSelector(
    getMessagesState,
    stateGetPageData
);

export const getMessages = createSelector(
    getMessagesPageState,
    stateGetMessages
);

export const getLeads = createSelector(
    getMessagesPageState,
    stateGetLeads
);

export const getReviews = createSelector(
    getMessagesPageState,
    stateGetReviews
);

export const getIsLoading = createSelector(
    getMessagesPageState,
    stateGetIsLoading
);

export const getError = createSelector(
    getMessagesPageState,
    state => state.error
);

export const getSuccess = createSelector(
    getMessagesPageState,
    state => state.success
);

export const getFilteredMessages = createSelector(
    getMessagesPageState,
    stateGetFilteredItems
);

export const getMessageById = createSelector(
    getMessagesPageState,
    state => state.pageData.messages.filter(m => m.id === state.selectedId)[0]
);

export const hasBeenFetched = createSelector(
    getMessagesPageState,
    state => state.hasBeenFetched
);
