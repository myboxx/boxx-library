import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { MessageSource, MESSAGE_TYPE } from '../models/Message.model';
import { ReviewModel } from '../models/Review.model';
import * as fromActions from './messages.actions';
import * as fromReducer from './messages.reducer';
import * as fromSelector from './messages.selectors';

@Injectable()
export class MessagesStore {
    constructor(public store: Store<fromReducer.MessagesState>) { }

    get Loading$() { return this.store.select(fromSelector.getIsLoading); }

    get Error$() {
        return this.store.select(fromSelector.getError);
    }

    get Success$() {
        return this.store.select(fromSelector.getSuccess);
    }

    loadMessagesPage(sorting?: 'ASC' | 'DESC') {
        return this.store.dispatch(fromActions.GetMessagesBeginAction({ sorting }));
    }

    get MessagesPageData$() {
        return this.store.select(fromSelector.getMessagesPageData);
    }

    filterMessagesByReadStatus(criteria: MESSAGE_TYPE) {
        this.store.dispatch(fromActions.FilterMessagesByReadStatusBeginAction({ messageType: criteria }));
    }

    filterMessagesBySource(sorting: 'ASC' | 'DESC' = 'ASC', sources: MessageSource[]) {
        this.store.dispatch(fromActions.FilterMessagesBySourceBeginAction({ sorting, sources }));
    }

    get FilteredMessages$() {
        return this.store.select(fromSelector.getFilteredMessages);
    }

    MessageById$(messageId: number) {
        this.store.dispatch(fromActions.SelectMessageAction({ messageId }));
        return this.store.select(fromSelector.getMessageById);
    }

    get Leads$() {
        return this.store.select(fromSelector.getLeads);
    }

    setLeadAsRead(id: number) {
        this.store.dispatch(fromActions.SetLeadAsReadBeginAction({ id }));
    }

    get Reviews$() {
        return this.store.select(fromSelector.getReviews);
    }

    upsertReviewReply(review: ReviewModel, comment: string){
        this.store.dispatch(fromActions.UpsertReviewReplyBeginAction({ review, comment }));
    }

    deleteReviewReply(review: ReviewModel){
        this.store.dispatch(fromActions.DeleteReviewReplyBeginAction({ review }));
    }

    get HasBeenFetched$() {
        return this.store.select(fromSelector.hasBeenFetched);
    }
}
