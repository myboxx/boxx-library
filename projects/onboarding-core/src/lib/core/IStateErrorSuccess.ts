import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';

export type OnboardingStateSuccessErrorType =
    | 'GET'
    | 'SAVE'
    | 'UPDATE'
    | 'UNKNOWN';

export interface IOnboardingStateError extends IStateErrorBase {
    after: OnboardingStateSuccessErrorType;
}

export interface IOnboardingStateSuccess extends IStateSuccessBase {
    after: OnboardingStateSuccessErrorType;
}
