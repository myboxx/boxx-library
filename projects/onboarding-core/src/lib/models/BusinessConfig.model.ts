import { OnboardingModuleComponentBaseProps } from './Onboarding.model';

export class OnboardingBusinessConfigModel
    implements OnboardingBusinessConfigModelProps
{
    constructor() {}
    servicesData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigServicesDataProps>;
    scheduleData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigScheduleDataProps>;
    paymentsData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigPaymentsDataProps>;
    socialNetworksData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigSocialNetworksDataProps>;
    gmbStatusData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigGmbStatusDataProps>;
    notificationsData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigNotificationsDataProps>;
    websiteData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigWebsiteDataProps>;
}

export interface OnboardingBusinessConfigModelProps {
    servicesData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigServicesDataProps>;
    scheduleData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigScheduleDataProps>;
    paymentsData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigPaymentsDataProps>;
    socialNetworksData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigSocialNetworksDataProps>;
    gmbStatusData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigGmbStatusDataProps>;
    notificationsData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigNotificationsDataProps>;
    websiteData: OnboardingModuleComponentBaseProps<OnboardingBusinessConfigWebsiteDataProps>;
}

export type OnboardingBusinessConfigServicesDataProps = string[];

export type OnboardingBusinessConfigScheduleDataProps =
    | ['HOURS']
    | ['NO_HOURS']
    | Array<Array<string[]>>; // example: [[['10:00'], ['11:00']], [],...]

export type OnboardingBusinessConfigPaymentsDataProps = string[];

export interface OnboardingBusinessConfigSocialNetworksDataProps {
    facebook: boolean;
    twitter: boolean;
    instagram: boolean;
}

export interface OnboardingBusinessConfigGmbStatusDataProps {
    status: string;
}

export type OnboardingBusinessConfigNotificationsDataProps = string[];

export interface OnboardingBusinessConfigWebsiteDataProps {
    type: 'new' | 'existing';
    images: {
        logo: string;
        main: string;
        gallery: { [key: number]: string };
    };
    description: string;
    shortDescription: string;
    showSchedule: boolean;
    showServices: boolean;
    showSocialNetworks: boolean;
}
