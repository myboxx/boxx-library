import { OnboardingModuleComponentBaseProps } from './Onboarding.model';

export class OnboardingMultiuserConfigModel {
    constructor() {}
}

export interface OnboardingMultiuserConfigModelProps {
    // todo: rename someStepData according to model
    someStepData: OnboardingModuleComponentBaseProps<any>;
}
