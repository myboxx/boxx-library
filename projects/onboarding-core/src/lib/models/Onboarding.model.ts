import { OnboardingBusinessConfigModel } from './BusinessConfig.model';
import { OnboardingInboxxConfigModel } from './InboxxConfig.model';
import { OnboardingMultiuserConfigModel } from './MultiuserConfig.model';
import { OnboardingRegistrationModel } from './Registration.model';
import { OnboardingStoreConfigModel } from './StoreConfig.model';

export class OnboardingModel implements OnboardingModelProps {
    constructor() {}
    registrationPage: OnboardingRegistrationModel;
    businessConfigPage: OnboardingBusinessConfigModel;
    inboxxConfigPage: OnboardingInboxxConfigModel;
    storeConfigPage: OnboardingStoreConfigModel;
    multiuserConfigPage: OnboardingMultiuserConfigModel;
}

export interface OnboardingModelProps {
    registrationPage: OnboardingRegistrationModel;
    businessConfigPage: OnboardingBusinessConfigModel;
    inboxxConfigPage: OnboardingInboxxConfigModel;
    storeConfigPage: OnboardingStoreConfigModel;
    multiuserConfigPage: OnboardingMultiuserConfigModel;
}

export interface OnboardingModuleComponentBaseProps<T> {
    completed: boolean;
    fields: T;
}
