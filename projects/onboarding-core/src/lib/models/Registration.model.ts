import { OnboardingModuleComponentBaseProps } from './Onboarding.model';

export class OnboardingRegistrationModel
    implements OnboardingRegistrationModelProps
{
    constructor() {}

    userData: OnboardingModuleComponentBaseProps<OnboardingRegistrationUserDataProps>;
    gmbData: OnboardingModuleComponentBaseProps<OnboardingRegistrationGmbDataProps>;
    subscriptionData: OnboardingModuleComponentBaseProps<OnboardingRegistrationSubscriptionDataProps>;
}

export interface OnboardingRegistrationModelProps {
    userData: OnboardingModuleComponentBaseProps<OnboardingRegistrationUserDataProps>;
    gmbData: OnboardingModuleComponentBaseProps<OnboardingRegistrationGmbDataProps>;
    subscriptionData: OnboardingModuleComponentBaseProps<OnboardingRegistrationSubscriptionDataProps>;
}

export interface OnboardingRegistrationUserDataProps {
    name: string;
    lastname: string;
    countryCode: string;
    languageCode: string;
    phone: string;
    email: string;
    password: string;
}

export interface OnboardingRegistrationGmbDataProps {
    hasAccount: boolean;
    name: string;
    activityId: string;
    activityName: string;
    locationId: string;
    locationName: string;
    phone: string;
    email: string;
}

export interface OnboardingRegistrationSubscriptionDataProps {
    type: 'basic' | 'advanced';
    cardDetails: {
        number: string;
        alias: string;
        expirationMonth: string;
        expirationYear: string;
        countryCode: string;
        zipcode: string;
    };
}
