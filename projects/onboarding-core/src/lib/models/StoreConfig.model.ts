import { OnboardingModuleComponentBaseProps } from './Onboarding.model';

export class OnboardingStoreConfigModel {
    constructor() {}
}

export interface OnboardingStoreConfigModelProps {
    // todo: rename someStepData according to model
    someStepData: OnboardingModuleComponentBaseProps<any>;
}
