import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ONBOARDING_REPOSITORY } from './repositories/IOnboarding.repository';
import { OnboardingRepository } from './repositories/Onboarding.repository';
import { ONBOARDING_SERVICE } from './services/IOnboarding.service';
import { OnboardingService } from './services/onboarding.service';
import { OnboardingEffects } from './state/onboarding.effects';
import { onboardingReducer } from './state/onboarding.reducer';
import { OnboardingStore } from './state/onboarding.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('onboarding', onboardingReducer),
        EffectsModule.forFeature([OnboardingEffects]),
    ],
    exports: [],
})
export class OnboardingCoreModule {
    static forRoot(
        config: ModuleOptionsInterface
    ): ModuleWithProviders<OnboardingCoreModule> {
        return {
            ngModule: OnboardingCoreModule,
            providers: [
                { provide: ONBOARDING_SERVICE, useClass: OnboardingService },
                {
                    provide: ONBOARDING_REPOSITORY,
                    useClass: OnboardingRepository,
                },
                ...config.providers,
                OnboardingStore,
            ],
        };
    }
}
