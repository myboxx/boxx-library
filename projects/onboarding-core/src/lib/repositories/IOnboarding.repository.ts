import { InjectionToken } from '@angular/core';
import { IHttpBasicResponse } from 'dist/core/public-api';
import { Observable } from 'rxjs';
import {
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
    OnboardingRegistrationUserDataProps,
} from '../models/Registration.model';

export interface IOnboardingRepository {
    // ------------- REGISTRATION-------------
    saveUserData(
        payload: OnboardingRegistrationUserDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveGmbData(
        payload: OnboardingRegistrationGmbDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveSubscriptionData(
        payload: OnboardingRegistrationSubscriptionDataProps
    ): Observable<IHttpBasicResponse<any>>;

    // ------------- BUSINESS CONFIG -------------
    saveServicesData(
        payload: OnboardingBusinessConfigServicesDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveScheduleData(
        payload: OnboardingBusinessConfigScheduleDataProps
    ): Observable<IHttpBasicResponse<any>>;
    savePaymentsData(
        payload: OnboardingBusinessConfigPaymentsDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveSocialNetworksData(
        payload: OnboardingBusinessConfigSocialNetworksDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveGmbStatusData(
        payload: OnboardingBusinessConfigGmbStatusDataProps
    ): Observable<IHttpBasicResponse<any>>;
    saveWebsiteData(
        payload: OnboardingBusinessConfigWebsiteDataProps
    ): Observable<IHttpBasicResponse<any>>;
}

export const ONBOARDING_REPOSITORY = new InjectionToken<IOnboardingRepository>(
    'OnboardingRepository'
);
