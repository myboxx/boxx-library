import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
    AbstractAppConfigService,
    APP_CONFIG_SERVICE,
    IHttpBasicResponse,
} from '@boxx/core';
import { Observable } from 'rxjs';
import {
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationUserDataProps,
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
} from '../models/Registration.model';
import { IOnboardingRepository } from './IOnboarding.repository';

@Injectable()
export class OnboardingRepository implements IOnboardingRepository {
    constructor(
        @Inject(APP_CONFIG_SERVICE)
        private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient
    ) {}

    // -------------- REGISTRATION -----------------
    saveUserData(
        payload: OnboardingRegistrationUserDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveGmbData(
        payload: OnboardingRegistrationGmbDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveSubscriptionData(
        payload: OnboardingRegistrationSubscriptionDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }

    // ------------ BUSINESS CONFIGURATION ----------
    saveServicesData(
        payload: OnboardingBusinessConfigServicesDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveScheduleData(
        payload: OnboardingBusinessConfigScheduleDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    savePaymentsData(
        payload: OnboardingBusinessConfigPaymentsDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveSocialNetworksData(
        payload: OnboardingBusinessConfigSocialNetworksDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveGmbStatusData(
        payload: OnboardingBusinessConfigGmbStatusDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }
    saveWebsiteData(
        payload: OnboardingBusinessConfigWebsiteDataProps
    ): Observable<IHttpBasicResponse<any>> {
        return this.httpClient.get<IHttpBasicResponse<any>>('');
    }

    private getBaseUrl() {
        return 'PLAESE_SET_THE_URL!!!';
        // return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/shop`;
    }
}
