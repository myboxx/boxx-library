import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import {
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
    OnboardingRegistrationUserDataProps,
} from '../models/Registration.model';

export interface IOnboardingService {
    // ------------- REGISTRATION -------------
    saveUserData(payload: OnboardingRegistrationUserDataProps): Observable<any>;
    saveGmbData(payload: OnboardingRegistrationGmbDataProps): Observable<any>;
    saveSubscriptionData(
        payload: OnboardingRegistrationSubscriptionDataProps
    ): Observable<any>;

    // ------------- BUSINESS CONFIG -------------
    saveServicesData(
        payload: OnboardingBusinessConfigServicesDataProps
    ): Observable<any>;
    saveScheduleData(
        payload: OnboardingBusinessConfigScheduleDataProps
    ): Observable<any>;
    savePaymentsData(
        payload: OnboardingBusinessConfigPaymentsDataProps
    ): Observable<any>;
    saveSocialNetworksData(
        payload: OnboardingBusinessConfigSocialNetworksDataProps
    ): Observable<any>;
    saveGmbStatusData(
        payload: OnboardingBusinessConfigGmbStatusDataProps
    ): Observable<any>;
    saveWebsiteData(
        payload: OnboardingBusinessConfigWebsiteDataProps
    ): Observable<any>;
}

export const ONBOARDING_SERVICE = new InjectionToken<IOnboardingService>(
    'OnboardingService'
);
