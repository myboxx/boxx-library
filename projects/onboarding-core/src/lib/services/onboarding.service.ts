import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationUserDataProps,
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
} from '../models/Registration.model';
import {
    IOnboardingRepository,
    ONBOARDING_REPOSITORY,
} from '../repositories/IOnboarding.repository';
import { IOnboardingService } from './IOnboarding.service';

@Injectable()
export class OnboardingService implements IOnboardingService {
    constructor(
        @Inject(ONBOARDING_REPOSITORY) private repository: IOnboardingRepository
    ) { }

    // ------------- REGISTRATION -------------
    saveUserData(
        payload: OnboardingRegistrationUserDataProps
    ): Observable<any> {
        return this.repository.saveUserData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveGmbData(payload: OnboardingRegistrationGmbDataProps): Observable<any> {
        return this.repository.saveGmbData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveSubscriptionData(
        payload: OnboardingRegistrationSubscriptionDataProps
    ): Observable<any> {
        return this.repository.saveSubscriptionData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }

    // ------------- BUSINESS CONFIG -------------

    saveServicesData(
        payload: OnboardingBusinessConfigServicesDataProps
    ): Observable<any> {
        return this.repository.saveServicesData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveScheduleData(
        payload: OnboardingBusinessConfigScheduleDataProps
    ): Observable<any> {
        return this.repository.saveScheduleData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    savePaymentsData(
        payload: OnboardingBusinessConfigPaymentsDataProps
    ): Observable<any> {
        return this.repository.savePaymentsData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveSocialNetworksData(
        payload: OnboardingBusinessConfigSocialNetworksDataProps
    ): Observable<any> {
        return this.repository.saveSocialNetworksData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveGmbStatusData(
        payload: OnboardingBusinessConfigGmbStatusDataProps
    ): Observable<any> {
        return this.repository.saveGmbStatusData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
    saveWebsiteData(
        payload: OnboardingBusinessConfigWebsiteDataProps
    ): Observable<any> {
        return this.repository.saveWebsiteData(payload).pipe(
            map((response) => response.data),
            catchError((error) => {
                throw error;
            })
        );
    }
}
