import { createAction, props } from '@ngrx/store';
import {
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';

export enum OnboardingBusinessConfigActionType {
    SaveServicesDataBegin = '[ONBOARDING] Save Services Data Begin',
    SaveServicesDataSuccess = '[ONBOARDING] Save Services Data Success',
    SaveServicesDataFail = '[ONBOARDING] Save Services Data Fail',

    SaveScheduleDataBegin = '[ONBOARDING] Save Schedule Data Begin',
    SaveScheduleDataSuccess = '[ONBOARDING] Save Schedule Data Success',
    SaveScheduleDataFail = '[ONBOARDING] Save Schedule Data Fail',

    SavePaymentsDataBegin = '[ONBOARDING] Save Payments Data Begin',
    SavePaymentsDataSuccess = '[ONBOARDING] Save Payments Data Success',
    SavePaymentsDataFail = '[ONBOARDING] Save Payments Data Fail',

    SaveSocialNetworksDataBegin = '[ONBOARDING] Save Social Networks Data Begin',
    SaveSocialNetworksDataSuccess = '[ONBOARDING] Save Social Networks Data Success',
    SaveSocialNetworksDataFail = '[ONBOARDING] Save Social Networks Data Fail',

    SaveGmbStatusDataBegin = '[ONBOARDING] Save Gmb Status Data Begin',
    SaveGmbStatusDataSuccess = '[ONBOARDING] Save Gmb Status Data Success',
    SaveGmbStatusDataFail = '[ONBOARDING] Save Gmb Status Data Fail',

    SaveWebsiteDataBegin = '[ONBOARDING] Save Website Data Begin',
    SaveWebsiteDataSuccess = '[ONBOARDING] Save Website Data Success',
    SaveWebsiteDataFail = '[ONBOARDING] Save Website Data Fail',

    ClearOnboardingBusinessConfigSuccessErrorData = '[ONBOARDING] Onboarding Business Config Success/Error Data',
}

export const SaveServicesDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SaveServicesDataBegin,
    props<{ payload: OnboardingBusinessConfigServicesDataProps }>()
);

export const SaveServicesDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SaveServicesDataSuccess,
    props<{ payload: any }>()
);

export const SaveServicesDataFail = createAction(
    OnboardingBusinessConfigActionType.SaveServicesDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const SaveScheduleDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SaveScheduleDataBegin,
    props<{ payload: OnboardingBusinessConfigScheduleDataProps }>()
);

export const SaveScheduleDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SaveScheduleDataSuccess,
    props<{ payload: any }>()
);

export const SaveScheduleDataFail = createAction(
    OnboardingBusinessConfigActionType.SaveScheduleDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const SavePaymentsDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SavePaymentsDataBegin,
    props<{ payload: OnboardingBusinessConfigPaymentsDataProps }>()
);

export const SavePaymentsDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SavePaymentsDataSuccess,
    props<{ payload: any }>()
);

export const SavePaymentsDataFail = createAction(
    OnboardingBusinessConfigActionType.SavePaymentsDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const SaveSocialNetworksDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SaveSocialNetworksDataBegin,
    props<{ payload: OnboardingBusinessConfigSocialNetworksDataProps }>()
);

export const SaveSocialNetworksDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SaveSocialNetworksDataSuccess,
    props<{ payload: any }>()
);

export const SaveSocialNetworksDataFail = createAction(
    OnboardingBusinessConfigActionType.SaveSocialNetworksDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const SaveGmbStatusDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SaveGmbStatusDataBegin,
    props<{ payload: OnboardingBusinessConfigGmbStatusDataProps }>()
);

export const SaveGmbStatusDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SaveGmbStatusDataSuccess,
    props<{ payload: any }>()
);

export const SaveGmbStatusDataFail = createAction(
    OnboardingBusinessConfigActionType.SaveGmbStatusDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const SaveWebsiteDataBeginAction = createAction(
    OnboardingBusinessConfigActionType.SaveWebsiteDataBegin,
    props<{ payload: OnboardingBusinessConfigWebsiteDataProps }>()
);

export const SaveWebsiteDataSuccessAction = createAction(
    OnboardingBusinessConfigActionType.SaveWebsiteDataSuccess,
    props<{ payload: any }>()
);

export const SaveWebsiteDataFail = createAction(
    OnboardingBusinessConfigActionType.SaveWebsiteDataFail,
    props<{ errors: any }>()
);

// ----------------------------------------------------------------

export const ClearBusinessConfigSuccessErrorDataAction = createAction(
    OnboardingBusinessConfigActionType.ClearOnboardingBusinessConfigSuccessErrorData
);
