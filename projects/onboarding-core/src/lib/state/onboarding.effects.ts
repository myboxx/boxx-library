import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
    IOnboardingService,
    ONBOARDING_SERVICE,
} from '../services/IOnboarding.service';
import * as fromBusinessConfigActions from './business-config.actions';
import * as fromRegistrationActions from './registration.actions';

@Injectable()
export class OnboardingEffects {
    // ======================== REGISTRATION ========================
    SaveUserData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromRegistrationActions.SaveUserDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveUserData(action.payload).pipe(
                    map((payload) => {
                        return fromRegistrationActions.SaveUserDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromRegistrationActions.SaveUserDataFailAction({
                                errors,
                            }),
                            fromRegistrationActions.ClearOnboardingRegistraionSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveGmbData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromRegistrationActions.SaveGmbDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveGmbData(action.payload).pipe(
                    map((payload) => {
                        return fromRegistrationActions.SaveGmbDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromRegistrationActions.SaveGmbDataFailAction({
                                errors,
                            }),
                            fromRegistrationActions.ClearOnboardingRegistraionSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveSubscriptionData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromRegistrationActions.SaveSubscriptionDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveSubscriptionData(action.payload).pipe(
                    map((payload) => {
                        return fromRegistrationActions.SaveSubscriptionDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromRegistrationActions.SaveSubscriptionDataFailAction(
                                {
                                    errors,
                                }
                            ),
                            fromRegistrationActions.ClearOnboardingRegistraionSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    // ======================== BUSINESS CONFIG ========================
    SaveServicesData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SaveServicesDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveServicesData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SaveServicesDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SaveServicesDataFail({
                                errors,
                            }),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveScheduleData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SaveScheduleDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveScheduleData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SaveScheduleDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SaveScheduleDataFail({
                                errors,
                            }),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SavePaymentsData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SavePaymentsDataBeginAction),
            switchMap((action: any) => {
                return this.service.savePaymentsData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SavePaymentsDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SavePaymentsDataFail({
                                errors,
                            }),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveSocialNetworksData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SaveSocialNetworksDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveSocialNetworksData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SaveSocialNetworksDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SaveSocialNetworksDataFail(
                                {
                                    errors,
                                }
                            ),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveGmbStatusData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SaveGmbStatusDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveGmbData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SaveGmbStatusDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SaveGmbStatusDataFail({
                                errors,
                            }),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    SaveWebsiteData$ = createEffect(() =>
        this.actions.pipe(
            ofType(fromBusinessConfigActions.SaveWebsiteDataBeginAction),
            switchMap((action: any) => {
                return this.service.saveWebsiteData(action.payload).pipe(
                    map((payload) => {
                        return fromBusinessConfigActions.SaveWebsiteDataSuccessAction(
                            { payload }
                        );
                    }),
                    catchError((errors) => {
                        return of([
                            fromBusinessConfigActions.SaveWebsiteDataFail({
                                errors,
                            }),
                            fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction(),
                        ]).pipe(switchMap((d) => d));
                    })
                );
            })
        )
    );

    constructor(
        private actions: Actions,
        @Inject(ONBOARDING_SERVICE) private service: IOnboardingService
    ) {}
}
