import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import {
    IOnboardingStateError,
    IOnboardingStateSuccess,
    OnboardingStateSuccessErrorType,
} from '../core/IStateErrorSuccess';
import {
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigNotificationsDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
    OnboardingRegistrationUserDataProps,
} from '../models/Registration.model';
import * as fromBusinessConfigActions from './business-config.actions';
import * as fromRegistrationActions from './registration.actions';

type AbstractState<T> = {
    isLoading: boolean;
    data: T;
    error: IOnboardingStateError;
    success: IOnboardingStateSuccess;
};

export interface IOnboardingState {
    registrationPage: {
        userData: AbstractState<OnboardingRegistrationUserDataProps>;
        gmbData: AbstractState<OnboardingRegistrationGmbDataProps>;
        subscriptionData: AbstractState<OnboardingRegistrationSubscriptionDataProps>;
    };
    businessConfigPage: {
        servicesData: AbstractState<OnboardingBusinessConfigServicesDataProps>;
        scheduleData: AbstractState<OnboardingBusinessConfigScheduleDataProps>;
        paymentsData: AbstractState<OnboardingBusinessConfigPaymentsDataProps>;
        socialNetworksData: AbstractState<OnboardingBusinessConfigSocialNetworksDataProps>;
        gmbStatusData: AbstractState<OnboardingBusinessConfigGmbStatusDataProps>;
        notificationsData: AbstractState<OnboardingBusinessConfigNotificationsDataProps>;
        websiteData: AbstractState<OnboardingBusinessConfigWebsiteDataProps>;
    };
    inboxxConfigPage: {};
    storeConfigPage: {};
    multiuserConfigPage: {};
}

export const InitialState: IOnboardingState = {
    registrationPage: {
        userData: {
            isLoading: false,
            data: {
                name: null,
                lastname: null,
                countryCode: null,
                languageCode: null,
                phone: null,
                email: null,
                password: null,
            },
            error: null,
            success: null,
        },
        gmbData: {
            isLoading: false,
            data: {
                hasAccount: false,
                name: null,
                activityId: null,
                activityName: null,
                locationId: null,
                locationName: null,
                phone: null,
                email: null,
            },
            error: null,
            success: null,
        },
        subscriptionData: {
            isLoading: false,
            data: {
                type: 'basic',
                cardDetails: {
                    number: null,
                    alias: null,
                    expirationMonth: null,
                    expirationYear: null,
                    countryCode: null,
                    zipcode: null,
                },
            },
            error: null,
            success: null,
        },
    },
    businessConfigPage: {
        servicesData: {
            isLoading: false,
            data: [],
            error: null,
            success: null,
        },
        scheduleData: {
            isLoading: false,
            data: ['HOURS'],
            error: null,
            success: null,
        },
        paymentsData: {
            isLoading: false,
            data: [],
            error: null,
            success: null,
        },
        socialNetworksData: {
            isLoading: false,
            data: {
                facebook: false,
                twitter: false,
                instagram: false,
            },
            error: null,
            success: null,
        },
        gmbStatusData: {
            isLoading: false,
            data: {
                status: null,
            },
            error: null,
            success: null,
        },
        notificationsData: {
            isLoading: false,
            data: [],
            error: null,
            success: null,
        },
        websiteData: {
            isLoading: false,
            data: {
                type: null,
                images: {
                    logo: null,
                    main: null,
                    gallery: null,
                },
                description: null,
                shortDescription: null,
                showSchedule: false,
                showServices: false,
                showSocialNetworks: false,
            },
            error: null,
            success: null,
        },
    },
    inboxxConfigPage: {},
    storeConfigPage: {},
    multiuserConfigPage: {},
};

const reducer: ActionReducer<IOnboardingState> = createReducer(
    InitialState,
    // =================== REGISTRATION ===================
    // ------------------ REGISTRATION: ON BEGIN ACTIONS -----------
    on(
        fromRegistrationActions.SaveUserDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                userData: {
                    ...state.registrationPage.userData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveGmbDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                gmbData: {
                    ...state.registrationPage.gmbData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveSubscriptionDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                subscriptionData: {
                    ...state.registrationPage.subscriptionData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),

    // ------------------ REGISTRATION: ON SUCCESS ACTIONS -----------
    on(
        fromRegistrationActions.SaveUserDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                userData: {
                    ...state.registrationPage.userData,
                    data: payload,
                    isLoading: false,
                    success: {
                        after: getOnboardingRegistrationActionType(type),
                    },
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveGmbDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                gmbData: {
                    ...state.registrationPage.gmbData,
                    data: payload,
                    isLoading: false,
                    success: {
                        after: getOnboardingRegistrationActionType(type),
                    },
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveSubscriptionDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                subscriptionData: {
                    ...state.registrationPage.subscriptionData,
                    data: payload,
                    isLoading: false,
                    success: {
                        after: getOnboardingRegistrationActionType(type),
                    },
                },
            },
        })
    ),

    // ------------------ REGISTRATION: ON FAIL ACTIONS -----------
    on(
        fromRegistrationActions.SaveUserDataFailAction,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                userData: {
                    ...state.registrationPage.userData,
                    isLoading: false,
                    error: {
                        after: getOnboardingRegistrationActionType(type),
                        error: errors,
                    },
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveGmbDataFailAction,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                gmbData: {
                    ...state.registrationPage.gmbData,
                    isLoading: false,
                    error: {
                        after: getOnboardingRegistrationActionType(type),
                        error: errors,
                    },
                },
            },
        })
    ),
    on(
        fromRegistrationActions.SaveSubscriptionDataFailAction,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                subscriptionData: {
                    ...state.registrationPage.subscriptionData,
                    isLoading: false,
                    error: {
                        after: getOnboardingRegistrationActionType(type),
                        error: errors,
                    },
                },
            },
        })
    ),
    on(
        fromRegistrationActions.ClearOnboardingRegistraionSuccessErrorDataAction,
        (state) => ({
            ...state,
            registrationPage: {
                ...state.registrationPage,
                userData: {
                    ...state.registrationPage.userData,
                    error: null,
                    success: null,
                },
                gmbData: {
                    ...state.registrationPage.gmbData,
                    error: null,
                    success: null,
                },
                subscriptionData: {
                    ...state.registrationPage.subscriptionData,
                    error: null,
                    success: null,
                },
            },
        })
    ),

    // =================== BUSINESS CONFIGURATION ===================
    // ---------- BUSINESS CONFIGURATION: ON BEGIN ACTIONS ----------
    on(
        fromBusinessConfigActions.SaveServicesDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                servicesData: {
                    ...state.businessConfigPage.servicesData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveScheduleDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                scheduleData: {
                    ...state.businessConfigPage.scheduleData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SavePaymentsDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                paymentsData: {
                    ...state.businessConfigPage.paymentsData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveSocialNetworksDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                socialNetworksData: {
                    ...state.businessConfigPage.socialNetworksData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveGmbStatusDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                gmbStatusData: {
                    ...state.businessConfigPage.gmbStatusData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveWebsiteDataBeginAction,
        (state): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                websiteData: {
                    ...state.businessConfigPage.websiteData,
                    isLoading: true,
                    success: null,
                    error: null,
                },
            },
        })
    ),
    // --------- BUSINESS CONFIGURATION: ON SUCCESS ACTIONS ---------
    on(
        fromBusinessConfigActions.SaveServicesDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                servicesData: {
                    ...state.businessConfigPage.servicesData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveScheduleDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                scheduleData: {
                    ...state.businessConfigPage.scheduleData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SavePaymentsDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                paymentsData: {
                    ...state.businessConfigPage.paymentsData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveSocialNetworksDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                socialNetworksData: {
                    ...state.businessConfigPage.socialNetworksData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveGmbStatusDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                gmbStatusData: {
                    ...state.businessConfigPage.gmbStatusData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveWebsiteDataSuccessAction,
        (state, { type, payload }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                websiteData: {
                    ...state.businessConfigPage.websiteData,
                    isLoading: false,
                    data: payload,
                    success: { after: null },
                },
            },
        })
    ),
    // ---------- BUSINESS CONFIGURATION: ON ERROR ACTIONS ----------
    on(
        fromBusinessConfigActions.SaveServicesDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                servicesData: {
                    ...state.businessConfigPage.servicesData,
                    isLoading: false,
                    error: { after: null, error: errors },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveScheduleDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                scheduleData: {
                    ...state.businessConfigPage.scheduleData,
                    isLoading: false,
                    error: { after: null, error: errors },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SavePaymentsDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                paymentsData: {
                    ...state.businessConfigPage.paymentsData,
                    isLoading: false,
                    error: { after: null, error: errors },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveSocialNetworksDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                socialNetworksData: {
                    ...state.businessConfigPage.socialNetworksData,
                    isLoading: false,
                    error: { after: null, error: errors },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveGmbStatusDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                gmbStatusData: {
                    ...state.businessConfigPage.gmbStatusData,
                    isLoading: false,
                    error: { after: null, error: errors },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.SaveWebsiteDataFail,
        (state, { type, errors }): IOnboardingState => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                websiteData: {
                    ...state.businessConfigPage.websiteData,
                    isLoading: false,
                    error: {
                        after: getBusinessConfignActionType(type),
                        error: errors,
                    },
                },
            },
        })
    ),
    on(
        fromBusinessConfigActions.ClearBusinessConfigSuccessErrorDataAction,
        (state) => ({
            ...state,
            businessConfigPage: {
                ...state.businessConfigPage,
                servicesData: {
                    ...state.businessConfigPage.servicesData,
                    success: null,
                    error: null,
                },
                scheduleData: {
                    ...state.businessConfigPage.scheduleData,
                    success: null,
                    error: null,
                },
                paymentsData: {
                    ...state.businessConfigPage.paymentsData,
                    success: null,
                    error: null,
                },
                socialNetworksData: {
                    ...state.businessConfigPage.socialNetworksData,
                    success: null,
                    error: null,
                },
                gmbStatusData: {
                    ...state.businessConfigPage.gmbStatusData,
                    success: null,
                    error: null,
                },
                websiteData: {
                    ...state.businessConfigPage.websiteData,
                    success: null,
                    error: null,
                },
            },
        })
    )
);

function getOnboardingRegistrationActionType(
    type: fromRegistrationActions.OnboardingRegistrationActionType
) {
    let action: OnboardingStateSuccessErrorType;

    switch (type) {
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveUserDataSuccess:
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveGmbDataSuccess:
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveSubscriptionDataSuccess:
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveUserDataFail:
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveGmbDataFail:
        case fromRegistrationActions.OnboardingRegistrationActionType
            .SaveSubscriptionDataFail:
            action = 'SAVE';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

function getBusinessConfignActionType(
    type: fromBusinessConfigActions.OnboardingBusinessConfigActionType
) {
    let action: OnboardingStateSuccessErrorType;

    switch (type) {
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SaveServicesDataSuccess:
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SaveScheduleDataSuccess:
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SavePaymentsDataSuccess:
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SaveSocialNetworksDataSuccess:
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SaveGmbStatusDataSuccess:
        case fromBusinessConfigActions.OnboardingBusinessConfigActionType
            .SaveWebsiteDataSuccess:
            action = 'SAVE';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

export function onboardingReducer(
    state: IOnboardingState | undefined,
    action: Action
) {
    return reducer(state, action);
}

export interface OnboardingState {
    onboarding: IOnboardingState;
}
