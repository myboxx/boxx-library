import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReducer from './onboarding.reducer';

export const getOnboardingState =
    createFeatureSelector<fromReducer.IOnboardingState>('onboarding');

// ------- ONBOARDING GLOBAL STATE:

export const getOnboardingPageState = createSelector(
    getOnboardingState,
    (state) => state
);

// ------------------ REGISTRATION ---------------
export const getRegistrationState = createSelector(
    getOnboardingPageState,
    (state) => state.registrationPage
);
//// --> REGISTRATION: USER
export const getRegistrationUserState = createSelector(
    getRegistrationState,
    (registrationState) => registrationState.userData
);
export const getRegistrationUserIsLoading = createSelector(
    getRegistrationUserState,
    (user) => user.isLoading
);
export const getRegistrationUserData = createSelector(
    getRegistrationUserState,
    (user) => user.data
);
export const getRegistrationUserSuccess = createSelector(
    getRegistrationUserState,
    (user) => user.success
);
export const getRegistrationUserError = createSelector(
    getRegistrationUserState,
    (user) => user.error
);
//// --> REGISTRATION: GMB
export const getRegistrationGmbState = createSelector(
    getRegistrationState,
    (registrationState) => registrationState.gmbData
);
export const getRegistrationGmbIsLoading = createSelector(
    getRegistrationGmbState,
    (gmb) => gmb.isLoading
);
export const getRegistrationGmbData = createSelector(
    getRegistrationGmbState,
    (gmb) => gmb.data
);
export const getRegistrationGmbSuccess = createSelector(
    getRegistrationGmbState,
    (gmb) => gmb.success
);
export const getRegistrationGmbError = createSelector(
    getRegistrationGmbState,
    (gmb) => gmb.error
);
//// --> REGISTRATION: SUBSCRIPTION
export const getRegistrationSubscriptionState = createSelector(
    getRegistrationState,
    (registrationPage) => registrationPage.subscriptionData
);
export const getRegistrationSubscriptionIsLoading = createSelector(
    getRegistrationSubscriptionState,
    (subscription) => subscription.isLoading
);
export const getRegistrationSubscriptionData = createSelector(
    getRegistrationSubscriptionState,
    (subscription) => subscription.data
);
export const getRegistrationSubscriptionSuccess = createSelector(
    getRegistrationSubscriptionState,
    (subscription) => subscription.success
);
export const getRegistrationSubscriptionError = createSelector(
    getRegistrationSubscriptionState,
    (subscription) => subscription.error
);

// ------------------- BUSINESS CONFIGURATION ----------------
export const getBusinessConfigurationState = createSelector(
    getOnboardingPageState,
    (state) => state.businessConfigPage
);
//// --> BUSINESS CONFIGURATION: SERVICES
export const getBusinessConfigurationServicesState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.servicesData
);
export const getBusinessConfigurationServicesData = createSelector(
    getBusinessConfigurationServicesState,
    (services) => services.data
);
export const getBusinessConfigurationServicesIsLoading = createSelector(
    getBusinessConfigurationServicesState,
    (services) => services.isLoading
);
export const getBusinessConfigurationServicesSuccess = createSelector(
    getBusinessConfigurationServicesState,
    (services) => services.success
);
export const getBusinessConfigurationServicesSerror = createSelector(
    getBusinessConfigurationServicesState,
    (services) => services.error
);
//// --> BUSINESS CONFIGURATION: SCHEDULE
export const getBusinessConfigurationScheduleState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.scheduleData
);
export const getBusinessConfigurationScheduleIsLoading = createSelector(
    getBusinessConfigurationScheduleState,
    (schedule) => schedule.isLoading
);
export const getBusinessConfigurationScheduleData = createSelector(
    getBusinessConfigurationScheduleState,
    (schedule) => schedule.data
);
export const getBusinessConfigurationScheduleSuccess = createSelector(
    getBusinessConfigurationScheduleState,
    (schedule) => schedule.success
);
export const getBusinessConfigurationScheduleError = createSelector(
    getBusinessConfigurationScheduleState,
    (schedule) => schedule.error
);

//// --> BUSINESS CONFIGURATION: PAYMENTS
export const getBusinessConfigurationPaymentsState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.paymentsData
);
export const getBusinessConfigurationPaymentsIsLoading = createSelector(
    getBusinessConfigurationPaymentsState,
    (payments) => payments.isLoading
);
export const getBusinessConfigurationPaymentsData = createSelector(
    getBusinessConfigurationPaymentsState,
    (payments) => payments.data
);
export const getBusinessConfigurationPaymentsSuccess = createSelector(
    getBusinessConfigurationPaymentsState,
    (payments) => payments.success
);
export const getBusinessConfigurationPaymentsError = createSelector(
    getBusinessConfigurationPaymentsState,
    (payments) => payments.error
);

//// --> BUSINESS CONFIGURATION: SOCIAL NETWORKS
export const getBusinessConfigurationSocialNetworksState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.socialNetworksData
);
export const getBusinessConfigurationSocialNetworksIsLoading = createSelector(
    getBusinessConfigurationSocialNetworksState,
    (socialNetworks) => socialNetworks.isLoading
);
export const getBusinessConfigurationSocialNetworksData = createSelector(
    getBusinessConfigurationSocialNetworksState,
    (socialNetworks) => socialNetworks.data
);
export const getBusinessConfigurationSocialNetworksSuccess = createSelector(
    getBusinessConfigurationSocialNetworksState,
    (socialNetworks) => socialNetworks.success
);
export const getBusinessConfigurationSocialNetworksError = createSelector(
    getBusinessConfigurationSocialNetworksState,
    (socialNetworks) => socialNetworks.error
);

//// --> BUSINESS CONFIGURATION: GMB STATUS
export const getBusinessConfigurationGmbStatusState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.gmbStatusData
);
export const getBusinessConfigurationGmbStatusIsLoading = createSelector(
    getBusinessConfigurationGmbStatusState,
    (gmbStatus) => gmbStatus.isLoading
);
export const getBusinessConfigurationGmbStatusData = createSelector(
    getBusinessConfigurationGmbStatusState,
    (gmbStatus) => gmbStatus.data
);
export const getBusinessConfigurationGmbStatusSuccess = createSelector(
    getBusinessConfigurationGmbStatusState,
    (gmbStatus) => gmbStatus.success
);
export const getBusinessConfigurationGmbStatusError = createSelector(
    getBusinessConfigurationGmbStatusState,
    (gmbStatus) => gmbStatus.error
);

//// --> BUSINESS CONFIGURATION: NOTIFICATIONS
export const getBusinessConfigurationNotificationsState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.notificationsData
);
export const getBusinessConfigurationNotificationsIsLoading = createSelector(
    getBusinessConfigurationNotificationsState,
    (notifications) => notifications.isLoading
);
export const getBusinessConfigurationNotificationsData = createSelector(
    getBusinessConfigurationNotificationsState,
    (notifications) => notifications.data
);
export const getBusinessConfigurationNotificationsSuccess = createSelector(
    getBusinessConfigurationNotificationsState,
    (notifications) => notifications.success
);
export const getBusinessConfigurationNotificationsError = createSelector(
    getBusinessConfigurationNotificationsState,
    (notifications) => notifications.error
);

//// --> BUSINESS CONFIGURATION: WEBSITE
export const getBusinessConfigurationWebsiteState = createSelector(
    getBusinessConfigurationState,
    (businessConfig) => businessConfig.websiteData
);
export const getBusinessConfigurationWebsiteIsLoading = createSelector(
    getBusinessConfigurationWebsiteState,
    (website) => website.isLoading
);
export const getBusinessConfigurationWebsiteData = createSelector(
    getBusinessConfigurationWebsiteState,
    (website) => website.data
);
export const getBusinessConfigurationWebsiteSuccess = createSelector(
    getBusinessConfigurationWebsiteState,
    (website) => website.success
);
export const getBusinessConfigurationWebsiteError = createSelector(
    getBusinessConfigurationWebsiteState,
    (website) => website.error
);
