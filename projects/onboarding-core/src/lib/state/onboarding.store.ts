import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
    OnboardingBusinessConfigGmbStatusDataProps,
    OnboardingBusinessConfigPaymentsDataProps,
    OnboardingBusinessConfigScheduleDataProps,
    OnboardingBusinessConfigServicesDataProps,
    OnboardingBusinessConfigSocialNetworksDataProps,
    OnboardingBusinessConfigWebsiteDataProps,
} from '../models/BusinessConfig.model';
import {
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
    OnboardingRegistrationUserDataProps,
} from '../models/Registration.model';
import * as fromBusinessConfigActions from './business-config.actions';
import * as fromReducer from './onboarding.reducer';
import * as fromSelectors from './onboarding.selectors';
import * as fromRegistrationActions from './registration.actions';

@Injectable()
export class OnboardingStore {
    constructor(public store: Store<fromReducer.IOnboardingState>) {}

    // ----------- REGISTRATION METHODS: ---------------------
    get registrationState$() {
        return this.store.select(fromSelectors.getRegistrationState);
    }

    //// USER DATA:
    SaveUserData = (payload: OnboardingRegistrationUserDataProps) => {
        this.store.dispatch(
            fromRegistrationActions.SaveUserDataBeginAction({ payload })
        );
    };
    get registrationUserState$() {
        return this.store.select(fromSelectors.getRegistrationUserState);
    }
    get registrationUserIsLoading$() {
        return this.store.select(fromSelectors.getRegistrationUserIsLoading);
    }
    get registrationUserData$() {
        return this.store.select(fromSelectors.getRegistrationUserData);
    }
    get registrationUserSuccess$() {
        return this.store.select(fromSelectors.getRegistrationUserSuccess);
    }
    get registrationUserError$() {
        return this.store.select(fromSelectors.getRegistrationUserError);
    }

    //// GMB DATA
    SaveGmbData = (payload: OnboardingRegistrationGmbDataProps) => {
        this.store.dispatch(
            fromRegistrationActions.SaveGmbDataBeginAction({ payload })
        );
    };
    get isLoadingRegistrationGmbState$() {
        return this.store.select(fromSelectors.getRegistrationGmbState);
    }
    get registrationGmbIsLoading$() {
        return this.store.select(fromSelectors.getRegistrationGmbIsLoading);
    }
    get registrationGmbData$() {
        return this.store.select(fromSelectors.getRegistrationGmbData);
    }
    get registrationGmbSuccess$() {
        return this.store.select(fromSelectors.getRegistrationGmbSuccess);
    }
    get registrationGmbError$() {
        return this.store.select(fromSelectors.getRegistrationGmbError);
    }

    //// SUBSCRIPTION DATA
    saveSubscriptionData = (
        payload: OnboardingRegistrationSubscriptionDataProps
    ) => {
        this.store.dispatch(
            fromRegistrationActions.SaveSubscriptionDataBeginAction({ payload })
        );
    };
    get registrationSubscriptionState$() {
        return this.store.select(
            fromSelectors.getRegistrationSubscriptionState
        );
    }
    get registrationSubscriptionIsLoading$() {
        return this.store.select(
            fromSelectors.getRegistrationSubscriptionIsLoading
        );
    }
    get registrationSubscriptionData$() {
        return this.store.select(fromSelectors.getRegistrationSubscriptionData);
    }
    get registrationSubscriptionSuccess$() {
        return this.store.select(
            fromSelectors.getRegistrationSubscriptionSuccess
        );
    }
    get registrationSubscriptionError$() {
        return this.store.select(
            fromSelectors.getRegistrationSubscriptionError
        );
    }

    // ----------- BUSINESS CONFIG METHODS: ---------------------------
    //// SERVICES
    SaveServicesData = (payload: OnboardingBusinessConfigServicesDataProps) => {
        this.store.dispatch(
            fromBusinessConfigActions.SaveServicesDataBeginAction({ payload })
        );
    };
    get businessConfigServicesState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationServicesState
        );
    }
    get businessConfigServicesIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationServicesIsLoading
        );
    }
    get businessConfigServicesData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationServicesData
        );
    }
    get businessConfigServicesSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationServicesSuccess
        );
    }
    get businessConfigServicesError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationServicesSerror
        );
    }

    //// SCHEDULE
    SaveScheduleData = (payload: OnboardingBusinessConfigScheduleDataProps) => {
        this.store.dispatch(
            fromBusinessConfigActions.SaveScheduleDataBeginAction({ payload })
        );
    };
    get businessConfigScheduleState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationScheduleState
        );
    }
    get businessConfigScheduleIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationScheduleIsLoading
        );
    }
    get businessConfigScheduleData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationScheduleData
        );
    }
    get businessConfigScheduleSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationScheduleSuccess
        );
    }
    get businessConfigScheduleError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationScheduleError
        );
    }

    //// PAYMENTS
    SavePaymentsData = (payload: OnboardingBusinessConfigPaymentsDataProps) => {
        this.store.dispatch(
            fromBusinessConfigActions.SavePaymentsDataBeginAction({ payload })
        );
    };
    get businessConfigPaymentsState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationPaymentsState
        );
    }
    get businessConfigPaymentsIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationPaymentsIsLoading
        );
    }
    get businessConfigPaymentsData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationPaymentsData
        );
    }
    get businessConfigPaymentsSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationPaymentsSuccess
        );
    }
    get businessConfigPaymentsError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationPaymentsError
        );
    }

    //// SOCIAL NEETWORKS
    SaveSocialNetworksData = (
        payload: OnboardingBusinessConfigSocialNetworksDataProps
    ) => {
        this.store.dispatch(
            fromBusinessConfigActions.SaveSocialNetworksDataBeginAction({
                payload,
            })
        );
    };
    get businessConfigSocialNetworksState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationSocialNetworksState
        );
    }
    get businessConfigSocialNetworksIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationSocialNetworksIsLoading
        );
    }
    get registrationSocialNetworksData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationSocialNetworksData
        );
    }
    get businessConfigSocialNetworksSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationSocialNetworksSuccess
        );
    }
    get businessConfigSocialNetworksError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationSocialNetworksError
        );
    }

    //// GMB STATUS
    SaveGmbStatusData = (
        payload: OnboardingBusinessConfigGmbStatusDataProps
    ) => {
        this.store.dispatch(
            fromBusinessConfigActions.SaveGmbStatusDataBeginAction({ payload })
        );
    };
    get businessConfigGmbStatusState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationGmbStatusState
        );
    }
    get businessConfigGmbStatusIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationGmbStatusIsLoading
        );
    }
    get businessConfigGmbStatusData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationGmbStatusData
        );
    }
    get businessConfigGmbStatusSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationGmbStatusSuccess
        );
    }
    get businessConfigGmbStatusError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationGmbStatusError
        );
    }

    //// WEBSITE
    SaveWebsiteData = (payload: OnboardingBusinessConfigWebsiteDataProps) => {
        this.store.dispatch(
            fromBusinessConfigActions.SaveWebsiteDataBeginAction({ payload })
        );
    };
    get businessConfigWebsiteState$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationWebsiteState
        );
    }
    get businessConfigWebsiteIsLoading$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationWebsiteIsLoading
        );
    }
    get businessConfigWebsiteData$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationWebsiteData
        );
    }
    get businessConfigWebsiteSuccess$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationWebsiteSuccess
        );
    }
    get businessConfigWebsiteError$() {
        return this.store.select(
            fromSelectors.getBusinessConfigurationWebsiteError
        );
    }
}
