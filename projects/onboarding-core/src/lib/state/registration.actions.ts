import { createAction, props } from '@ngrx/store';
import {
    OnboardingRegistrationGmbDataProps,
    OnboardingRegistrationSubscriptionDataProps,
    OnboardingRegistrationUserDataProps,
} from '../models/Registration.model';

export enum OnboardingRegistrationActionType {
    SaveUserDataBegin = '[ONBOARDING] Save User Data Begin',
    SaveUserDataSuccess = '[ONBOARDING] Save User Data Success',
    SaveUserDataFail = '[ONBOARDING] Save User Data Fail',

    SaveGmbDataBegin = '[ONBOARDING] Save Gmb Data Begin',
    SaveGmbDataSuccess = '[ONBOARDING] Save Gmb Data Success',
    SaveGmbDataFail = '[ONBOARDING] Save Gmb Data Fail',

    SaveSubscriptionDataBegin = '[ONBOARDING] Save Subscription Data Begin',
    SaveSubscriptionDataSuccess = '[ONBOARDING] Save Subscription Data Success',
    SaveSubscriptionDataFail = '[ONBOARDING] Save Subscription Data Fail',

    ClearOnboardingRegistraionSuccessErrorData = '[ONBOARDING] Onboarding Registraion Success/Error Data',
}

export const SaveUserDataBeginAction = createAction(
    OnboardingRegistrationActionType.SaveUserDataBegin,
    props<{ payload: OnboardingRegistrationUserDataProps }>()
);

export const SaveUserDataSuccessAction = createAction(
    OnboardingRegistrationActionType.SaveUserDataSuccess,
    props<{ payload: any }>()
);

export const SaveUserDataFailAction = createAction(
    OnboardingRegistrationActionType.SaveUserDataFail,
    props<{ errors: any }>()
);

// -----------------------------------------------------

export const SaveGmbDataBeginAction = createAction(
    OnboardingRegistrationActionType.SaveGmbDataBegin,
    props<{ payload: OnboardingRegistrationGmbDataProps }>()
);

export const SaveGmbDataSuccessAction = createAction(
    OnboardingRegistrationActionType.SaveGmbDataSuccess,
    props<{ payload: any }>()
);

export const SaveGmbDataFailAction = createAction(
    OnboardingRegistrationActionType.SaveGmbDataFail,
    props<{ errors: any }>()
);

// -----------------------------------------------------

export const SaveSubscriptionDataBeginAction = createAction(
    OnboardingRegistrationActionType.SaveSubscriptionDataBegin,
    props<{ payload: OnboardingRegistrationSubscriptionDataProps }>()
);

export const SaveSubscriptionDataSuccessAction = createAction(
    OnboardingRegistrationActionType.SaveSubscriptionDataSuccess,
    props<{ payload: any }>()
);

export const SaveSubscriptionDataFailAction = createAction(
    OnboardingRegistrationActionType.SaveSubscriptionDataFail,
    props<{ errors: any }>()
);

export const ClearOnboardingRegistraionSuccessErrorDataAction = createAction(
    OnboardingRegistrationActionType.ClearOnboardingRegistraionSuccessErrorData
);
