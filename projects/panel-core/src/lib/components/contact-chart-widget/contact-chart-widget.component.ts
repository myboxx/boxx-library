import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'boxx-contact-chart-widget',
  templateUrl: './contact-chart-widget.component.html',
  styleUrls: ['./contact-chart-widget.component.scss'],
})
export class ContactChartWidgetComponent implements OnInit {
    @Input() stats: {
        total: number;
        client: number;
        prospect: number;
        not_specified: number
    };

    @Input() labels: {
        contacts: string;
        not_specified: string;
        prospect: string;
        client: string;
    };

constructor() { }

ngOnInit() {}

}
