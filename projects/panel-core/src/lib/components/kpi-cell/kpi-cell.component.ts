import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'boxx-kpi-cell',
  templateUrl: './kpi-cell.component.html',
  styleUrls: ['./kpi-cell.component.scss'],
})
export class KpiCellComponent implements OnInit {
  @Input() className: string;
  @Input() iconName: string;
  @Input() iconSrc: string;
  @Input() label: string;
  @Input() value: number;

  constructor() { }

  ngOnInit() {}

}
