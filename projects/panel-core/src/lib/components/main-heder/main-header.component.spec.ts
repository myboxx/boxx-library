import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MainHederComponent } from './main-header.component';

describe('MainHederComponent', () => {
  let component: MainHederComponent;
  let fixture: ComponentFixture<MainHederComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainHederComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MainHederComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
