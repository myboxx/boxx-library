import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-panel-widget',
    templateUrl: './panel-widget.component.html',
    styleUrls: ['./panel-widget.component.scss'],
})
export class PanelWidgetComponent implements OnInit {
    @Input() title: string;

    constructor() { }

    ngOnInit() { }

}
