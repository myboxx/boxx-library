import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { QrPopoverComponent } from './qr-popover.component';

@Component({
    selector: 'boxx-qr-component',
    templateUrl: './qr-component.html',
    styleUrls: ['./qr-component.scss']
})
export class QrComponent implements OnInit, OnDestroy {
    @Input() isLoading$: Observable<boolean>;
    @Input() websiteUrl: string;

    constructor(
        private popoverController: PopoverController
    ) { }


    destroyed$ = new Subject<boolean>();

    ngOnInit() {

    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    async openQRPopover() {
        const popover = await this.popoverController.create({
            component: QrPopoverComponent,
            componentProps: {
                websiteUrlString: this.websiteUrl
            }
        });
        await popover.present();
    }
}
