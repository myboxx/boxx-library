import { Component, Input, OnInit } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController, LoadingController, Platform, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'boxx-qr-popover',
    templateUrl: './qr-popover.component.html',
    styleUrls: ['./qr-popover.component.scss'],
})
export class QrPopoverComponent implements OnInit {
    @Input() websiteUrlString: string;

    constructor(
        public platform: Platform,
        private popoverCtrl: PopoverController,
        private file: File,
        private loaderCtrl: LoadingController,
        private alertCtrl: AlertController,
        private actionSheetController: ActionSheetController,
        private translate: TranslateService
    ) {

        this.translate.get(['GENERAL', 'QR'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);

        this.platform.ready().then(() => {
            this.isCordovaOrCapacitor = this.platform.is('cordova') || this.platform.is('capacitor');

            if (this.platform.is('android')) {
                this.isAndroid = true;
                this.folderpath = this.file.externalRootDirectory;
            }
            else {
                this.isAndroid = false;
                this.folderpath = this.file.documentsDirectory;
            }
        });
    }

    isCordovaOrCapacitor = false;

    elementType = 'url';
    folderpath: string;
    isAndroid: boolean;

    translations: any;

    destroyed$ = new Subject<boolean>();

    ngOnInit() {
        setTimeout(() => {
            const qrMainContainer = document.getElementsByTagName('ngx-qrcode')[0];

            if (!qrMainContainer) { return console.error('No hay contenedor para la imagen QR'); }

            const qrImgContainer = qrMainContainer.firstElementChild;

            if (!qrImgContainer) { return console.error('No hay imagen QR'); }


            const child = qrImgContainer.firstElementChild;

            if (child) {
                const src = child.getAttribute('src');

                if (src) {
                    const a = document.getElementById('downladableQr'); // Create <a>
                    a.setAttribute('src', src);
                }
                else {
                    console.error('--- No img src found');
                }
            }
            else {
                console.error('--- Element not found');
            }
        }, 900);
    }

    close() {
        this.popoverCtrl.dismiss();
    }

    async saveImage(src: HTMLElement) {
        let option: 500 | 700 | 1200;
        const actionSheet = await this.actionSheetController.create({
            header: this.translations.GENERAL.ACTION.options,
            buttons: [{
                text: '500 x 500',
                handler: () => { option = 500; }
            },
            {
                text: '700 x 700',
                handler: () => { option = 700; }
            },
            {
                text: '1200 x 1200',
                handler: () => { option = 1200; }
            }]
        });

        actionSheet.present();

        await actionSheet.onDidDismiss();

        if (!option) { return; }


        const loader = await this.loaderCtrl.create();

        loader.present();

        const scaledImg = await this.scaleImage(src.getAttribute('src'), option, option)
            .catch(error => {
                this.alertCtrl.create({
                    header: this.translations.GENERAL.error,
                    message: this.translations.QR.scalingError + '<br>' + JSON.stringify(error),
                    buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
                });

                loader.dismiss();
            });

        if (!scaledImg) { return; }


        const block = (scaledImg || '').split(';');

        // Get the content type
        const dataType = block[0].split(':')[1];

        // get the real base64 content of the file
        const realData = block[1].split(',')[1];

        // The name of your file, note that you need to know if is .png,.jpeg etc
        const filename = `MY_WEBSITE_${option}x${option}.${dataType.split('/')[1]}`;

        this.saveBase64(this.folderpath, realData, filename, dataType).then(async path => {
            this.close();

            this.alertCtrl.create({
                header: this.translations.QR.completed,
                message: await this.translate.get('QR.fileDownloadedOk', { filename }).toPromise(),
                buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
            }).then(a => a.present());
        }, error => {
            console.error('saveBase64: error', error);
            this.alertCtrl.create({
                header: this.translations.GENERAL.error,
                message: this.translations.QR.fileDownloadError + '<br>' + JSON.stringify(error),
                buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
            }).then(a => a.present());
        }).finally(() => this.loaderCtrl.dismiss());
    }

    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data Pure base64 string without contentType
     * @param contentType the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    b64toBlob(b64Data, contentType, sliceSize?) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    public saveBase64(pictureDir: string, content: string, name: string, dataType: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const blob = this.b64toBlob(content, dataType);


            if (!this.isCordovaOrCapacitor){
                this.downloadFile(blob, name, dataType);
                return resolve(pictureDir + name);
            }

            this.file.writeFile(pictureDir, name, blob, { replace: true })
                .then(() => resolve(pictureDir + name))
                .catch((err) => {
                    console.error('error writing blob. ERROR: ', err);
                    reject(err);
                });
        });
    }

    scaleImage(base64Data: string, width: number, height: number): Promise<string> {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                // We create a canvas and get its context.
                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');

                // We set the dimensions at the wanted size.
                canvas.width = width;
                canvas.height = height;

                // We resize the image with the canvas method drawImage();
                ctx.drawImage(img, 0, 0, width, height);

                resolve(canvas.toDataURL());
            };

            img.onerror = ((e) => {
                reject(e.toString());
            });

            img.src = base64Data;
        });
    }

    // source: https://davidwalsh.name/javascript-download
    downloadFile(data, fileName, type = 'text/plain') {
        // Create an invisible A element
        const a = document.createElement('a');
        a.style.display = 'none';
        document.body.appendChild(a);

        // Set the HREF to a Blob representation of the data to be downloaded
        a.href = window.URL.createObjectURL(
            new Blob([data], { type })
        );

        // Use download attribute to set set desired file name
        a.setAttribute('download', fileName);

        // Trigger the download by simulating click
        a.click();

        // Cleanup
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);
    }
}
