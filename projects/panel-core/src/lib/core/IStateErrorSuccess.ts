import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';

export interface IPanelStateError extends IStateErrorBase {
    after: 'GET' | 'UNKNOWN';
}

export interface IPanelStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'UNKNOWN';
}
