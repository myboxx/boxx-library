import { IPanelApiResponse } from '../repositories/IPanel.repository';
import { LeadModel } from '@boxx/messages-core';
import { EventModel } from '@boxx/events-core';
import { ContactModel } from '@boxx/contacts-core';


export class PanelPageModel implements IPanelProps {

    constructor(data: IPanelProps) {
        this.websiteData = data.websiteData;
        this.contactsData = data.contactsData;
        this.messagesData = data.messagesData;
        this.eventsData = data.eventsData;
    }

    websiteData: { pageViews: number; };
    contactsData: { lastContacts: ContactModel[]; stats: { total: number; client: number; prospect: number; notSpecified: number; }; };
    messagesData: { lastMessages: LeadModel[]; stats: { total: number; read: number; unread: number; }; };
    eventsData: { upcomingEvents: EventModel[]; stats: { total: number; }; };


    static fromApiResponse(data: IPanelApiResponse): PanelPageModel {

        const lastContacts = data.contacts.recent_contacts.map(c => ContactModel.fromDataResponse(c));
        const lastMessages = data.form_messages.last_received.map(m => LeadModel.fromApiResponse(m));
        const upcomingEvents = data.schedule.upcoming_events.map(e => EventModel.fromDataResponse(e))
        .sort((a, b) => a.datetimeFrom.localeCompare(b.datetimeFrom));

        return new PanelPageModel({
            websiteData: { pageViews: +data.analytics.page_views },
            contactsData: {
                lastContacts,
                stats: {
                    total: +data.contacts.stats.total,
                    client: +data.contacts.stats.clients,
                    prospect: +data.contacts.stats.prospects,
                    notSpecified: +data.contacts.stats.not_specified
                }
            },
            messagesData: {
                lastMessages,
                stats: {
                    total: +data.form_messages.stats.total,
                    read: +data.form_messages.stats.read,
                    unread: +data.form_messages.stats.unread,
                }
            },
            eventsData: {
                upcomingEvents,
                stats: { total: +data.schedule.stats.total }
            }
        });
    }

    static empty(): PanelPageModel {
        return new PanelPageModel({
            websiteData: { pageViews: 0 },
            contactsData: {
                lastContacts: [],
                stats: {
                    total: 0,
                    client: 0,
                    prospect: 0,
                    notSpecified: 0
                }
            },
            messagesData: {
                lastMessages: [],
                stats: {
                    total: 0,
                    read: 0,
                    unread: 0,
                }
            },
            eventsData: {
                upcomingEvents: [],
                stats: { total: 0 }
            }
        });
    }
}

export interface IPanelProps {
    websiteData: { pageViews: number };
    contactsData: {
        lastContacts: Array<ContactModel>
        stats: {
            total: number
            client: number
            prospect: number,
            notSpecified: number
        }
    };
    messagesData: {
        lastMessages: Array<LeadModel>
        stats: {
            total: number
            read: number
            unread: number
        }
    };
    eventsData: {
        upcomingEvents: Array<EventModel>
        stats: { total: number }
    };
}
