import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { ContactChartWidgetComponent } from './components/contact-chart-widget/contact-chart-widget.component';
import { KpiCellComponent } from './components/kpi-cell/kpi-cell.component';
import { MainHeaderComponent } from './components/main-heder/main-header.component';
import { PanelWidgetComponent } from './components/panel-widget/panel-widget.component';
import { QrComponent } from './components/qr-component/qr-component';
import { QrPopoverComponent } from './components/qr-component/qr-popover.component';
import { PANEL_REPOSITORY } from './repositories/IPanel.repository';
import { PanelRepository } from './repositories/panel.repository';
import { PANEL_SERVICE } from './services/IPanel.service';
import { PanelService } from './services/Panel.service';
import { PanelEffects } from './state/panel.effects';
import { panelReducer } from './state/panel.reducer';
import { PanelStore } from './state/panel.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
  declarations: [
    ContactChartWidgetComponent,
    KpiCellComponent,
    MainHeaderComponent,
    PanelWidgetComponent,
    QrComponent,
    QrPopoverComponent,
  ],
  imports: [
    HttpClientModule,
    StoreModule.forFeature('panel', panelReducer),
    EffectsModule.forFeature([PanelEffects]),
    TranslateModule.forChild(),
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule
  ],
  exports: [
    ContactChartWidgetComponent,
    KpiCellComponent,
    MainHeaderComponent,
    PanelWidgetComponent,
    QrComponent,
    QrPopoverComponent,
  ]
})
export class PanelCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<PanelCoreModule> {
        return {
            ngModule: PanelCoreModule,
            providers: [
                { provide: PANEL_SERVICE, useClass: PanelService },
                { provide: PANEL_REPOSITORY, useClass: PanelRepository },
                ...config.providers,
                PanelStore
            ]
        };
    }
}
