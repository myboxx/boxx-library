import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { IPanelApiResponse, IPanelRepository } from './IPanel.repository';

@Injectable()
export class PanelRepository implements IPanelRepository {

    constructor(
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient,
    ) { }

    getPanelData(): Observable<IHttpBasicResponse<IPanelApiResponse>> {
        // console.log("--- EXECUTING PanelRepository.getPanelData()");
        return this.httpClient.get<IHttpBasicResponse<IPanelApiResponse>>(`${this.getBaseUrl()}`);
    }

    private getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/home`;
    }
}
