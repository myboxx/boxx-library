import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { PanelPageModel } from '../models/panel.model';

export interface IPanelService {
    getPanelData(): Observable<PanelPageModel>;
}

export const PANEL_SERVICE = new InjectionToken<IPanelService>('panelService');
