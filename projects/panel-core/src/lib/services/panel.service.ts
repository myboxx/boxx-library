import { Inject, Injectable } from '@angular/core';
import { IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PanelPageModel } from '../models/panel.model';
import { IPanelApiResponse, IPanelRepository, PANEL_REPOSITORY } from '../repositories/IPanel.repository';
import { IPanelService } from './IPanel.service';


@Injectable({
    providedIn: 'root'
})
export class PanelService implements IPanelService {
    constructor(
        @Inject(PANEL_REPOSITORY) private repository: IPanelRepository
    ) { }

    getPanelData(): Observable<PanelPageModel> {
        return this.repository.getPanelData().pipe(
            map((response: IHttpBasicResponse<IPanelApiResponse>) => {

                return PanelPageModel.fromApiResponse(response.data);
            }),
            catchError(error => {
                throw error;
            })
        );
    }
}
