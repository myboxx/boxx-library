import { createAction, props } from '@ngrx/store';

export enum PanelActionTypes {
    LoadPanelBegin = '[PANEL] Load Panel Begin',
    LoadPanelSuccess = '[PANEL] Load Panel Success',
    LoadPanelFail = '[PANEL] Load Panel Fail',
}

export const LoadPanelBeginAction = createAction(
    PanelActionTypes.LoadPanelBegin
);

export const LoadPanelSuccessAction = createAction(
    PanelActionTypes.LoadPanelSuccess,
    props<{ payload: any }>()
);

export const LoadPanelFailAction = createAction(
    PanelActionTypes.LoadPanelFail,
    props<{ errors: any }>()
);
