import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { IPanelService, PANEL_SERVICE } from '../services/IPanel.service';
import * as PanelActions from './panel.actions';

@Injectable()
export class PanelEffects {
    loadPanel$ = createEffect(
        () => this.actions$.pipe(
            ofType(PanelActions.PanelActionTypes.LoadPanelBegin),
            switchMap(() => {
                return this.service.getPanelData().pipe(
                    map(panel => PanelActions.LoadPanelSuccessAction({ payload: panel })),
                    catchError(error => {
                        console.error('Couldn\'t load main panel', error);
                        return of(PanelActions.LoadPanelFailAction({ errors: error }));
                    })
                );
            })
        )
    );

    constructor(
        private actions$: Actions,
        @Inject(PANEL_SERVICE) private service: IPanelService
    ) { }
}
