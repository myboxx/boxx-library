import { createReducer, on, Action, ActionReducer } from '@ngrx/store';
import { IPanelStateError, IPanelStateSuccess } from '../core/IStateErrorSuccess';
import { PanelPageModel } from '../models/panel.model';
import * as fromActions from './panel.actions';


export interface PanelState {
    isLoading: boolean;
    hasBeenFetched: boolean;
    data: PanelPageModel;
    error: IPanelStateError;
    success: IPanelStateSuccess;
}

export const initialState: PanelState = {
    isLoading: false,
    hasBeenFetched: false,
    data: PanelPageModel.empty(),
    error: null,
    success: null
};

const reducer: ActionReducer<PanelState> = createReducer(
    initialState,
    on(fromActions.LoadPanelBeginAction, (state): PanelState => ({
        ...state,
        error: null,
        success: null,
        isLoading: true,
    })),
    on(fromActions.LoadPanelSuccessAction, (state, action): PanelState => ({
        ...state,
        hasBeenFetched: true,
        data: action.payload,
        isLoading: false,
        error: null,
        success: { after: getSuccessActionType(action.type) }
    })),
    on(fromActions.LoadPanelFailAction, (state, action): PanelState => ({
        ...state,
        isLoading: false,
        error: { after: getErrorActionType(action.type), error: action.errors }
    }))
);

function getErrorActionType(type: fromActions.PanelActionTypes) {

    let action: 'GET' | 'UNKNOWN' = 'UNKNOWN';

    switch (type) {
        case fromActions.PanelActionTypes.LoadPanelFail:
            action = 'GET'; break;
    }

    return action;
}

function getSuccessActionType(type: fromActions.PanelActionTypes) {

    let action: 'GET' | 'UNKNOWN' = 'UNKNOWN';

    switch (type) {
        case fromActions.PanelActionTypes.LoadPanelSuccess:
            action = 'GET'; break;
    }

    return action;
}

export function panelReducer(state: PanelState | undefined, action: Action) {
    return reducer(state, action);
}
