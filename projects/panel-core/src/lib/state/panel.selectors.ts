import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPanelReducer from './panel.reducer';


export const getPanelState = createFeatureSelector<fromPanelReducer.PanelState>('panel');

export const getPanelPageState = createSelector(
    getPanelState,
    state => state
);

const stateGetIsLoading = (state: fromPanelReducer.PanelState) => state.isLoading;

export const stateGetData = (state: fromPanelReducer.PanelState) => state.data;

export const getIsLoading = createSelector(
    getPanelPageState,
    stateGetIsLoading
);

export const getError = createSelector(
    getPanelPageState,
    state => state.error
);

export const getSuccess = createSelector(
    getPanelPageState,
    state => state.success
);

export const getData = createSelector(
    getPanelPageState,
    stateGetData
);

export const hasBeenFetched = createSelector(
    getPanelPageState,
    state => state.hasBeenFetched
);
