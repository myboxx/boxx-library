import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSelector from './panel.selectors';
import * as fromActions from './panel.actions';
import * as fromReducer from './panel.reducer';

@Injectable()
export class PanelStore {
    constructor(private store: Store<fromReducer.PanelState>) { }

    get Loading$() { return this.store.select(fromSelector.getIsLoading); }

    get Error$() { return this.store.select(fromSelector.getError); }

    get Success$() { return this.store.select(fromSelector.getSuccess); }

    loadPanel() {
        return this.store.dispatch(fromActions.LoadPanelBeginAction());
    }

    get Panel$() {
        return this.store.select(fromSelector.getData);
    }

    get HasBeenFetched$() {
        return this.store.select(fromSelector.hasBeenFetched);
    }
}
