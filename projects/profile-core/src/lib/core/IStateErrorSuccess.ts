import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';

export interface IProfileStateError extends IStateErrorBase {
    after: 'GET' | 'UPDATE' | 'UNKNOWN';
}

export interface IProfileStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'UPDATE' | 'UNKNOWN';
}
