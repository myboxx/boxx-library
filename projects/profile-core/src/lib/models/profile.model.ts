import { IGetProfileDataResponse } from '../repositories/IProfile.repository';

export class ProfileModel implements IProfileModelProps{

    constructor(data: IProfileModelProps){
        this.businessName = data.businessName;
        this.activeNewsletter = data.activeNewsletter;
        this.createdAt = data.createdAt;
        this.clientEmail = data.clientEmail;
        this.clientId = data.clientId;
        this.name = data.name;
        this.phoneNumber = data.phoneNumber;
        this.userEmail = data.userEmail;
        this.userId = data.userId;
    }

    activeNewsletter: boolean;
    businessName: string;
    createdAt: string;
    clientEmail: string;
    clientId: number;
    name: string;
    phoneNumber: string;
    userEmail: string;
    userId: number;

    static toModel(data: IGetProfileDataResponse): ProfileModel{
        return new ProfileModel({
            activeNewsletter: data.active_newsletter === '1',
            businessName: data.business_name,
            createdAt: data.created_at,
            clientEmail: data.client_email,
            clientId: +data.client_id,
            name: data.name,
            phoneNumber: data.phone_number !== 'null' ? data.phone_number : null,
            userEmail: data.user_email,
            userId: +data.user_id
        });
    }

    static empty(): ProfileModel{
        return new ProfileModel({
            activeNewsletter: null,
            businessName: null,
            createdAt: null,
            clientEmail: null,
            clientId: null,
            name: null,
            phoneNumber: null,
            userEmail: null,
            userId: null
        });
    }
}

export interface IProfileModelProps{
    activeNewsletter: boolean;
    businessName: string;
    createdAt: string;
    clientEmail: string;
    clientId: number;
    name: string;
    phoneNumber: string;
    userEmail: string;
    userId: number;
}
