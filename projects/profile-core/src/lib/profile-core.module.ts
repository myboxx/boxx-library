import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PROFILE_REPOSITORY } from './repositories/IProfile.repository';
import { ProfileRepository } from './repositories/profile.repository';
import { PROFILE_SERVICE } from './services/IProfile.service';
import { ProfileService } from './services/profile.service';
import { ProfileEffects } from './state/profile.effects';
import { profileReducer } from './state/profile.reducer';
import { ProfileStore } from './state/profile.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
  declarations: [

  ],
  imports: [
    HttpClientModule,
    StoreModule.forFeature('profile', profileReducer),
    EffectsModule.forFeature([ProfileEffects]),
  ],
  exports: [

  ]
})
export class ProfileCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<ProfileCoreModule> {
        return {
            ngModule: ProfileCoreModule,
            providers: [
                { provide: PROFILE_SERVICE, useClass: ProfileService },
                { provide: PROFILE_REPOSITORY, useClass: ProfileRepository },
                ...config.providers,
                ProfileStore
            ]
        };
    }
 }
