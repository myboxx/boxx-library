import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpBasicResponse } from '@boxx/core';

export interface IProfileRepository {
    getProfile(): Observable<IHttpBasicResponse<IGetProfileDataResponse>>;

    updateProfile(profileForm: IUpdateProfilePayload): Observable<IHttpBasicResponse<IUpdateProfileDataResponse>>;
}

export const PROFILE_REPOSITORY = new InjectionToken<IProfileRepository>('profileRepository');

export interface IGetProfileDataResponse {
    active_newsletter: string; // "1" | "0"
    business_name: string;
    client_email: string;
    client_id: string;
    created_at: string;
    name: string;
    phone_number: string;
    user_email: string;
    user_id: string;
}

export interface IUpdateProfilePayload {
    name: string;
    email: string;
    phoneNumber: string;
    businessName: string;
}

export interface IUpdateProfileDataResponse {
    business_name: string;
    email: string;
    id: string; // as number
    language: string;
    name: string;
    role_name: string;
}
