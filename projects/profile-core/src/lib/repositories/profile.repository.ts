import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import {
    IGetProfileDataResponse,
    IProfileRepository,
    IUpdateProfileDataResponse,
    IUpdateProfilePayload
} from './IProfile.repository';

@Injectable()
export class ProfileRepository implements IProfileRepository {

    constructor(
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient,
    ) { }

    getProfile(): Observable<IHttpBasicResponse<IGetProfileDataResponse>> {

        return this.httpClient.get<IHttpBasicResponse<IGetProfileDataResponse>>(`${this.getBaseUrl()}/get_profile`);
    }

    updateProfile(profileForm: IUpdateProfilePayload): Observable<IHttpBasicResponse<IUpdateProfileDataResponse>> {

        let params = new HttpParams();
        for (const key in profileForm) {
            if (profileForm.hasOwnProperty(key)) {
                params = params.append(key, profileForm[key]);
            }
        }
        const body = params.toString();

        return this.httpClient.post<IHttpBasicResponse<IUpdateProfileDataResponse>>(`${this.getBaseUrl()}/update`, body);
    }

    private getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/profile`;
    }
}
