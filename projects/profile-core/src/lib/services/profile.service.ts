import { Inject, Injectable } from '@angular/core';
import { IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ProfileModel } from '../models/profile.model';
import {
    IGetProfileDataResponse,
    IProfileRepository,
    IUpdateProfileDataResponse,
    IUpdateProfilePayload,
    PROFILE_REPOSITORY
} from '../repositories/IProfile.repository';
import { IProfileService } from './IProfile.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileService implements IProfileService<ProfileModel>{

    constructor(
        @Inject(PROFILE_REPOSITORY) private repository: IProfileRepository,
    ) { }

    getProfile(): Observable<ProfileModel> {
        return this.repository.getProfile().pipe(
            map((response: IHttpBasicResponse<IGetProfileDataResponse>) => {
                return ProfileModel.toModel(response.data);
            }),
            catchError(error => {
                throw error;
            })
        );
    }

    updateProfile(profileForm: IUpdateProfilePayload): Observable<ProfileModel> {
        return this.repository.updateProfile(profileForm).pipe(
            map((response: IHttpBasicResponse<IUpdateProfileDataResponse>) => {

                return new ProfileModel({
                    activeNewsletter: null,
                    businessName: response.data.business_name,
                    createdAt: null,
                    clientEmail: null,
                    clientId: null,
                    name: response.data.name,
                    phoneNumber: null,
                    userEmail: response.data.email,
                    userId: +response.data.id,
                });
            }),
            catchError(error => {
                throw error;
            })
        );
    }
}
