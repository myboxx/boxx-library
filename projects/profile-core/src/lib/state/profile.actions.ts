import { createAction, props } from '@ngrx/store';
import { ProfileModel } from '../models/profile.model';

export enum ProfileActionTypes {
    LoadProfileBegin = '[PROFILE] Load Profile Begin',
    LoadProfileSuccess = '[PROFILE] Load Profile Success',
    LoadProfileFail = '[PROFILE] Load Profile Fail',

    UpdateProfileBegin = '[PROFILE] Update Profile Begin',
    UpdateProfileSuccess = '[PROFILE] Update Profile Success',
    UpdateProfileFail = '[PROFILE] Update Profile Fail',
}

// GET
export const LoadProfileBeginAction = createAction(
    ProfileActionTypes.LoadProfileBegin
);

export const LoadProfileSuccessAction = createAction(
    ProfileActionTypes.LoadProfileSuccess,
    props<{ payload: ProfileModel }>()
);

export const LoadProfileFailAction = createAction(
    ProfileActionTypes.LoadProfileFail,
    props<{ errors: any }>()
);

// UPDATE
export const UpdateProfileBeginAction = createAction(
    ProfileActionTypes.UpdateProfileBegin,
    props<{ profileForm: { name: string, email: string } }>()
);

export const UpdateProfileSuccessAction = createAction(
    ProfileActionTypes.UpdateProfileSuccess,
    props<{ response: ProfileModel }>()
);

export const UpdateProfileFailAction = createAction(
    ProfileActionTypes.UpdateProfileFail,
    props<{ errors: any }>()
);
