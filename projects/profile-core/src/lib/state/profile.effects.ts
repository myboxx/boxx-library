import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ProfileModel } from '../models/profile.model';
import { IProfileService, PROFILE_SERVICE } from '../services/IProfile.service';
import * as ProfileActions from './profile.actions';


@Injectable()
export class ProfileEffects {
    loadProfile$ = createEffect(
        () => this.actions$.pipe(
            ofType(ProfileActions.ProfileActionTypes.LoadProfileBegin),
            switchMap(() => {
                return this.service.getProfile().pipe(
                    map(profile => ProfileActions.LoadProfileSuccessAction({ payload: profile })),
                    catchError(error => {
                        console.error('Couldn\'t load Profile');
                        return of(ProfileActions.LoadProfileFailAction({ errors: error }));
                    })
                );
            })
        )
    );

    updateProfile$ = createEffect(
        () => this.actions$.pipe(
            ofType(ProfileActions.ProfileActionTypes.UpdateProfileBegin),
            switchMap((action) => {
                return this.service.updateProfile((action as any).profileForm).pipe(
                    map(response => ProfileActions.UpdateProfileSuccessAction({ response })),
                    catchError(error => {
                        console.error('Couldn\'t UPDATE Profile', error);
                        return of(ProfileActions.UpdateProfileFailAction({ errors: error }));
                    })
                );
            })
        )
    );

    constructor(
        private actions$: Actions,
        @Inject(PROFILE_SERVICE) private service: IProfileService<ProfileModel>
    ) { }
}
