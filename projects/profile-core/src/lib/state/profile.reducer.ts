import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import { IProfileStateError, IProfileStateSuccess } from '../core/IStateErrorSuccess';
import { ProfileModel } from '../models/profile.model';
import * as fromActions from './profile.actions';

export interface ProfileState {
    isLoading: boolean;
    profile: ProfileModel;
    error: IProfileStateError;
    success: IProfileStateSuccess;
}

export const initialState: ProfileState = {
    isLoading: false,
    profile: ProfileModel.empty(),
    error: null,
    success: null
};

const reducer: ActionReducer<ProfileState> = createReducer(
    initialState,
    on(
        fromActions.LoadProfileBeginAction,
        fromActions.UpdateProfileBeginAction,
        (state): ProfileState => ({
            ...state,
            error: null,
            success: null,
            isLoading: true,
        })),
    on(fromActions.LoadProfileSuccessAction, (state, action): ProfileState => ({
        ...state,
        profile: action.payload,
        isLoading: false,
    })),
    on(
        fromActions.LoadProfileFailAction,
        fromActions.UpdateProfileFailAction,
        (state, action): ProfileState => ({
            ...state,
            isLoading: false,
            error: { after: getErrorActionType(action.type), error: action.errors }
        })),

    on(fromActions.UpdateProfileSuccessAction, (state, action): ProfileState => ({
        ...state,
        profile: action.response,
        isLoading: false,
        success: { after: getSuccessActionType(action.type) }
    }))
);

function getErrorActionType(type: fromActions.ProfileActionTypes) {

    let action: 'GET' | 'UPDATE' | 'UNKNOWN' = 'UNKNOWN';

    switch (type) {
        case fromActions.ProfileActionTypes.LoadProfileFail:
            action = 'GET'; break;
        case fromActions.ProfileActionTypes.UpdateProfileFail:
            action = 'UPDATE'; break;
    }

    return action;
}

function getSuccessActionType(type: fromActions.ProfileActionTypes) {

    let action: 'GET' | 'UPDATE' | 'UNKNOWN' = 'UNKNOWN';

    switch (type) {
        case fromActions.ProfileActionTypes.LoadProfileSuccess:
            action = 'GET'; break;
        case fromActions.ProfileActionTypes.UpdateProfileSuccess:
            action = 'UPDATE'; break;
    }

    return action;
}

export function profileReducer(state: ProfileState | undefined, action: Action) {
    return reducer(state, action);
}
