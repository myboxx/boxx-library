import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromProfileReducer from './profile.reducer';


export const getProfileState = createFeatureSelector<fromProfileReducer.ProfileState>('profile');

export const getProfilePageState = createSelector(
    getProfileState,
    state => state
);

const stateGetIsLoading = (state: fromProfileReducer.ProfileState) => state.isLoading;

export const stateGetProfile = (state: fromProfileReducer.ProfileState) => state.profile;

export const stateGetError = (state: fromProfileReducer.ProfileState) => state.error;

export const getIsLoading = createSelector(
    getProfilePageState,
    stateGetIsLoading
);

export const getProfile = createSelector(
    getProfilePageState,
    stateGetProfile
);

export const getError = createSelector(
    getProfilePageState,
    stateGetError
);

export const getSuccess = createSelector(
    getProfilePageState,
    state => state.success
);
