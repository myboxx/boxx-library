import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromActions from './profile.actions';
import * as fromReducer from './profile.reducer';
import * as fromSelector from './profile.selectors';

@Injectable()
export class ProfileStore {
    constructor(private store: Store<fromReducer.ProfileState>) { }

    get Loading$() { return this.store.select(fromSelector.getIsLoading); }

    get Error$() { return this.store.select(fromSelector.getError); }

    get Success$() { return this.store.select(fromSelector.getSuccess); }

    LoadProfile() {
        return this.store.dispatch(fromActions.LoadProfileBeginAction());
    }

    get Profile$() {
        return this.store.select(fromSelector.getProfile);
    }

    UpdateProfile(profileForm: { name: string, email: string }) {
        return this.store.dispatch(fromActions.UpdateProfileBeginAction({ profileForm }));
    }
}
