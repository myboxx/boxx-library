import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-card-configuration-form',
    templateUrl: './card-configuration-form.component.html',
    styleUrls: ['./card-configuration-form.component.scss'],
})
export class CardConfigurationFormComponent implements OnInit {
    country = { name: '', flag: '' };

    constructor() {}

    ngOnInit() {}

    openCountryPicker() {}
}
