import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-content-header-item',
    templateUrl: './content-header-item.component.html',
    styleUrls: ['./content-header-item.component.scss'],
})
export class ContentHeaderItemComponent implements OnInit {

    constructor() { }

    ngOnInit() { }

}
