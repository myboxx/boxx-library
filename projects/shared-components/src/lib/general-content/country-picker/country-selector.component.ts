import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ICountryCodes } from '@boxx/contacts-core';


@Component({
    selector: 'boxx-country-selector',
    templateUrl: './country-selector.component.html',
    styleUrls: ['./country-selector.component.scss']
})
export class CountryPickerComponent implements OnInit {
    @Input() countries: Array<ICountryCodes> = [];

    items = [];

    constructor(
        public viewController: ModalController
    ) { }

    ngOnInit() {
        this.items = this.countries;
    }

    select(country: ICountryCodes) {
        this.viewController.dismiss(country);
    }

    search(searchTerm: string) {
        this.items = this.countries.filter(country =>
            (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1
        );
    }

    close() {
        this.viewController.dismiss();
    }
}
