import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'boxx-custom-modal',
    templateUrl: './custom-modal.component.html',
    styleUrls: ['./custom-modal.component.scss'],
})
export class CustomModalComponent implements OnInit {

    constructor(private popoverController: ModalController) { }

    ngOnInit() { }

    close() {
        this.popoverController.dismiss();
    }
}
