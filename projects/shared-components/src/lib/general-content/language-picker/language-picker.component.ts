import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-language-picker',
    templateUrl: './language-picker.component.html',
    styleUrls: ['./language-picker.component.scss'],
})
export class LanguagePickerComponent implements OnInit {
    @Input() languageList: Array<{
        id: string;
        name: string;
        description: string;
        selected: boolean;
    }> = [];

    @Input() selectedId: string;

    constructor(private modalCtrl: ModalController) {}

    ngOnInit() {
        this.languageList = [
            {
                id: 'en',
                name: 'English',
                description: 'English (US)',
                selected: false,
            },
            {
                id: 'es',
                name: 'Spanish',
                description: 'Spanish (Latin America)',
                selected: false,
            },
            {
                id: 'pt',
                name: 'Portuguese',
                description: 'Portugués',
                selected: false,
            },
        ];

        this.languageList.some(language => {
            if (language.id === this.selectedId) {
                language.selected = true;
                return true;
            }
        });
    }

    closeView() {
        this.modalCtrl.dismiss();
    }

    selectLanguage(language: { id: string; name: string; description: string; selected: boolean }) {
        this.modalCtrl.dismiss(language);
    }
}
