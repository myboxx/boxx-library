import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'boxx-hidden-input-item',
    templateUrl: './hidden-input-item.component.html',
    styleUrls: ['./hidden-input-item.component.scss'],
})
export class HiddenInputItemComponent implements OnInit {
    @Input() text: string;
    @Output() closeIconName: string;
    @Output() closeIconSize: 'small' | 'large' | undefined;
    @Output() closeIconSlot: 'start' | 'end';
    @Output() whenInputVisibilityChange = new EventEmitter<boolean>();

    inputShown = false;

    constructor() { }

    ngOnInit() { }

    toggleRealInputVisibility(show: boolean) {
        this.inputShown = show;

        this.whenInputVisibilityChange.emit(show);
    }
}
