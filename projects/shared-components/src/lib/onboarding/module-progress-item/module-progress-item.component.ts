import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-module-progress-item',
    templateUrl: './module-progress-item.component.html',
    styleUrls: ['./module-progress-item.component.scss'],
})
export class ModuleProgressItemComponent implements OnInit {
    @Input() text: string;
    @Input() class: string;
    @Input() progressbarColor: string;
    @Input() value = 0; // progress value, from 0 to 1

    constructor() { }

    ngOnInit() { }

}
