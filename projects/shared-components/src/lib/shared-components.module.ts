import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CardConfigurationFormComponent } from './business/card-configuration-form/card-configuration-form.component';
import { AppointmentItemComponent } from './events/appointment-item/appointment-item.component';
import { ContentDividerComponent } from './general-content/content-divider/content-divider.component';
import { ContentHeaderItemComponent } from './general-content/content-header-item/content-header-item.component';
import { CountryPickerComponent } from './general-content/country-picker/country-selector.component';
import { CustomModalComponent } from './general-content/custom-modal/custom-modal/custom-modal.component';
import { LanguagePickerComponent } from './general-content/language-picker/language-picker.component';
import { LocationPickerComponent } from './general-content/location-picker/location-picker.component';
import { MainHeaderIconComponent } from './general-content/main-header-icon/main-header-icon.component';
import { ConversationItemComponent } from './messages/conversation-item/conversation-item.component';
import { HiddenInputItemComponent } from './onboarding/hidden-input-item/hidden-input-item.component';
import { ModuleProgressItemComponent } from './onboarding/module-progress-item/module-progress-item.component';
import { KpiItemComponent } from './panel/kpi-item/kpi-item.component';
import { WidgetHeaderItemComponent } from './panel/widget-header-item/widget-header-item.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule.forChild()
    ],
    declarations: [
        // ------ GENERAL-CONTENT COMPONENTS:
        MainHeaderIconComponent,
        ContentHeaderItemComponent,
        ContentDividerComponent,
        CustomModalComponent,
        CountryPickerComponent,
        LanguagePickerComponent,
        LocationPickerComponent,

        // ------ BUSINESS COMPONENTS:
        CardConfigurationFormComponent,

        // ------ EVENTS COMPONENTS:
        AppointmentItemComponent,

        // ------ MESSAGES COMPONENTS:
        ConversationItemComponent,

        // ------ ONBOARDING COMPONENTS:
        HiddenInputItemComponent,
        ModuleProgressItemComponent,

        // ------ ONBOARDING COMPONENTS:
        KpiItemComponent,
        WidgetHeaderItemComponent,
    ],
    exports: [
        // ------ GENERAL-CONTENT COMPONENTS:
        MainHeaderIconComponent,
        ContentHeaderItemComponent,
        ContentDividerComponent,
        CustomModalComponent,
        CountryPickerComponent,
        LanguagePickerComponent,
        LocationPickerComponent,

        // ------ BUSINESS COMPONENTS:
        CardConfigurationFormComponent,

        // ------ EVENTS COMPONENTS:
        AppointmentItemComponent,

        // ------ MESSAGES COMPONENTS:
        ConversationItemComponent,

        // ------ ONBOARDING COMPONENTS:
        HiddenInputItemComponent,
        ModuleProgressItemComponent,

        // ------ PANEL COMPONENTS:
        KpiItemComponent,
        WidgetHeaderItemComponent,

        // ------ PANEL COMPONENTS:
    ]
})
export class SharedComponentsModule { }
