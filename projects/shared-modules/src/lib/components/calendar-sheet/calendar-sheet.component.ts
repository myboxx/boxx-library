import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'boxx-calendar-sheet',
    templateUrl: './calendar-sheet.component.html',
    styleUrls: ['./calendar-sheet.component.scss'],
})
export class CalendarSheetComponent implements OnInit {
    @Input() dayName: string;
    @Input() monthDay: string;
    @Input() monthName: string;

    @Input() headerIonColor: string;

    constructor() { }

    ngOnInit() {
        this.dayName = this.dayName.toUpperCase().replace('.', '');
        this.monthName = this.monthName.toUpperCase().replace('.', '');
    }

}
