import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ContactModel, ContactStore } from '@boxx/contacts-core';
import { AlertController, IonSearchbar, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
    selector: 'boxx-contact-picker-modal',
    templateUrl: './contact-picker.modal.html',
    styleUrls: ['./contact-picker.modal.scss']
})
export class ContactPickerModalComponent implements OnInit, OnDestroy {
    @ViewChild('contactPickerSB', { static: false }) searchBar: IonSearchbar;

    @Input() currentAttendees: number[];

    constructor(
        private modalCtrl: ModalController,
        private store: ContactStore,
        private alertCtrl: AlertController,
        private translate: TranslateService
    ) {
        this.translate.get(['CONTACTS', 'GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);
    }

    searchValue = '';
    contactList: Array<{ isChecked: boolean, contact: ContactModel }> = [];
    searchResults: Array<{ isChecked: boolean, contact: ContactModel }>;

    isFiltering = false;
    isIndeterminate: boolean;
    masterCheck: boolean;

    destroyed$ = new Subject<boolean>();
    isLoading$: Observable<boolean>;

    translations: any;

    ngOnInit() {

        this.isLoading$ = this.store.Loading$;

        this.store.Contacts$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(cl => {
                this.searchResults = this.contactList = cl.map(c =>
                    ({ isChecked: this.currentAttendees.findIndex((e) => e === c.id) !== -1, contact: c })
                );
                setTimeout(() => {
                    this.searchBar.setFocus();
                }, 800);
            });

        this.store.HasBeenFetched$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(ok => {
                if (!ok) {
                    this.store.fetchContacts();
                }
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    checkMaster() {
        setTimeout(() => {
            this.searchResults.forEach(obj => {
                obj.isChecked = this.masterCheck;
            });
        });
    }

    checkEvent() {
        const totalItems = this.searchResults.length;
        let checked = 0;
        this.searchResults.map(obj => {
            if (obj.isChecked) { checked++; }
        });
        if (checked > 0 && checked < totalItems) {
            // If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        } else if (checked === totalItems) {
            // If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        } else {
            // If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    }

    async close() {
        if (this.hasChanges()) {
            (await this.alertCtrl.create({
                header: this.translations.GENERAL.ACTION.confirm,
                message: this.translations.CONTACTS.confirmCloseWithSelectedContactsMsg,
                buttons: [{
                    text: this.translations.GENERAL.ACTION.ok,
                    handler: () => {
                        this.modalCtrl.dismiss(/*{someData: ...} */);
                    }
                }, {
                    text: this.translations.GENERAL.ACTION.cancel,
                    role: 'cancel',
                    cssClass: 'primary',
                }, ]
            })).present();
        }
        else {
            this.modalCtrl.dismiss(/*{someData: ...} */);
        }
    }

    select() {
        return this.modalCtrl.dismiss(this._getSelectedContacts());
    }

    private _getSelectedContacts() {
        return this.searchResults.filter(c => c.isChecked).map(c => c.contact);
    }

    hasChanges() {
        return this._getSelectedContacts().length !== this.currentAttendees.length ||
            this._getSelectedContacts()
                .filter(c => !this.currentAttendees.includes(c.id)).length > 0;
    }

    search(searchTerm: string) {

        this.searchResults = this.contactList;

        if (!searchTerm) {
            this.isFiltering = false;
            return;
        }

        this.isFiltering = true;

        this.searchResults = this.searchResults.filter(item => {
            return (item.contact.name + item.contact.lastName).toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                item.contact.phone.indexOf(searchTerm) > -1 ||
                item.contact.email.indexOf(searchTerm) > -1;
        });
    }

    showAll() {
        this.searchValue = ' ';
        setTimeout(() => {
            this.searchValue = '';
        }, 10);
    }
}
