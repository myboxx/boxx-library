import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel, ContactStore, COUNTRY_CALLING_CODES, ICountryCodes } from '@boxx/contacts-core';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSettingsService } from '../../providers/global-params';
import { CountryCodeSelectorPopoverComponent } from './country-codes-selector.popover';
import { CountrySelectorModalComponent } from './country-selector.modal';

@Component({
    selector: 'boxx-contact-upsert-modal',
    templateUrl: './contact-upsert.modal.html',
    styleUrls: ['./contact-upsert.modal.scss'],
})
export class ContactUpsertModalComponent implements OnInit, OnDestroy {
    @Input() contact: ContactModel;
    @Input() mode: 'CREATE' | 'UPDATE';

    constructor(
        private modalCtrl: ModalController,
        private store: ContactStore,
        private translate: TranslateService,
        private popoverController: PopoverController,
        public mdalCtrl: ModalController,
        private loadingCtrl: LoadingController,
        private moduleService: AppSettingsService
    ) {
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => {
                this.types[0].value = t.GENERAL.client;
                this.types[1].value = t.GENERAL.prospect;
                this.types[2].value = t.GENERAL.not_specified;
            });
    }

    destroyed$ = new Subject<boolean>();

    contactForm: FormGroup;

    types = [
        { key: 'CLIENT', value: 'Client' },
        { key: 'PROSPECT', value: 'Prospect' },
        { key: 'NOT_SPECIFIED', value: 'Not qualified' }
    ];

    validationMessages = {
        name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.name,
        last_name: this.moduleService.getAppConstants().VALIDATION_MESSAGES.last_name,
        phone: this.moduleService.getAppConstants().VALIDATION_MESSAGES.phone,
        email: this.moduleService.getAppConstants().VALIDATION_MESSAGES.email,
        address: this.moduleService.getAppConstants().VALIDATION_MESSAGES.address
    };

    ngOnInit() {

        this.contactForm = new FormGroup({
            name: new FormControl(this.contact.name, Validators.required),
            last_name: new FormControl(this.contact.lastName, Validators.required),
            phone: new FormControl(
                this.contact.phone,
                [Validators.minLength(10), Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]*')]
            ),
            email: new FormControl(
                this.contact.email, Validators.pattern(this.moduleService.getAppConstants().EMAILPATTERN)
            ),
            street_address: new FormControl(this.contact.streetAddress),
            type: new FormControl(this.contact.type),
            city: new FormControl(this.contact.city),
            id: new FormControl(this.contact.id),
            country_code: new FormControl(COUNTRY_CALLING_CODES[1]),
            phone_code: new FormControl(this.contact.phoneCode || COUNTRY_CALLING_CODES[1].callingCodes[0]),
        });

        this.store.CountryCodes$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (countries) => {
                if (!countries.length) { return this.store.fetchCountryCodes(); }

                const countryCode = countries.filter(c => c.alpha3Code === this.contact.countryCode)[0] || COUNTRY_CALLING_CODES[1];
                this.contactForm.patchValue({ country_code: countryCode });
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    close() {
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    update() {
        // this._appendCountryCode();

        const form = this.contactForm.value;

        delete form.phone_code;

        form.country_code = form.country_code.alpha3Code;

        this.store.updateContact(form);
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    create() {
        // this._appendCountryCode();
        const form = this.contactForm.value;

        delete form.phone_code;

        form.country_code = form.country_code.alpha3Code;

        this.store.createContact(form);
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    async presentPopover(/*ev: any*/) {
        const loader = await this.loadingCtrl.create();

        loader.present();

        this.store.CountryCodes$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (countries) => {
                if (!countries.length) { return this.store.fetchCountryCodes(); }

                let popover: any = await this.mdalCtrl.create({
                    component: CountrySelectorModalComponent,
                    componentProps: { countries },
                    // event: ev,
                });

                await popover.present();

                loader.dismiss();

                const response = await popover.onDidDismiss();
                const selCountry = response.data;

                if (selCountry) {
                    if (selCountry.callingCodes.length > 1) {
                        popover = await this.popoverController.create({
                            component: CountryCodeSelectorPopoverComponent,
                            componentProps: { codes: selCountry.callingCodes },
                            // event: ev,
                            translucent: true,
                            backdropDismiss: false
                        });
                        await popover.present();
                        const { data } = await popover.onDidDismiss();

                        this.contactForm.patchValue({ country_code: selCountry, phone_code: `+${data}` });
                    }
                    else {
                        this.contactForm.patchValue({ country_code: selCountry, phone_code: `+${selCountry.callingCodes[0]}` });
                    }
                }
            });
    }

    private _appendCountryCode() {
        const country: ICountryCodes = this.contactForm.get('country_code').value;

        switch (country.alpha3Code) {
            case 'MEX':
                this.contactForm.patchValue({
                    phone: `+${country.callingCodes[0]} 1 ${this.contactForm.get('phone').value}`.replace(/ /g, '')
                });
                break;
            case 'ARG':
                this.contactForm.patchValue({
                    phone: `+${country.callingCodes[0]} 9 ${this.contactForm.get('phone').value}`.replace(/ /g, '')
                });
        }

        this.contactForm.get('country_code').disable();
    }
}
