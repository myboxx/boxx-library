import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';


@Component({
    selector: 'boxx-country-code-select-popover',
    template: `
    <ion-content>
        <ion-list-header translate>
            GENERAL.ACTION.selectCountryCode
        </ion-list-header>
        <ion-item *ngFor="let code of codes" (click)="select(code)">
            <ion-label>
                +{{code}}
            </ion-label>
        </ion-item>
    </ion-content>
    `
})
export class CountryCodeSelectorPopoverComponent implements OnInit {
    @Input() codes: Array<number> = [];

    constructor(
        public popoverController: PopoverController
    ) { }

    ngOnInit() {

    }

    select(code: number) {
        this.popoverController.dismiss(code);
    }
}
