import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ICountryCodes } from '@boxx/contacts-core';


@Component({
    selector: 'boxx-country-selector-modal',
    templateUrl: './country-selector.modal.html',
    styleUrls: ['./country-selector.modal.scss']
})
export class CountrySelectorModalComponent implements OnInit {
    @Input() countries: Array<ICountryCodes> = [];

    constructor(
        public popoverController: ModalController
    ) { }

    items = [];

    ngOnInit() {
        this.items = this.countries;
    }

    select(country: ICountryCodes) {
        this.popoverController.dismiss(country);
    }

    search(searchTerm: string) {
        this.items = this.countries.filter(country => {
            return (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1;
        });
    }

    close() {
        this.popoverController.dismiss();
    }
}
