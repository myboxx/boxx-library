import { Component, Inject, Input, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactModel } from '@boxx/contacts-core';
import { EventModel, EventsStore, IEventReminder } from '@boxx/events-core';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { AppSettingsService, LOCAL_NOTIFICATIONS_SERVICE } from '../../providers/global-params';
import { ILocalNotificationsService } from '../../shared-modules.module';
import { ContactPickerModalComponent } from '../contact-picker-modal/contact-picker.modal';
import { PlacePickerModalComponent } from '../place-picker-modal/place-picker.modal';


// To prevent compiling errors
const moment = moment_;

@Component({
    selector: 'boxx-event-upsert',
    templateUrl: './event-upsert.modal.html',
    styleUrls: ['./event-upsert.modal.scss'],
})
export class EventUpsertModalComponent implements OnInit, OnDestroy {

    constructor(
        @Optional() @Inject(LOCAL_NOTIFICATIONS_SERVICE) private localNotifSrvc: ILocalNotificationsService,
        private store: EventsStore,
        public loadingCtrl: LoadingController,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private translate: TranslateService,
        private toastController: ToastController,
        private moduleService: AppSettingsService
    ) {

        this.translate.get(['GENERAL', 'FORM', 'EVENTS'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);
    }
    @Input() period: { from: any, to: any };
    @Input() action: 'CREATE' | 'UPDATE';
    @Input() attendees: Array<ContactModel>;

    event$: Observable<EventModel>;
    loading$: Observable<boolean>;
    destroyed$ = new Subject<boolean>();
    eventId: number;

    loader: any;

    // startTime = moment().toISOString(true);
    // endTime = moment().add(1, 'hours').toISOString(true);

    monthNames = moment.months().toString();

    evAttendees: Array<ContactModel> = [];

    eventForm: FormGroup;

    translations: any;

    validationMessages = {
        title: this.moduleService.getAppConstants().VALIDATION_MESSAGES.title,
        place: this.moduleService.getAppConstants().VALIDATION_MESSAGES.place,
        datetime_from: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_from,
        datetime_to: this.moduleService.getAppConstants().VALIDATION_MESSAGES.datetime_to,
    };

    ngOnInit() {

        this.eventForm = new FormGroup({
            title: new FormControl('', Validators.required),
            place: new FormControl(),
            datetime_from: new FormControl(moment(this.period.from).toISOString(true), Validators.required),
            datetime_to: new FormControl(moment(this.period.to).add(1, 'hours').toISOString(true)),
            attendees: new FormControl(this.attendees || []),
            description: new FormControl(''),
            latitude: new FormControl(''),
            longitude: new FormControl('')
        });

        this.evAttendees = this.eventForm.get('attendees').value;

        if (this.action === 'UPDATE') {
            this.store.selectedEvent$()
                .pipe(takeUntil(this.destroyed$))
                .subscribe(id => {
                    this.eventId = id;

                    this.store.EventById$(id)
                        .pipe(takeUntil(this.destroyed$))
                        .subscribe(m => {

                            this.evAttendees = m.attendees.map(
                                c => new ContactModel({
                                    id: c.contactId, name: c.name, lastName: c.lastName, email: c.email, phone: c.phone
                                })
                            );

                            this.eventForm.patchValue({
                                title: m.title,
                                place: m.place,
                                datetime_from: moment(m.datetimeFrom).toISOString(true),
                                datetime_to: moment(m.datetimeTo).toISOString(true),
                                attendees: this.evAttendees,
                                description: m.description
                            });
                        });
                });
        }
        else {
            // only to reset the state.success property
            this.store.EventById$(null);
        }

        this._handleLoading();
        this._handleSuccess();
        this._handleError();
    }

    private _handleLoading() {
        this.loading$ = this.store.Loading$;

        this.loading$
            .pipe(
                takeUntil(this.destroyed$),
                withLatestFrom(this.store.Error$),
            )
            .subscribe(async ([loading, error]) => {
                if (loading) {
                    this.loader = await this.loadingCtrl.create();
                    this.loader.present();
                }
                else {
                    if (this.loader) {
                        this.loadingCtrl.dismiss().then(() => {
                            this.loader = null;
                        });
                    }
                }
            });
    }

    private _handleError() {
        this.store.Error$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (e) => {
                if (e) {
                    const toast = await this.toastController.create({
                        message: this.translations.EVENTS.ERRORS[e.after.toLowerCase()],
                        duration: 5000,
                        color: 'danger'
                    });
                    toast.present();
                }
            });
    }

    private _handleSuccess() {
        this.store.Success$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (success) => {
                if (success) {
                    const toast = await this.toastController.create({
                        message: this.translations.EVENTS.SUCCESS[success.after.toLowerCase()],
                        duration: 3000,
                        color: 'success'
                    });

                    toast.present().then(() => this.modalCtrl.dismiss({ eventId: this.eventId }));

                    if (success.after === 'CREATE' || success.after === 'UPDATE') {
                        const momentStartTime = moment(this.eventForm.get('datetime_from').value);

                        const reminder: IEventReminder = {
                            id: success.after === 'CREATE' ? (success.data as EventModel).id : this.eventId,
                            title: 'Evento próximo',
                            text: this.eventForm.get('title').value,
                            date: momentStartTime.subtract(10, 'minutes').set({ second: 0, millisecond: 0 }).toDate()
                        };

                        if (this.localNotifSrvc){
                            if (success.after === 'CREATE') {
                                this.localNotifSrvc.setReminder(reminder);
                            }
                            else {
                                this.localNotifSrvc.updateReminder(reminder);
                            }
                        }
                    }
                }
            });
    }

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    upsertEvent() {
        this.eventForm.get('attendees')
            .patchValue(this.eventForm.get('attendees').value.map((c: ContactModel) => ({ contact_id: c.id })));

        this.action === 'CREATE' ?
            this.store.createEvent(this.eventForm.value) : this.store.updateEvent(this.eventId, this.eventForm.value);
    }

    async openContactPicker() {
        const modal = await this.modalCtrl.create({
            component: ContactPickerModalComponent,
            componentProps: {
                currentAttendees: this.evAttendees.map(c => c.id)
            }
        });

        await modal.present();

        const { data } = await modal.onWillDismiss();

        if (data) {
            this.eventForm.patchValue({ attendees: [...data] });
            this.evAttendees = [...data];
        }
    }

    async removeAttendee(contact: ContactModel) {
        this.translate.get('EVENTS.confirmRemoveAttendee', { name: `${contact.name} ${contact.lastName}` })
            .pipe(takeUntil(this.destroyed$))
            .subscribe(async (msg) => {
                (await this.alertCtrl.create({
                    header: this.translations.GENERAL.ACTION.confirm,
                    message: msg,
                    buttons: [{
                        text: this.translations.GENERAL.ACTION.ok,
                        handler: () => {
                            this.evAttendees = this.evAttendees.filter(c => c.id !== contact.id);
                            this.eventForm.patchValue({
                                attendees: this.evAttendees
                            });
                        }
                    }, {
                        text: this.translations.GENERAL.ACTION.cancel,
                        role: 'cancel',
                        cssClass: 'primary',
                    }]
                })).present();
            });
    }

    async showAttendeeDetails(contact: ContactModel) {
        (await this.alertCtrl.create({
            header: `${contact.name} ${contact.lastName}`,
            message: `<b>Email</b>: ${contact.email}<br><b>${this.translations.FORM.LABEL.phone}</b>: ${contact.phone}`,
            buttons: [{
                text: this.translations.GENERAL.ACTION.ok,
                role: 'cancel',
                cssClass: 'primary',
                // handler: () => { }
            }]
        })).present();
    }

    async openSearchPlace() {
        const modal = await this.modalCtrl.create({
            component: PlacePickerModalComponent,
            // componentProps: {}
        });

        await modal.present();

        const { data } = await modal.onWillDismiss();

        if (data) {
            this.eventForm.patchValue({ place: data.description });
        }
    }

    close() {
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    dateChange(value, isStart = true) {
        const currentStartDate = moment(this.eventForm.value.datetime_from);
        const currentEndDate = moment(this.eventForm.value.datetime_to);
        if (isStart) {
            const startValue = moment(value);
            if (startValue > currentEndDate) {
                this.eventForm.patchValue({ datetime_to: currentStartDate.add(1, 'hours').toISOString(true) });
            }
        } else {
            const endValue = moment(value);
            if (endValue < currentStartDate) {
                this.eventForm.patchValue({ datetime_from: currentEndDate.subtract(1, 'hours').toISOString(true) });
            }
        }
    }
}
