import { Component, OnInit } from '@angular/core';
import { AppVersionService } from '../../services/NativeAppVersion.service';

@Component({
  selector: 'boxx-native-version-info',
  templateUrl: './native-version-info.component.html',
  styleUrls: ['./native-version-info.component.scss'],
})
export class NativeVersionInfoComponent implements OnInit {

  constructor(
    public appVersionService: AppVersionService
  ) { }

  ngOnInit() {}

}
