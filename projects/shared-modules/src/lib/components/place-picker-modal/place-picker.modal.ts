import { Component, OnDestroy, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';

declare var google: any;

@Component({
    selector: 'boxx-place-picker-modal',
    templateUrl: './place-picker.modal.html',
    styleUrls: ['./place-picker.modal.scss']
})
export class PlacePickerModalComponent implements OnDestroy {
    @ViewChild('placePickerSB', { static: false }) searchBar: IonSearchbar;

    constructor(private modalCtrl: ModalController) {
        this.mapsService = new google.maps.places.AutocompleteService();
    }

    destroyed$ = new Subject<boolean>();
    isLoading$: Observable<boolean>;

    searchResults: Array<any> = [];

    translations: any;

    mapsService: any;

    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ionViewDidEnter() {
        setTimeout(() => {
            this.searchBar.setFocus();
        }, 200);
    }

    onInput(query: string) {
        this.searchResults = [];

        if (!query) {
            return;
        }

        const config = {
            types: [], // other types available in the API: 'address 'establishment', 'regions', and 'cities'
            input: query
        };

        this.mapsService.getPlacePredictions(config, (predictions, status) => {
            if (predictions && predictions.length > 0) {
                predictions.forEach((prediction) => {
                    this.searchResults.push(prediction);
                });
            } else {
                const locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };

                this.searchResults.push(locationDesc);
            }
        });
    }

    async close() {
        this.modalCtrl.dismiss(/*{someData: ...} */);
    }

    chooseItem(item: any) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    }

    select() { }
}
