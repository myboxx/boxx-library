import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FIREBASEX_SERVICE, IFirebasexService } from '../../services/ISharedModules.service';


@Component({
    selector: 'boxx-remote-config',
    templateUrl: './remote-config.component.html',
    styleUrls: ['./remote-config.component.scss'],
})
export class RemoteConfigComponent implements OnInit {
    @Output() whenFinish = new EventEmitter<boolean>();

    constructor(
        @Inject(FIREBASEX_SERVICE) private firebasex: IFirebasexService,
        private translate: TranslateService,
        private appVersion: AppVersion,
        private market: Market,
        private alertCtrl: AlertController,
        private platform: Platform
    ) {
        this.translate.get(['GENERAL'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t: any) => this.translations = t);
    }

    destroyed$ = new Subject<boolean>();

    translations: any;

    configMessage = '...';

    isIos = false;

    ngOnInit() {
        this.platform.ready().then(() => {

            this.configMessage = this.translations.GENERAL.loadingRemoteConfig;

            this.isIos = this.platform.is('ios');

            this.checkForUpdates();

            this.configMessage = '';
        });
    }

    async checkForUpdates() {

        const currentVersionNumber = await this.appVersion.getVersionNumber();
        const packageName = await this.appVersion.getPackageName();

        await this.firebasex.fetch(600);

        await this.firebasex.activateFetched();

        const mostRecentVersion = await this.firebasex.getValue('most_recent_version' + (this.isIos ? '_ios' : '')) || '0.0.1';

        console.log('--- most_recent_version', mostRecentVersion);

        if (mostRecentVersion > currentVersionNumber) {
            const alert = await this.alertCtrl.create({
                header: this.translations.GENERAL.appUpdateHeader,
                message: `${this.translations.GENERAL.appUpdateMsg} ${mostRecentVersion}`,
                buttons: [{
                    text: this.translations.GENERAL.ACTION.update,
                    handler: () => {
                        this.market.open(packageName);

                        return false;
                    }
                }],
                backdropDismiss: false
            });

            alert.present();
        }
        else {
            this.whenFinish.emit(true);
        }
    }
}
