import { InjectionToken } from '@angular/core';
import { IEventReminder } from '@boxx/events-core';
import { SharedModulesOptionsInterface } from '../shared-modules.module';

export class AppSettingsService {
    constructor(settings: SharedModulesOptionsInterface) {
        this.setAppConstants(settings.appConstants);
    }

    private appConstants: any;

    private setAppConstants(value: any) { this.appConstants = value; }
    getAppConstants() { return this.appConstants; }
}

export const LOCAL_NOTIFICATIONS_SERVICE = new InjectionToken('localNotificationsService');
