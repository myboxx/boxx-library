import { Injectable } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AppVersionService {

    constructor(
        private appVersion: AppVersion,
        private platform: Platform
    ) {
        this.platform.ready().then(async () => {
            if (this.platform.is('cordova') || this.platform.is('capacitor')){
                this.appNameVal = await this.appVersion.getAppName();
                this.packageNameVal = await this.appVersion.getPackageName();
                this.versionCodeVal = await this.appVersion.getVersionCode();
                this.versionNumberVal = await this.appVersion.getVersionNumber();
            }

            this.whenReadySubject.next({
                appName: this.appNameVal,
                packageName: this.packageNameVal,
                versionCode: this.versionCodeVal,
                versionNumber: this.versionNumberVal
            });
        });
    }

    private appNameVal: string;
    private packageNameVal: string;
    private versionCodeVal: string | number;
    private versionNumberVal: string;

    private whenReadySubject = new BehaviorSubject<{
        appName: string,
        packageName: string,
        versionCode: string | number,
        versionNumber: string
    }>({
        appName: undefined,
        packageName: undefined,
        versionCode: undefined,
        versionNumber: 'PWA'
    });

    public whenReady$ = this.whenReadySubject.asObservable();

    appName() {
        return this.appNameVal;
    }

    packageName() {
        return this.packageNameVal;
    }

    versionCode() {
        return this.versionCodeVal;
    }

    versionNumber() {
        return this.versionNumberVal;
    }
}
