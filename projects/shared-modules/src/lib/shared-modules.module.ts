import { CommonModule } from '@angular/common';
import { InjectionToken, ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IEventReminder } from '@boxx/events-core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarSheetComponent } from './components/calendar-sheet/calendar-sheet.component';
import { ContactPickerModalComponent } from './components/contact-picker-modal/contact-picker.modal';
import { ContactUpsertModalComponent } from './components/contact-upsert-modal/contact-upsert.modal';
import { CountryCodeSelectorPopoverComponent } from './components/contact-upsert-modal/country-codes-selector.popover';
import { CountrySelectorModalComponent } from './components/contact-upsert-modal/country-selector.modal';
import { EventUpsertModalComponent } from './components/event-upsert-modal/event-upsert.modal';
import { NativeVersionInfoComponent } from './components/native-version-info/native-version-info.component';
import { PlacePickerModalComponent } from './components/place-picker-modal/place-picker.modal';
import { RemoteConfigComponent } from './components/remote-config/remote-config.component';
import { AppSettingsService } from './providers/global-params';
import { AppVersionService } from './services/NativeAppVersion.service';

export const AppSettingsObject = new InjectionToken('AppSettingsObject');

export function createAppSettingsService(settings: SharedModulesOptionsInterface) {
    return new AppSettingsService(settings);
}

export interface SharedModulesOptionsInterface {
    appConstants: any;
    providers: Provider[];
}

export interface ILocalNotificationsService {
    setReminder(reminder: IEventReminder);

    removeReminder(id: number);

    updateReminder(reminder: IEventReminder);

    checkIdScheduled(id: number);

    init();

    stop();
}

@NgModule({
  declarations: [
    CalendarSheetComponent,
    ContactPickerModalComponent,
    ContactUpsertModalComponent,
    CountryCodeSelectorPopoverComponent,
    CountrySelectorModalComponent,
    EventUpsertModalComponent,
    PlacePickerModalComponent,
    NativeVersionInfoComponent,
    RemoteConfigComponent,
  ],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      IonicModule,
      TranslateModule.forChild()
  ],
  exports: [
    CalendarSheetComponent,
    ContactPickerModalComponent,
    ContactUpsertModalComponent,
    CountryCodeSelectorPopoverComponent,
    CountrySelectorModalComponent,
    EventUpsertModalComponent,
    PlacePickerModalComponent,
    NativeVersionInfoComponent,
    RemoteConfigComponent,
  ]
})
export class SharedModulesModule {
    static forRoot(config: SharedModulesOptionsInterface): ModuleWithProviders<SharedModulesModule> {

        return {
            ngModule: SharedModulesModule,
            providers: [
                { provide: AppSettingsObject, useValue: config },
                {
                    provide: AppSettingsService,
                    useFactory: (createAppSettingsService),
                    deps: [AppSettingsObject]
                },
                ...config.providers,
                AppVersionService,
                AppVersion,
                Market
            ]
        };
    }
 }
