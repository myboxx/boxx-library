/*
 * Public API Surface of shared-modules
 */

export * from './lib/shared-modules.module';
export * from './lib/components/contact-picker-modal/contact-picker.modal';
export * from './lib/components/contact-upsert-modal/contact-upsert.modal';
export * from './lib/components/contact-upsert-modal/country-codes-selector.popover';
export * from './lib/components/contact-upsert-modal/country-selector.modal';
export * from './lib/components/event-upsert-modal/event-upsert.modal';
export * from './lib/components/place-picker-modal/place-picker.modal';
export * from './lib/components/remote-config/remote-config.component';
export * from './lib/services/ISharedModules.service';
export * from './lib/services/NativeAppVersion.service';
export { LOCAL_NOTIFICATIONS_SERVICE } from './lib/providers/global-params';
export { ILocalNotificationsService } from './lib/shared-modules.module';
