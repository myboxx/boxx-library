import { Injectable } from '@angular/core';
import { IHttpBasicResponse } from '@boxx/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ICategoryApiModel, IOrderApiModel, IProductApiModel } from '../repositories/IStore.api';
import { IStoreRepository } from '../repositories/IStore.repository';

@Injectable()
export class StoreRepositoryMock implements IStoreRepository {
    // --------------------- CATEGORIES: -------------------------
    loadCategories(): Observable<IHttpBasicResponse<ICategoryApiModel[]>> {
        const data: ICategoryApiModel[] = [
            {
                active: true,
                description: '<p>Sillas</p>',
                id: 13812,
                name: 'Sillas',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13812/',
                shop_id: 1821,
                slug: 'sillas'
            },
            {
                active: true,
                description: '<p>Consolas</p>',
                id: 13814,
                name: 'Consolas',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13814/',
                shop_id: 1821,
                slug: 'consolas'
            },
            {
                active: true,
                description: '<p>Escritorios</p>',
                id: 13815,
                name: 'Escritorios',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13815/',
                shop_id: 1821,
                slug: 'escritorios'
            },
            {
                active: true,
                description: 'test de categoría desc',
                id: 13919,
                name: 'categoría prueba',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13919/',
                shop_id: 1821,
                slug: 'categoria-prueba'
            },
            {
                active: true,
                description: 'categoria desde endpoint',
                id: 13926,
                name: 'Categoria pm 01',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13926/',
                shop_id: 1821,
                slug: 'categoria-pm-01'
            },
            {
                active: true,
                description: 'categoria desde endpoint modificada',
                id: 13928,
                name: 'Categoria pm 01 mod',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13928/',
                shop_id: 1821,
                slug: 'categoria-pm-01-mod'
            },
            {
                active: true,
                description: 'categoria desde endpoint 2',
                id: 13934,
                name: 'Categoria nueva desde pm',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13934/',
                shop_id: 1821,
                slug: 'categoria-nueva-desde-pm'
            },
            {
                active: true,
                description: 'categoria desde endpoint 3',
                id: 13935,
                name: 'Categoria nueva desde pm 3',
                parent_id: null,
                resource_uri: '/api/v2/shopcategories/13935/',
                shop_id: 1821,
                slug: 'categoria-nueva-desde-pm-3'
            }
        ];
        const responseOk: IHttpBasicResponse<ICategoryApiModel[]> = {
            status: 'success',
            data
        };

        return of(responseOk).pipe(delay(800));
    }
    createCategory(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    updateCategory(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    deleteCategory(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }

    // --------------------- ORDERS: -------------------------
    loadOrders(): Observable<IHttpBasicResponse<IOrderApiModel[]>> {
        const data: IOrderApiModel[] = [
            {
                buyer_email: 'carlosa.camargo16@gmail.com',
                buyer_name: 'Carlos Alberto Camargo Gonzalez',
                id: 883,
                price: 0,
                resource_uri: '/api/v2/shoporders/883/',
                session_id: '5fw1pj80s0eh0txo9xfkdih4yrdxst4y',
                ship_to_address: '505 AV SANTA FE ',
                ship_to_city: 'MEXICO',
                ship_to_country: 'MX',
                ship_to_postal_code: '05349',
                ship_to_state: null,
                shipping_cost: null,
                shipping_id: '9350646',
                shipping_name: 'envioclick',
                site_id: 1916476,
                status: 'new',
                timestamp: '2021-03-05T23:36:05',
                transaction_id: null,
                whitelabel_id: 170
            },
            {
                buyer_email: 'cesar.arias1@elavon.com',
                buyer_name: 'Cesar Arias  Garcia',
                id: 884,
                price: 0,
                resource_uri: '/api/v2/shoporders/884/',
                session_id: 'hxx49phai60verb9ifsgvahbfx9tbmxu',
                ship_to_address: '55 Circuito Merlot',
                ship_to_city: 'Villedos',
                ship_to_country: 'MX',
                ship_to_postal_code: '76235',
                ship_to_state: null,
                shipping_cost: null,
                shipping_id: '9385700',
                shipping_name: 'envioclick',
                site_id: 1916476,
                status: 'new',
                timestamp: '2021-03-08T16:02:29',
                transaction_id: null,
                whitelabel_id: 170
            },
            {
                buyer_email: 'felix.mendoza1@elavon.com',
                buyer_name: 'Felix Edmundo Mendoza Rodriguez',
                id: 885,
                price: 0,
                resource_uri: '/api/v2/shoporders/885/',
                session_id: '2aw74ucr8cbm67a83uubnjni8d7n90ap',
                ship_to_address: '81 Av Toluca',
                ship_to_city: 'Lomas de Atizapán ',
                ship_to_country: 'MX',
                ship_to_postal_code: '52977',
                ship_to_state: null,
                shipping_cost: null,
                shipping_id: '9385700',
                shipping_name: 'envioclick',
                site_id: 1916476,
                status: 'new',
                timestamp: '2021-03-08T16:13:47',
                transaction_id: null,
                whitelabel_id: 170
            },
            {
                buyer_email: 'sandra.lee@elavon.com',
                buyer_name: 'Sandy Lee',
                id: 886,
                price: 0,
                resource_uri: '/api/v2/shoporders/886/',
                session_id: 'alyeugrvhmdjakvwdgm9jpzz4ckjrmkd',
                ship_to_address: '540 Eje 10',
                ship_to_city: 'Pueblo los Reyes ',
                ship_to_country: 'MX',
                ship_to_postal_code: '04330',
                ship_to_state: null,
                shipping_cost: null,
                shipping_id: '9386750',
                shipping_name: 'envioclick',
                site_id: 1916476,
                status: 'new',
                timestamp: '2021-03-08T16:14:38',
                transaction_id: null,
                whitelabel_id: 170
            },
            {
                buyer_email: 'felix.mendoza1@elavon.com',
                buyer_name: 'Félix Edmundo Mendoza Rodríguez',
                id: 887,
                price: 0,
                resource_uri: '/api/v2/shoporders/887/',
                session_id: '2aw74ucr8cbm67a83uubnjni8d7n90ap',
                ship_to_address: '81 Av Toluca',
                ship_to_city: 'Lomas de Atizapán',
                ship_to_country: 'MX',
                ship_to_postal_code: '52977',
                ship_to_state: null,
                shipping_cost: null,
                shipping_id: '9385700',
                shipping_name: 'envioclick',
                site_id: 1916476,
                status: 'new',
                timestamp: '2021-03-08T16:17:46',
                transaction_id: null,
                whitelabel_id: 170
            },
        ];
        const responseOk: IHttpBasicResponse<IOrderApiModel[]> = {
            status: 'success',
            data
        };

        return of(responseOk).pipe(delay(800));
    }
    acceptOrder(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    notifyOrderShipment(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    notifyOrderDelivery(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    deleteOrder(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }

    // --------------------- PRODUCTS: -------------------------
    loadProducts(): Observable<IHttpBasicResponse<IProductApiModel[]>> {
        const data: IProductApiModel[] = [
            {
                active: true,
                added: '2021-03-10T19:06:48',
                asin: null,
                auto_stock: true,
                brand: null,
                categories: [
                    {
                        active: true,
                        description: '<p>Sillas</p>',
                        id: 13812,
                        name: 'Sillas',
                        parent_id: null,
                        resource_uri: '/api/v2/shopcategories/13812/',
                        shop_id: 1821,
                        slug: 'sillas'
                    }
                ],
                description: '<p>Medidas: 45cm Largo, 52.5cm Profundidad, 77cm Altura</p>',
                excerpt: null,
                height: '77.00',
                id: 297013,
                image: {
                    absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.png',
                    added: '2021-03-09T21:26:49',
                    alt: null,
                    checksum: '098f4ade178a1bfb5c3fb0ad3d8a93b8',
                    filesize: 228142,
                    height: 904,
                    id: 3067009,
                    image: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.png',
                    is_global: false,
                    last_increment: 0,
                    mimetype: 'image/png',
                    modified: '2021-03-09T21:26:49',
                    resource_uri: '/api/v2/images/3067009/',
                    site_id: 1916476,
                    sizes: {
                        big: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.big.png',
                        large: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.large.png',
                        medium: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.medium.png',
                        original: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.png',
                        small: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.small.png',
                        thumbnail: 'img/upload/captura-de-pantalla-2021-03-09-a-las-152627.thumbnail.png'
                    },
                    type: 'image',
                    user_id: 646631,
                    versions: [
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.big.png',
                            filesize: 70619,
                            height: 460,
                            id: 11685649,
                            image: 'captura-de-pantalla-2021-03-09-a-las-152627.big.png',
                            image_id: 3067009,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11685649/',
                            type: 'big',
                            width: 305
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.large.png',
                            filesize: 114021,
                            height: 600,
                            id: 11685648,
                            image: 'captura-de-pantalla-2021-03-09-a-las-152627.large.png',
                            image_id: 3067009,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11685648/',
                            type: 'large',
                            width: 398
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.medium.png',
                            filesize: 23456,
                            height: 240,
                            id: 11685650,
                            image: 'captura-de-pantalla-2021-03-09-a-las-152627.medium.png',
                            image_id: 3067009,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11685650/',
                            type: 'medium',
                            width: 159
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.small.png',
                            filesize: 12367,
                            height: 160,
                            id: 11685651,
                            image: 'captura-de-pantalla-2021-03-09-a-las-152627.small.png',
                            image_id: 3067009,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11685651/',
                            type: 'small',
                            width: 106
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.thumbnail.png',
                            filesize: 10734,
                            height: 100,
                            id: 11685652,
                            image: 'captura-de-pantalla-2021-03-09-a-las-152627.thumbnail.png',
                            image_id: 3067009,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11685652/',
                            type: 'thumbnail',
                            width: 100
                        }
                    ],
                    whitelabel_id: 170,
                    width: 600
                },
                image_id: 3067009,
                length: '45.00',
                msrp_price: null,
                outgoing_url: null,
                price: '2536.00',
                primary_image_url: '/img/upload/captura-de-pantalla-2021-03-09-a-las-152627.png',
                product_images: [],
                published: true,
                quantity: 10,
                resource_uri: '/api/v2/products/297013/',
                sale_price: null,
                shop_id: 1821,
                sku: null,
                slug: 'silla-xtilu-asiento-tejido',
                status: null,
                title: 'Silla Xtilu asiento tejido',
                upc: null,
                variations: [
                    {
                        id: 217,
                        name: 'Tono de Acabado',
                        options: [
                            {
                                id: 484,
                                name: 'Natural Poliuretano Mate',
                                order: 0,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 485,
                                name: 'Miel',
                                order: 1,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 486,
                                name: 'Olivo',
                                order: 2,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 487,
                                name: 'Nogal Clásico',
                                order: 3,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 488,
                                name: 'Nogal Americano',
                                order: 4,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 489,
                                name: 'Azul Mancha',
                                order: 5,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 490,
                                name: 'Verde Vintage',
                                order: 6,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 491,
                                name: 'Rojo Mancha',
                                order: 7,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 492,
                                name: 'Gris Tornasol',
                                order: 8,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 493,
                                name: 'Negro Mate',
                                order: 9,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 494,
                                name: 'Verde Militar',
                                order: 10,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 495,
                                name: 'Gris Concreto',
                                order: 11,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 496,
                                name: 'Chocolate Mancha',
                                order: 12,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 497,
                                name: 'Concordia',
                                order: 13,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 498,
                                name: 'Blanco Avejentado',
                                order: 14,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 499,
                                name: 'Blanco Mancha',
                                order: 15,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 500,
                                name: 'Blanco Laca Cerrado',
                                order: 16,
                                price: '0.00',
                                variation_id: 217
                            }
                        ],
                        required: true,
                        resource_uri: '/api/v2/products/variations/217/',
                        shop_id: 1821
                    }
                ],
                vendor: null,
                weight: '7.0',
                weight_unit: 'lb',
                wholesale_price: null,
                width: '52.50'
            },
            {
                active: true,
                added: '2021-03-10T22:43:30',
                asin: null,
                auto_stock: false,
                brand: null,
                categories: [
                    {
                        active: true,
                        description: '<p>Consolas</p>',
                        id: 13814,
                        name: 'Consolas',
                        parent_id: null,
                        resource_uri: '/api/v2/shopcategories/13814/',
                        shop_id: 1821,
                        slug: 'consolas'
                    }
                ],
                description: '<p>Medidas: 160m Largo, 42cm Ancho, 60cm Altura</p>',
                excerpt: null,
                height: '60.00',
                id: 297017,
                image: {
                    absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.png',
                    added: '2021-03-10T22:43:14',
                    alt: null,
                    checksum: 'a1cbfe8b760b1ae9f502d2972b8c9489',
                    filesize: 270520,
                    height: 838,
                    id: 3067191,
                    image: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.png',
                    is_global: false,
                    last_increment: 0,
                    mimetype: 'image/png',
                    modified: '2021-03-10T22:43:14',
                    resource_uri: '/api/v2/images/3067191/',
                    site_id: 1916476,
                    sizes: {
                        big: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.big.png',
                        large: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.large.png',
                        medium: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.medium.png',
                        original: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.png',
                        small: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.small.png',
                        thumbnail: 'img/upload/captura-de-pantalla-2021-03-10-a-las-163529.thumbnail.png'
                    },
                    type: 'image',
                    user_id: 646631,
                    versions: [
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.big.png',
                            filesize: 92718,
                            height: 460,
                            id: 11686515,
                            image: 'captura-de-pantalla-2021-03-10-a-las-163529.big.png',
                            image_id: 3067191,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686515/',
                            type: 'big',
                            width: 446
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.large.png',
                            filesize: 152644,
                            height: 600,
                            id: 11686514,
                            image: 'captura-de-pantalla-2021-03-10-a-las-163529.large.png',
                            image_id: 3067191,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686514/',
                            type: 'large',
                            width: 582
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.medium.png',
                            filesize: 28852,
                            height: 240,
                            id: 11686516,
                            image: 'captura-de-pantalla-2021-03-10-a-las-163529.medium.png',
                            image_id: 3067191,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686516/',
                            type: 'medium',
                            width: 233
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.small.png',
                            filesize: 14461,
                            height: 160,
                            id: 11686517,
                            image: 'captura-de-pantalla-2021-03-10-a-las-163529.small.png',
                            image_id: 3067191,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686517/',
                            type: 'small',
                            width: 156
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.thumbnail.png',
                            filesize: 7080,
                            height: 100,
                            id: 11686518,
                            image: 'captura-de-pantalla-2021-03-10-a-las-163529.thumbnail.png',
                            image_id: 3067191,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686518/',
                            type: 'thumbnail',
                            width: 100
                        }
                    ],
                    whitelabel_id: 170,
                    width: 813
                },
                image_id: 3067191,
                length: '160.00',
                msrp_price: null,
                outgoing_url: null,
                price: '6995.00',
                primary_image_url: '/img/upload/captura-de-pantalla-2021-03-10-a-las-163529.png',
                product_images: [],
                published: true,
                quantity: 20,
                resource_uri: '/api/v2/products/297017/',
                sale_price: null,
                shop_id: 1821,
                sku: null,
                slug: 'consola-2pc',
                status: null,
                title: 'Consola 2PC',
                upc: null,
                variations: [
                    {
                        id: 217,
                        name: 'Tono de Acabado',
                        options: [
                            {
                                id: 484,
                                name: 'Natural Poliuretano Mate',
                                order: 0,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 485,
                                name: 'Miel',
                                order: 1,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 486,
                                name: 'Olivo',
                                order: 2,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 487,
                                name: 'Nogal Clásico',
                                order: 3,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 488,
                                name: 'Nogal Americano',
                                order: 4,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 489,
                                name: 'Azul Mancha',
                                order: 5,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 490,
                                name: 'Verde Vintage',
                                order: 6,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 491,
                                name: 'Rojo Mancha',
                                order: 7,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 492,
                                name: 'Gris Tornasol',
                                order: 8,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 493,
                                name: 'Negro Mate',
                                order: 9,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 494,
                                name: 'Verde Militar',
                                order: 10,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 495,
                                name: 'Gris Concreto',
                                order: 11,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 496,
                                name: 'Chocolate Mancha',
                                order: 12,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 497,
                                name: 'Concordia',
                                order: 13,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 498,
                                name: 'Blanco Avejentado',
                                order: 14,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 499,
                                name: 'Blanco Mancha',
                                order: 15,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 500,
                                name: 'Blanco Laca Cerrado',
                                order: 16,
                                price: '0.00',
                                variation_id: 217
                            }
                        ],
                        required: true,
                        resource_uri: '/api/v2/products/variations/217/',
                        shop_id: 1821
                    }
                ],
                vendor: null,
                weight: '30.0',
                weight_unit: 'lb',
                wholesale_price: null,
                width: '42.00'
            },
            {
                active: true,
                added: '2021-03-10T23:12:26',
                asin: null,
                auto_stock: false,
                brand: null,
                categories: [],
                description: '<p>Medidas: 120cm Largo, 50cm Ancho, 76cm Altura</p>',
                excerpt: null,
                height: '76.00',
                id: 297018,
                image: {
                    absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.png',
                    added: '2021-03-10T23:11:37',
                    alt: null,
                    checksum: '99168d8f1c3b3b37a0722281e4510937',
                    filesize: 409321,
                    height: 796,
                    id: 3067196,
                    image: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.png',
                    is_global: false,
                    last_increment: 0,
                    mimetype: 'image/png',
                    modified: '2021-03-10T23:11:37',
                    resource_uri: '/api/v2/images/3067196/',
                    site_id: 1916476,
                    sizes: {
                        big: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.big.png',
                        large: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.large.png',
                        medium: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.medium.png',
                        original: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.png',
                        small: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.small.png',
                        thumbnail: 'img/upload/captura-de-pantalla-2021-03-10-a-las-171052.thumbnail.png'
                    },
                    type: 'image',
                    user_id: 646631,
                    versions: [
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.big.png',
                            filesize: 82329,
                            height: 324,
                            id: 11686538,
                            image: 'captura-de-pantalla-2021-03-10-a-las-171052.big.png',
                            image_id: 3067196,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686538/',
                            type: 'big',
                            width: 460
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.large.png',
                            filesize: 136401,
                            height: 422,
                            id: 11686537,
                            image: 'captura-de-pantalla-2021-03-10-a-las-171052.large.png',
                            image_id: 3067196,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686537/',
                            type: 'large',
                            width: 600
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.medium.png',
                            filesize: 25164,
                            height: 169,
                            id: 11686539,
                            image: 'captura-de-pantalla-2021-03-10-a-las-171052.medium.png',
                            image_id: 3067196,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686539/',
                            type: 'medium',
                            width: 240
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.small.png',
                            filesize: 12460,
                            height: 112,
                            id: 11686540,
                            image: 'captura-de-pantalla-2021-03-10-a-las-171052.small.png',
                            image_id: 3067196,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686540/',
                            type: 'small',
                            width: 160
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.thumbnail.png',
                            filesize: 118,
                            height: 1,
                            id: 11686541,
                            image: 'captura-de-pantalla-2021-03-10-a-las-171052.thumbnail.png',
                            image_id: 3067196,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686541/',
                            type: 'thumbnail',
                            width: 100
                        }
                    ],
                    whitelabel_id: 170,
                    width: 1130
                },
                image_id: 3067196,
                length: '120.00',
                msrp_price: null,
                outgoing_url: null,
                price: '5847.00',
                primary_image_url: '/img/upload/captura-de-pantalla-2021-03-10-a-las-171052.png',
                product_images: [],
                published: true,
                quantity: 20,
                resource_uri: '/api/v2/products/297018/',
                sale_price: null,
                shop_id: 1821,
                sku: null,
                slug: 'escritorio-con-cajonera',
                status: null,
                title: 'Escritorio con Cajonera',
                upc: null,
                variations: [
                    {
                        id: 217,
                        name: 'Tono de Acabado',
                        options: [
                            {
                                id: 484,
                                name: 'Natural Poliuretano Mate',
                                order: 0,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 485,
                                name: 'Miel',
                                order: 1,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 486,
                                name: 'Olivo',
                                order: 2,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 487,
                                name: 'Nogal Clásico',
                                order: 3,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 488,
                                name: 'Nogal Americano',
                                order: 4,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 489,
                                name: 'Azul Mancha',
                                order: 5,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 490,
                                name: 'Verde Vintage',
                                order: 6,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 491,
                                name: 'Rojo Mancha',
                                order: 7,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 492,
                                name: 'Gris Tornasol',
                                order: 8,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 493,
                                name: 'Negro Mate',
                                order: 9,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 494,
                                name: 'Verde Militar',
                                order: 10,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 495,
                                name: 'Gris Concreto',
                                order: 11,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 496,
                                name: 'Chocolate Mancha',
                                order: 12,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 497,
                                name: 'Concordia',
                                order: 13,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 498,
                                name: 'Blanco Avejentado',
                                order: 14,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 499,
                                name: 'Blanco Mancha',
                                order: 15,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 500,
                                name: 'Blanco Laca Cerrado',
                                order: 16,
                                price: '0.00',
                                variation_id: 217
                            }
                        ],
                        required: true,
                        resource_uri: '/api/v2/products/variations/217/',
                        shop_id: 1821
                    }
                ],
                vendor: null,
                weight: '30.0',
                weight_unit: 'lb',
                wholesale_price: null,
                width: '50.00'
            },
            {
                active: true,
                added: '2021-03-10T23:37:53',
                asin: null,
                auto_stock: false,
                brand: null,
                categories: [
                    {
                        active: true,
                        description: '<p>Escritorios</p>',
                        id: 13815,
                        name: 'Escritorios',
                        parent_id: null,
                        resource_uri: '/api/v2/shopcategories/13815/',
                        shop_id: 1821,
                        slug: 'escritorios'
                    }
                ],
                description: '<p>Medidas: 90cm Largo, 40cm Ancho, 79cm Altura</p>',
                excerpt: null,
                height: '79.00',
                id: 297019,
                image: {
                    absolute_path: '/img/upload/tocador-3-cajones.png',
                    added: '2021-03-10T23:37:40',
                    alt: null,
                    checksum: 'd9e3d4029e990bfd6fb928ee45e0d935',
                    filesize: 197096,
                    height: 888,
                    id: 3067199,
                    image: 'img/upload/tocador-3-cajones.png',
                    is_global: false,
                    last_increment: 0,
                    mimetype: 'image/png',
                    modified: '2021-03-10T23:37:40',
                    resource_uri: '/api/v2/images/3067199/',
                    site_id: 1916476,
                    sizes: {
                        big: 'img/upload/tocador-3-cajones.big.png',
                        large: 'img/upload/tocador-3-cajones.large.png',
                        medium: 'img/upload/tocador-3-cajones.medium.png',
                        original: 'img/upload/tocador-3-cajones.png',
                        small: 'img/upload/tocador-3-cajones.small.png',
                        thumbnail: 'img/upload/tocador-3-cajones.thumbnail.png'
                    },
                    type: 'image',
                    user_id: 646631,
                    versions: [
                        {
                            absolute_path: '/img/upload/tocador-3-cajones.big.png',
                            filesize: 67591,
                            height: 460,
                            id: 11686554,
                            image: 'tocador-3-cajones.big.png',
                            image_id: 3067199,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686554/',
                            type: 'big',
                            width: 397
                        },
                        {
                            absolute_path: '/img/upload/tocador-3-cajones.large.png',
                            filesize: 107088,
                            height: 600,
                            id: 11686553,
                            image: 'tocador-3-cajones.large.png',
                            image_id: 3067199,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686553/',
                            type: 'large',
                            width: 518
                        },
                        {
                            absolute_path: '/img/upload/tocador-3-cajones.medium.png',
                            filesize: 23388,
                            height: 240,
                            id: 11686555,
                            image: 'tocador-3-cajones.medium.png',
                            image_id: 3067199,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686555/',
                            type: 'medium',
                            width: 207
                        },
                        {
                            absolute_path: '/img/upload/tocador-3-cajones.small.png',
                            filesize: 12517,
                            height: 160,
                            id: 11686556,
                            image: 'tocador-3-cajones.small.png',
                            image_id: 3067199,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686556/',
                            type: 'small',
                            width: 138
                        },
                        {
                            absolute_path: '/img/upload/tocador-3-cajones.thumbnail.png',
                            filesize: 7116,
                            height: 100,
                            id: 11686557,
                            image: 'tocador-3-cajones.thumbnail.png',
                            image_id: 3067199,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686557/',
                            type: 'thumbnail',
                            width: 100
                        }
                    ],
                    whitelabel_id: 170,
                    width: 767
                },
                image_id: 3067199,
                length: '90.00',
                msrp_price: null,
                outgoing_url: null,
                price: '4858.00',
                primary_image_url: '/img/upload/tocador-3-cajones.png',
                product_images: [],
                published: true,
                quantity: 29,
                resource_uri: '/api/v2/products/297019/',
                sale_price: null,
                shop_id: 1821,
                sku: null,
                slug: 'tocador-3-cajones',
                status: null,
                title: 'Tocador 3 Cajones',
                upc: null,
                variations: [
                    {
                        id: 217,
                        name: 'Tono de Acabado',
                        options: [
                            {
                                id: 484,
                                name: 'Natural Poliuretano Mate',
                                order: 0,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 485,
                                name: 'Miel',
                                order: 1,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 486,
                                name: 'Olivo',
                                order: 2,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 487,
                                name: 'Nogal Clásico',
                                order: 3,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 488,
                                name: 'Nogal Americano',
                                order: 4,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 489,
                                name: 'Azul Mancha',
                                order: 5,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 490,
                                name: 'Verde Vintage',
                                order: 6,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 491,
                                name: 'Rojo Mancha',
                                order: 7,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 492,
                                name: 'Gris Tornasol',
                                order: 8,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 493,
                                name: 'Negro Mate',
                                order: 9,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 494,
                                name: 'Verde Militar',
                                order: 10,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 495,
                                name: 'Gris Concreto',
                                order: 11,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 496,
                                name: 'Chocolate Mancha',
                                order: 12,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 497,
                                name: 'Concordia',
                                order: 13,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 498,
                                name: 'Blanco Avejentado',
                                order: 14,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 499,
                                name: 'Blanco Mancha',
                                order: 15,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 500,
                                name: 'Blanco Laca Cerrado',
                                order: 16,
                                price: '0.00',
                                variation_id: 217
                            }
                        ],
                        required: true,
                        resource_uri: '/api/v2/products/variations/217/',
                        shop_id: 1821
                    }
                ],
                vendor: null,
                weight: '16.5',
                weight_unit: 'lb',
                wholesale_price: null,
                width: '40.00'
            },
            {
                active: true,
                added: '2021-03-10T23:53:47',
                asin: null,
                auto_stock: false,
                brand: null,
                categories: [],
                description: '<p>Medidas: 68cm Largo, 32cm Ancho, 180cm Altura</p>',
                excerpt: null,
                height: '180.00',
                id: 297020,
                image: {
                    absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.png',
                    added: '2021-03-10T23:52:06',
                    alt: null,
                    checksum: 'c60419ee276e5f2877eeaa70520a85b2',
                    filesize: 120828,
                    height: 907,
                    id: 3067200,
                    image: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.png',
                    is_global: false,
                    last_increment: 0,
                    mimetype: 'image/png',
                    modified: '2021-03-10T23:52:06',
                    resource_uri: '/api/v2/images/3067200/',
                    site_id: 1916476,
                    sizes: {
                        big: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.big.png',
                        large: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.large.png',
                        medium: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.medium.png',
                        original: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.png',
                        small: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.small.png',
                        thumbnail: 'img/upload/captura-de-pantalla-2021-03-10-a-las-175139.thumbnail.png'
                    },
                    type: 'image',
                    user_id: 646631,
                    versions: [
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.big.png',
                            filesize: 38507,
                            height: 460,
                            id: 11686559,
                            image: 'captura-de-pantalla-2021-03-10-a-las-175139.big.png',
                            image_id: 3067200,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686559/',
                            type: 'big',
                            width: 304
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.large.png',
                            filesize: 61897,
                            height: 600,
                            id: 11686558,
                            image: 'captura-de-pantalla-2021-03-10-a-las-175139.large.png',
                            image_id: 3067200,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686558/',
                            type: 'large',
                            width: 396
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.medium.png',
                            filesize: 12760,
                            height: 240,
                            id: 11686560,
                            image: 'captura-de-pantalla-2021-03-10-a-las-175139.medium.png',
                            image_id: 3067200,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686560/',
                            type: 'medium',
                            width: 159
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.small.png',
                            filesize: 6717,
                            height: 160,
                            id: 11686561,
                            image: 'captura-de-pantalla-2021-03-10-a-las-175139.small.png',
                            image_id: 3067200,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686561/',
                            type: 'small',
                            width: 106
                        },
                        {
                            absolute_path: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.thumbnail.png',
                            filesize: 4981,
                            height: 100,
                            id: 11686562,
                            image: 'captura-de-pantalla-2021-03-10-a-las-175139.thumbnail.png',
                            image_id: 3067200,
                            is_global: false,
                            mimetype: 'image/png',
                            resource_uri: '/api/v2/images/versions/11686562/',
                            type: 'thumbnail',
                            width: 100
                        }
                    ],
                    whitelabel_id: 170,
                    width: 600
                },
                image_id: 3067200,
                length: '68.00',
                msrp_price: null,
                outgoing_url: null,
                price: '3664.00',
                primary_image_url: '/img/upload/captura-de-pantalla-2021-03-10-a-las-175139.png',
                product_images: [],
                published: true,
                quantity: 20,
                resource_uri: '/api/v2/products/297020/',
                sale_price: null,
                shop_id: 1821,
                sku: null,
                slug: 'librero-pared',
                status: null,
                title: 'Librero Pared',
                upc: null,
                variations: [
                    {
                        id: 217,
                        name: 'Tono de Acabado',
                        options: [
                            {
                                id: 484,
                                name: 'Natural Poliuretano Mate',
                                order: 0,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 485,
                                name: 'Miel',
                                order: 1,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 486,
                                name: 'Olivo',
                                order: 2,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 487,
                                name: 'Nogal Clásico',
                                order: 3,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 488,
                                name: 'Nogal Americano',
                                order: 4,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 489,
                                name: 'Azul Mancha',
                                order: 5,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 490,
                                name: 'Verde Vintage',
                                order: 6,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 491,
                                name: 'Rojo Mancha',
                                order: 7,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 492,
                                name: 'Gris Tornasol',
                                order: 8,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 493,
                                name: 'Negro Mate',
                                order: 9,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 494,
                                name: 'Verde Militar',
                                order: 10,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 495,
                                name: 'Gris Concreto',
                                order: 11,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 496,
                                name: 'Chocolate Mancha',
                                order: 12,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 497,
                                name: 'Concordia',
                                order: 13,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 498,
                                name: 'Blanco Avejentado',
                                order: 14,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 499,
                                name: 'Blanco Mancha',
                                order: 15,
                                price: '0.00',
                                variation_id: 217
                            },
                            {
                                id: 500,
                                name: 'Blanco Laca Cerrado',
                                order: 16,
                                price: '0.00',
                                variation_id: 217
                            }
                        ],
                        required: true,
                        resource_uri: '/api/v2/products/variations/217/',
                        shop_id: 1821
                    }
                ],
                vendor: null,
                weight: '25.0',
                weight_unit: 'lb',
                wholesale_price: null,
                width: '32.00'
            }
        ];

        const responseOk: IHttpBasicResponse<IProductApiModel[]> = {
            status: 'success',
            data
        };

        return of(responseOk).pipe(delay(800));
    }
    createProduct(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    updateProduct(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }
    deleteProduct(payload: any): Observable<IHttpBasicResponse<any>> {
        throw new Error('Method not implemented.');
    }

}
