import { ICategoryApiModel } from '../repositories/IStore.api';

export class CategoryModel implements CategoryModelProps {

    constructor(data: CategoryModelProps) {
        this.active = data.active;
        this.categoryImages = data.categoryImages;
        this.description = data.description;
        this.id = data.id;
        this.name = data.name;
        this.parentId = data.parentId;
        this.resourceUri = data.resourceUri;
        this.shopId = data.shopId;
        this.slug = data.slug;
    }

    active: boolean;
    categoryImages: number[];
    description: string;
    id: number;
    name: string;
    parentId?: any;
    resourceUri: string;
    shopId: number;
    slug: string;

    static fromApi(data: ICategoryApiModel) {
        let categoryImages = [];
        if (Array.isArray(data.category_images)) {
            categoryImages = data.category_images;
        }
        else if (typeof data.category_images === 'object') {
            Object.keys(data.category_images).forEach(key => {
                categoryImages.push(data.category_images[key].url);
            });
        }

        return new CategoryModel({
            active: data.active,
            categoryImages,
            description: data.description,
            id: data.id,
            name: data.name,
            parentId: data.parent_id,
            resourceUri: data.resource_uri,
            shopId: data.shop_id,
            slug: data.slug
        });
    }

    static empty() {
        return new CategoryModel({
            active: null,
            categoryImages: [],
            description: null,
            id: null,
            name: null,
            parentId: null,
            resourceUri: null,
            shopId: null,
            slug: null,
        });
    }
}

export interface CategoryModelProps {
    active: boolean;
    categoryImages: number[];
    description: string;
    id: number;
    name: string;
    parentId?: any;
    resourceUri: string;
    shopId: number;
    slug: string;
}
