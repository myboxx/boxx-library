import { IImageApiModel } from '../repositories/IStore.api';

export class ImageModel implements ImageModelPorps {
    constructor(data: ImageModelPorps) {
        this.absolutePath = data.absolutePath;
        this.added = data.added;
        this.alt = data.alt;
        this.baseUrl = data.baseUrl;
        this.checksum = data.checksum;
        this.filesize = data.filesize;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.isGlobal = data.isGlobal;
        this.lastIncrement = data.lastIncrement;
        this.mimetype = data.mimetype;
        this.modified = data.modified;
        this.resourceUri = data.resourceUri;
        this.siteId = data.siteId;
        this.sizes = data.sizes;
        this.type = data.type;
        this.userId = data.userId;
        this.versions = data.versions;
        this.whitelabelId = data.whitelabelId;
        this.width = data.width;
    }

    absolutePath: string;
    added: string;
    alt?: any;
    baseUrl?: string;
    checksum: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    isGlobal: boolean;
    lastIncrement: number;
    mimetype: string;
    modified: string;
    resourceUri: string;
    siteId: number;
    sizes: IImageModelSizes;
    type: string;
    userId: number;
    versions: IImageModelVersions[];
    whitelabelId: number;
    width: number;

    static fromApi(data: IImageApiModel) {
        const versions = data.versions.map(v => ({
            absolutePath: v.absolute_path,
            filesize: v.filesize,
            height: v.height,
            id: v.id,
            image: v.image,
            imageId: v.image_id,
            isGlobal: v.is_global,
            mimetype: v.mimetype,
            resourceUri: v.resource_uri,
            type: v.type,
            width: v.width,
        }));

        return new ImageModel({
            absolutePath: data.absolute_path,
            added: data.added,
            alt: data.alt,
            baseUrl: data.base_url,
            checksum: data.checksum,
            filesize: data.filesize,
            height: data.height,
            id: data.id,
            image: data.image,
            isGlobal: data.is_global,
            lastIncrement: data.last_increment,
            mimetype: data.mimetype,
            modified: data.modified,
            resourceUri: data.resource_uri,
            siteId: data.site_id,
            sizes: data.sizes,
            type: data.type,
            userId: data.user_id,
            versions,
            whitelabelId: data.whitelabel_id,
            width: data.width,
        });
    }

    static empty() {
        return new ImageModel({
            absolutePath: null,
            added: null,
            alt: null,
            baseUrl: null,
            checksum: null,
            filesize: null,
            height: null,
            id: null,
            image: null,
            isGlobal: null,
            lastIncrement: null,
            mimetype: null,
            modified: null,
            resourceUri: null,
            siteId: null,
            sizes: null,
            type: null,
            userId: null,
            versions: null,
            whitelabelId: null,
            width: null,
        });
    }
}

export interface ImageModelPorps {
    absolutePath: string;
    added: string;
    alt?: any;
    baseUrl?: string;
    checksum: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    isGlobal: boolean;
    lastIncrement: number;
    mimetype: string;
    modified: string;
    resourceUri: string;
    siteId: number;
    sizes: IImageModelSizes;
    type: string;
    userId: number;
    versions: Array<IImageModelVersions>;
    whitelabelId: number;
    width: number;
}

export interface IImageModelSizes {
    big: string;
    large: string;
    medium: string;
    original: string;
    small: string;
    thumbnail: string;
}

export interface IImageModelVersions {
    absolutePath: string;
    filesize: number;
    height: number;
    id: number;
    image: string;
    imageId: number;
    isGlobal: boolean;
    mimetype: string;
    resourceUri: string;
    type: string;
    width: number;
}
