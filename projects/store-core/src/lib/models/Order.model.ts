import { IOrderApiModel } from '../repositories/IStore.api';

export class OrderModel implements OrderModelProps {

    constructor(data: OrderModelProps) {
        this.buyerEmail = data.buyerEmail;
        this.buyerName = data.buyerName;
        this.id = data.id;
        this.price = data.price;
        this.resourceUri = data.resourceUri;
        this.sessionId = data.sessionId;
        this.shipToAddress = data.shipToAddress;
        this.shipToCity = data.shipToCity;
        this.shipToCountry = data.shipToCountry;
        this.shipToPostalCode = data.shipToPostalCode;
        this.shipToState = data.shipToState;
        this.shippingCost = data.shippingCost;
        this.shippingId = data.shippingId;
        this.shippingName = data.shippingName;
        this.siteId = data.siteId;
        this.status = data.status;
        this.timestamp = data.timestamp;
        this.transactionId = data.transactionId;
        this.whitelabelId = data.whitelabelId;
    }

    buyerEmail: string;
    buyerName: string;
    id: number;
    price: number;
    resourceUri: string;
    sessionId: string;
    shipToAddress: string;
    shipToCity: string;
    shipToCountry: string;
    shipToPostalCode: string;
    shipToState?: any;
    shippingCost?: any;
    shippingId: string;
    shippingName: string;
    siteId: number;
    status: 'new' | string;
    timestamp: string;
    transactionId?: any;
    whitelabelId: number;


    static fromApi(data: IOrderApiModel) {
        return new OrderModel({
            buyerEmail: data.buyer_email,
            buyerName: data.buyer_name,
            id: data.id,
            price: data.price,
            resourceUri: data.resource_uri,
            sessionId: data.session_id,
            shipToAddress: data.ship_to_address,
            shipToCity: data.ship_to_city,
            shipToCountry: data.ship_to_country,
            shipToPostalCode: data.ship_to_postal_code,
            shipToState: data.ship_to_state,
            shippingCost: data.shipping_cost,
            shippingId: data.shipping_id,
            shippingName: data.shipping_name,
            siteId: data.site_id,
            status: data.status,
            timestamp: data.timestamp,
            transactionId: data.transaction_id,
            whitelabelId: data.whitelabel_id
        });
    }

    static empty() {
        return new OrderModel({
            buyerEmail: null,
            buyerName: null,
            id: null,
            price: null,
            resourceUri: null,
            sessionId: null,
            shipToAddress: null,
            shipToCity: null,
            shipToCountry: null,
            shipToPostalCode: null,
            shipToState: null,
            shippingCost: null,
            shippingId: null,
            shippingName: null,
            siteId: null,
            status: null,
            timestamp: null,
            transactionId: null,
            whitelabelId: null
        });
    }
}

export interface OrderModelProps {
    buyerEmail: string;
    buyerName: string;
    id: number;
    price: number;
    resourceUri: string;
    sessionId: string;
    shipToAddress: string;
    shipToCity: string;
    shipToCountry: string;
    shipToPostalCode: string;
    shipToState?: any;
    shippingCost?: any;
    shippingId: string;
    shippingName: string;
    siteId: number;
    status: 'new' | string;
    timestamp: string;
    transactionId?: any;
    whitelabelId: number;
}
