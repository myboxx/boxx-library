import { IProductApiModel, IVariationApiModel } from '../repositories/IStore.api';
import { CategoryModel } from './Category.model';

export class ProductModel implements ProductModelProps {

    constructor(data: ProductModelProps) {
        this.active = data.active;
        this.added = data.added;
        this.asin = data.asin;
        this.autoStock = data.autoStock;
        this.brand = data.brand;
        this.categories = data.categories;
        this.description = data.description;
        this.excerpt = data.excerpt;
        this.height = data.height;
        this.id = data.id;
        this.image = data.image;
        this.imageId = data.imageId;
        this.length = data.length;
        this.msrpPrice = data.msrpPrice;
        this.outgoingUrl = data.outgoingUrl;
        this.price = data.price;
        this.primaryImageUrl = data.primaryImageUrl;
        this.productImages = data.productImages;
        this.published = data.published;
        this.quantity = data.quantity;
        this.resourceUri = data.resourceUri;
        this.salePrice = data.salePrice;
        this.shopId = data.shopId;
        this.sku = data.sku;
        this.slug = data.slug;
        this.status = data.status;
        this.title = data.title;
        this.upc = data.upc;
        this.variations = data.variations;
        this.vendor = data.vendor;
        this.weight = data.weight;
        this.weightUnit = data.weightUnit;
        this.wholesalePrice = data.wholesalePrice;
        this.width = data.width;
    }

    active: boolean;
    added?: string;
    asin?: any;
    autoStock: boolean;
    brand?: any;
    categories: CategoryModel[];
    description: string;
    excerpt: string;
    height: string;
    id: number;
    image: { id: number; url: string; };
    imageId?: number;
    length: string;
    msrpPrice?: any;
    outgoingUrl: string;
    price: string;
    primaryImageUrl: string;
    productImages: { [key: string]: { id: number; url: string; }; };
    published: boolean;
    quantity: number;
    resourceUri: string;
    salePrice: string;
    shopId: number;
    sku: string;
    slug: string;
    status: string;
    title: string;
    upc: any;
    variations: ProductVariationModel[];
    vendor: any;
    weight: string;
    weightUnit?: string;
    wholesalePrice: string;
    width: string;


    static fromApi(data: IProductApiModel) {
        const categories = data.categories.map(c => CategoryModel.fromApi(c));

        let productImages: {} = {};
        if (data.product_images) {
            if (!Array.isArray(data.product_images)) {
                productImages = data.product_images;
            }
            else {
                data.product_images.forEach((img, idx) => productImages[idx] = img);
            }
        }


        const variations = data.variations.map(v => ProductVariationModel.fromApi(v));

        return new ProductModel({
            active: data.active,
            added: data.added,
            asin: data.asin,
            autoStock: data.auto_stock,
            brand: data.brand,
            categories,
            description: data.description,
            excerpt: data.excerpt,
            height: data.height,
            id: data.id,
            image: data.image ? { id: data.image.id, url: data.image.base_url + '/' + data.image.absolute_path } : null,
            imageId: data.image_id,
            length: data.length,
            msrpPrice: data.msrp_price,
            outgoingUrl: data.outgoing_url,
            price: data.price,
            primaryImageUrl: data.primary_image_url,
            productImages,
            published: data.published,
            quantity: data.quantity,
            resourceUri: data.resource_uri,
            salePrice: data.sale_price,
            shopId: data.shop_id,
            sku: data.sku,
            slug: data.slug,
            status: data.status,
            title: data.title,
            upc: data.upc,
            variations,
            vendor: data.vendor,
            weight: data.weight,
            weightUnit: data.weight_unit,
            wholesalePrice: data.wholesale_price,
            width: data.width,
        });
    }

    static empty() {
        return new ProductModel({
            active: null,
            added: null, // date
            asin: null, // ????
            autoStock: null,
            brand: null, // ????
            categories: null,
            description: null,
            excerpt: null,
            height: null,
            id: null,
            image: null,
            imageId: null, // ???
            length: null,
            msrpPrice: null, // ????
            outgoingUrl: null,
            price: null,
            primaryImageUrl: null,
            productImages: null,
            published: null,
            quantity: null,
            resourceUri: null, // ????
            salePrice: null, // ????
            shopId: null, // ????
            sku: null,
            slug: null, // ????
            status: null,
            title: null,
            upc: null, // ????
            variations: null,
            vendor: null, // ????
            weight: null,
            weightUnit: null,
            wholesalePrice: null, // ????
            width: null,
        });
    }
}

export interface ProductModelProps {
    active: boolean;
    added?: string; // date
    asin?: any; // ????
    autoStock: boolean;
    brand?: any; // ????
    categories: CategoryModel[];
    description: string;
    excerpt: string;
    height: string;
    id: number;
    image: { id: number, url: string };
    imageId?: number; // ???
    length: string;
    msrpPrice?: any; // ????
    outgoingUrl: string;
    price: string;
    primaryImageUrl: string;
    productImages: { [key: string]: { id: number, url: string } };
    published: boolean;
    quantity: number;
    resourceUri: string; // ????
    salePrice: string; // ????
    shopId: number; // ????
    sku: string;
    slug: string; // ????
    status: string;
    title: string;
    upc: any; // ????
    variations: ProductVariationModel[];
    vendor: any; // ????
    weight: string;
    weightUnit?: string;
    wholesalePrice: string; // ????
    width: string;
}

export class ProductVariationModel implements ProductVariationModelProps {
    constructor(data: ProductVariationModelProps) {
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.resourceUri = data.resourceUri;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }

    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;

    static fromApi(data: IVariationApiModel) {
        const options = data.options.map(o => ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }));

        return new ProductVariationModel({
            id: data.id,
            name: data.name,
            options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id
        });
    }

    static empty() {
        return new ProductVariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null
        });
    }
}

export interface ProductVariationModelProps {
    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
}
