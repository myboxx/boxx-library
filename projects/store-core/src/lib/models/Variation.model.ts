import { IVariationApiModel } from '../repositories/IStore.api';

export class VariationModel implements VariationModelProps {
    constructor(data: VariationModelProps){
        this.id = data.id;
        this.name = data.name;
        this.options = data.options;
        this.required = data.required;
        this.shop = data.shop;
        this.shopId = data.shopId;
    }

    id: number;
    name: string;
    options: { id: number; name: string; order: number; price: string; variationId: number; }[];
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;

    static fromApi(data: IVariationApiModel) {
        const options = data.options.map(o => ({
            id: o.id,
            name: o.name,
            order: o.order,
            price: o.price,
            variationId: o.variation_id
        }));

        return new VariationModel({
            id: data.id,
            name: data.name,
            options,
            required: data.required,
            resourceUri: data.resource_uri,
            shop: data.shop,
            shopId: data.shop_id,
        });
    }

    static empty() {
        return new VariationModel({
            id: null,
            name: null,
            options: null,
            required: null,
            resourceUri: null,
            shop: null,
            shopId: null,
        });
    }
 }

export interface VariationModelProps {
    id: number;
    name: string;
    options: Array<{
        id: number;
        name: string;
        order: number;
        price: string;
        variationId: number;
    }>;
    required: boolean;
    resourceUri: string;
    shop?: number;
    shopId: number;
}
