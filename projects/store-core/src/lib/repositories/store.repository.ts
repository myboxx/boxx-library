import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
    AbstractAppConfigService,
    APP_CONFIG_SERVICE,
    IHttpBasicResponse,
} from '@boxx/core';
import { Observable } from 'rxjs';
import {
    ICategoryApiModel,
    ICreateVariationPayload,
    IImageApiModel,
    IOrderApiModel,
    IProductApiModel,
    IUpsertCategoryPayload,
    IUpsertProductPayload,
    IVariationApiModel,
} from './IStore.api';
import { IStoreRepository } from './IStore.repository';

@Injectable()
export class StoreRepository implements IStoreRepository {
    constructor(
        @Inject(APP_CONFIG_SERVICE)
        private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient
    ) {}

    // --------------------- CATEGORIES: -------------------------
    loadCategories(
        pagination: string = ''
    ): Observable<IHttpBasicResponse<ICategoryApiModel[]>> {
        return this.httpClient.get<IHttpBasicResponse<ICategoryApiModel[]>>(
            `${this.getBaseUrl()}/categories${pagination}`
        );
    }
    loadCategoryById(
        id: number
    ): Observable<IHttpBasicResponse<ICategoryApiModel>> {
        return this.httpClient.get<IHttpBasicResponse<ICategoryApiModel>>(
            `${this.getBaseUrl()}/categories/${id}`
        );
    }
    createCategory(
        payload: IUpsertCategoryPayload
    ): Observable<IHttpBasicResponse<ICategoryApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.post<IHttpBasicResponse<ICategoryApiModel>>(
            `${this.getBaseUrl()}/create_category`,
            body
        );
    }
    deleteCategory(id: number): Observable<IHttpBasicResponse<null>> {
        return this.httpClient.delete<IHttpBasicResponse<null>>(
            `${this.getBaseUrl()}/delete_category/${id}`
        );
    }

    // --------------------- ORDERS: -------------------------
    loadOrders(
        pagination: string = ''
    ): Observable<IHttpBasicResponse<IOrderApiModel[]>> {
        return this.httpClient.get<IHttpBasicResponse<IOrderApiModel[]>>(
            `${this.getBaseUrl()}/orders${pagination}`
        );
    }
    loadOrderById(id: number): Observable<IHttpBasicResponse<IOrderApiModel>> {
        return this.httpClient.get<IHttpBasicResponse<IOrderApiModel>>(
            `${this.getBaseUrl()}/orders/${id}`
        );
    }
    updateOrder(
        id: number,
        payload: any
    ): Observable<IHttpBasicResponse<IOrderApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.post<IHttpBasicResponse<IOrderApiModel>>(
            `${this.getBaseUrl()}/update_order/${id}`,
            body
        );
    }

    // --------------------- PRODUCTS: -------------------------
    loadProducts(): Observable<IHttpBasicResponse<IProductApiModel[]>> {
        return this.httpClient.get<IHttpBasicResponse<IProductApiModel[]>>(
            `${this.getBaseUrl()}/products`
        );
    }
    loadProductById(
        id: number
    ): Observable<IHttpBasicResponse<IProductApiModel>> {
        return this.httpClient.get<IHttpBasicResponse<IProductApiModel>>(
            `${this.getBaseUrl()}/products/${id}`
        );
    }
    createProduct(
        payload: IUpsertProductPayload
    ): Observable<IHttpBasicResponse<IProductApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.post<IHttpBasicResponse<IProductApiModel>>(
            `${this.getBaseUrl()}/create_product`,
            body
        );
    }
    updateProduct(
        id: number,
        payload: IUpsertProductPayload
    ): Observable<IHttpBasicResponse<IProductApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.put<IHttpBasicResponse<IProductApiModel>>(
            `${this.getBaseUrl()}/update_product/${id}`,
            body
        );
    }
    deleteProduct(id: number): Observable<IHttpBasicResponse<null>> {
        return this.httpClient.delete<IHttpBasicResponse<null>>(
            `${this.getBaseUrl()}/delete_product/${id}`
        );
    }

    // ---------------- PRODUCT VARIATIONS: ---------------------
    loadProductsVariations(
        pagination: string = ''
    ): Observable<IHttpBasicResponse<IVariationApiModel[]>> {
        return this.httpClient.get<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/product_variations${pagination}`
        );
    }
    loadProductVariationByVariationId(
        id: number
    ): Observable<IHttpBasicResponse<IVariationApiModel>> {
        return this.httpClient.get<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/product_variations/${id}`
        );
    }
    createProductVariation(
        payload: ICreateVariationPayload
    ): Observable<IHttpBasicResponse<IVariationApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.post<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/create_product_variation`,
            body
        );
    }
    deleteProductVariation(id: number): Observable<IHttpBasicResponse<null>> {
        return this.httpClient.delete<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/delete_product_variation/${id}`
        );
    }

    // --------------------- IMAGES: -------------------------
    loadSiteImages(
        pagination: string = ''
    ): Observable<IHttpBasicResponse<IImageApiModel[]>> {
        return this.httpClient.get<IHttpBasicResponse<any[]>>(
            `${this.getBaseUrl()}/site_images${pagination}`
        );
    }
    loadSiteImageById(
        id: number
    ): Observable<IHttpBasicResponse<IImageApiModel>> {
        return this.httpClient.get<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/site_images/${id}`
        );
    }
    createSiteImage(
        payload: number
    ): Observable<IHttpBasicResponse<IImageApiModel>> {
        const body = this.createHttParams(payload);
        return this.httpClient.post<IHttpBasicResponse<IImageApiModel>>(
            `${this.getBaseUrl()}/create_site_image`,
            body
        );
    }
    deleteSiteImage(id: number): Observable<IHttpBasicResponse<null>> {
        return this.httpClient.delete<IHttpBasicResponse<any>>(
            `${this.getBaseUrl()}/delete_site_image/${id}`
        );
    }

    createHttParams(payload: any) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                typeof payload[key] === 'object'
                    ? (params = params.append(
                          key,
                          JSON.stringify(payload[key])
                      ))
                    : (params = params.append(key, payload[key]));
            }
        }

        return params.toString();
    }

    private getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/shop`;
    }
}
