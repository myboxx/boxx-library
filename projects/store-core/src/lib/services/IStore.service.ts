import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
import {
    ICreateVariationPayload,
    IUpsertCategoryPayload,
    IUpsertProductPayload,
} from '../repositories/IStore.api';

export interface IStoreService {
    // --------------------- CATEGORIES: -------------------------
    loadCategories(): Observable<CategoryModel[]>;
    loadCategoryById(id: number): Observable<CategoryModel>;
    createCategory(payload: IUpsertCategoryPayload): Observable<CategoryModel>;
    deleteCategory(payload: any): Observable<boolean>;

    // --------------------- ORDERS: -------------------------
    loadOrders(): Observable<OrderModel[]>;
    loadOrderById(id: number): Observable<OrderModel>;
    updateOrder(id: number, payload: any): Observable<OrderModel>;

    // --------------------- PRODUCTS: -------------------------
    loadProducts(): Observable<ProductModel[]>;
    loadProductById(id: number): Observable<ProductModel>;
    createProduct(payload: IUpsertProductPayload): Observable<ProductModel>;
    updateProduct(
        id: number,
        payload: IUpsertProductPayload
    ): Observable<ProductModel>;
    deleteProduct(id: number): Observable<boolean>;

    // ---------------- PRODUCT VARIATIONS: ---------------------
    loadProductsVariations(): Observable<VariationModel[]>;
    loadProductVariationById(id: number): Observable<VariationModel>;
    createProductVariation(
        payload: ICreateVariationPayload
    ): Observable<VariationModel>;
    deleteProductVariation(id: number): Observable<boolean>;

    // --------------------- IMAGES: -------------------------
    loadSiteImages(): Observable<ImageModel[]>;
    loadSiteImageById(id: number): Observable<ImageModel>;
    createSiteImage(payload: number): Observable<ImageModel>;
    deleteSiteImage(id: number): Observable<boolean>;
}

export const STORE_SERVICE = new InjectionToken<IStoreService>('StoreService');
