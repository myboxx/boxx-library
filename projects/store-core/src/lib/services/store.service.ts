import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
import { ICreateVariationPayload, IUpsertCategoryPayload, IUpsertProductPayload, IVariationApiModel } from '../repositories/IStore.api';
import { IStoreRepository, STORE_REPOSITORY } from '../repositories/IStore.repository';
import { IStoreService } from './IStore.service';

@Injectable({
    providedIn: 'root'
})
export class StoreService implements IStoreService {
    constructor(
        @Inject(STORE_REPOSITORY) private repository: IStoreRepository
    ) { }

    // --------------------- CATEGORIES: -------------------------
    loadCategories(): Observable<CategoryModel[]> {
        return this.repository.loadCategories().pipe(
            map(response => response.data.map(c => CategoryModel.fromApi(c))),
            catchError(error => { throw error; })
        );
    }
    loadCategoryById(id: number): Observable<CategoryModel> {
        return this.repository.loadCategoryById(id).pipe(
            map(response => CategoryModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    createCategory(payload: IUpsertCategoryPayload): Observable<CategoryModel> {
        return this.repository.createCategory(payload).pipe(
            map(response => CategoryModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    deleteCategory(payload: any): Observable<boolean> {
        return this.repository.deleteCategory(payload).pipe(
            map(resp => resp.status === 'success'),
            catchError(error => { throw error; })
        );
    }

    // --------------------- ORDERS: -------------------------
    loadOrders(): Observable<OrderModel[]> {
        return this.repository.loadOrders().pipe(
            map(response => response.data.map(o => OrderModel.fromApi(o))),
            catchError(error => { throw error; })
        );
    }
    loadOrderById(id: number): Observable<OrderModel> {
        return this.repository.loadOrderById(id).pipe(
            map(response => OrderModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    updateOrder(id: number, payload: any): Observable<OrderModel> {
        return this.repository.updateOrder(id, payload).pipe(
            map(response => OrderModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }

    // --------------------- PRODUCTS: -------------------------
    loadProducts(): Observable<ProductModel[]> {
        return this.repository.loadProducts().pipe(
            map(response => response.data.map(p => ProductModel.fromApi(p))),
            catchError(error => { throw error; })
        );
    }
    loadProductById(id: number): Observable<ProductModel> {
        return this.repository.loadProductById(id).pipe(
            map(response => ProductModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    createProduct(payload: IUpsertProductPayload): Observable<ProductModel> {
        return this.repository.createProduct(payload).pipe(
            map(response => ProductModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    updateProduct(id: number, payload: IUpsertProductPayload): Observable<ProductModel> {
        return this.repository.updateProduct(id, payload).pipe(
            map(response => ProductModel.fromApi(response.data)),
            catchError(error => { throw error; })
        );
    }
    deleteProduct(id: number): Observable<boolean> {
        return this.repository.deleteProduct(id).pipe(
            map(resp => resp.status === 'success'),
            catchError(error => { throw error; })
        );
    }

    // ---------------- PRODUCT VARIATIONS: ---------------------
    loadProductsVariations(): Observable<VariationModel[]> {
        return this.repository.loadProductsVariations().pipe(
            map(resp => resp.data.map(v => VariationModel.fromApi(v))),
            catchError(error => { throw error; })
        );
    }
    loadProductVariationById(id: number): Observable<VariationModel> {
        return this.repository.loadProductVariationByVariationId(id).pipe(
            map(resp => VariationModel.fromApi(resp.data)),
            catchError(error => { throw error; })
        );
    }
    createProductVariation(payload: ICreateVariationPayload): Observable<VariationModel> {
        return this.repository.createProductVariation(payload).pipe(
            map(resp => VariationModel.fromApi(resp.data)),
            catchError(error => { throw error; })
        );
    }
    deleteProductVariation(id: number): Observable<boolean> {
        return this.repository.deleteProductVariation(id).pipe(
            map(resp => resp.status === 'success'),
            catchError(error => { throw error; })
        );
    }

    // --------------------- IMAGES: -------------------------
    loadSiteImages(): Observable<ImageModel[]> {
        return this.repository.loadSiteImages().pipe(
            map(resp => resp.data.map(i => ImageModel.fromApi(i))),
            catchError(error => { throw error; })
        );
    }
    loadSiteImageById(id: number): Observable<ImageModel> {
        throw new Error('Method not implemented');
    }
    createSiteImage(payload: number): Observable<ImageModel> {
        throw new Error('Method not implemented');
    }
    deleteSiteImage(id: number): Observable<boolean> {
        throw new Error('Method not implemented');
    }
}
