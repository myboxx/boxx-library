import { createAction, props } from '@ngrx/store';
import { CategoryModel } from '../models/Category.model';
import { IUpsertCategoryPayload } from '../repositories/IStore.api';

export enum StoreCategoriesActionTypes {
    LoadCategoriesBegin = '[STORE] Load Categories Begin',
    LoadCategoriesSuccess = '[STORE] Load Categories Success',
    LoadCategoriesFail = '[STORE] Load Categories Fail',

    LoadCategoryByIdBegin = '[STORE] Load LoadCategory By Id Begin',
    LoadCategoryByIdSuccess = '[STORE] Load Categorie By Id Success',
    LoadCategoryByIdFail = '[STORE] Load Categorie By Id Fail',

    CreateCategoryBegin = '[STORE] Create Category Begin',
    CreateCategorySuccess = '[STORE] Create Category Success',
    CreateCategoryFail = '[STORE] Create Category Fail',

    DeleteCategoryBegin = '[STORE] Delete Category Begin',
    DeleteCategorySuccess = '[STORE] Delete Category Success',
    DeleteCategoryFail = '[STORE] Delete Category Fail',

    ClearCategoriesSuccessErrorData = '[STORE] Clear Categories Success/Error Data'
}

export const LoadCategoriesBeginAction = createAction(
    StoreCategoriesActionTypes.LoadCategoriesBegin
);

export const LoadCategoriesSuccessAction = createAction(
    StoreCategoriesActionTypes.LoadCategoriesSuccess,
    props<{ payload: CategoryModel[] }>()
);

export const LoadCategoriesFailAction = createAction(
    StoreCategoriesActionTypes.LoadCategoriesFail,
    props<{ errors: any }>()
);

export const LoadCategorieByIdBeginAction = createAction(
    StoreCategoriesActionTypes.LoadCategoryByIdBegin,
    props<{ id: number }>()
);

export const LoadCategorieByIdSuccessAction = createAction(
    StoreCategoriesActionTypes.LoadCategoryByIdSuccess,
    props<{ payload: CategoryModel }>()
);

export const LoadCategorieByIdFailAction = createAction(
    StoreCategoriesActionTypes.LoadCategoryByIdFail,
    props<{ errors: any }>()
);

export const CreateCategoryBeginAction = createAction(
    StoreCategoriesActionTypes.CreateCategoryBegin,
    props<{ payload: IUpsertCategoryPayload }>()
);

export const CreateCategorySuccessAction = createAction(
    StoreCategoriesActionTypes.CreateCategorySuccess,
    props<{ payload: CategoryModel[] }>()
);

export const CreateCategoryFailAction = createAction(
    StoreCategoriesActionTypes.CreateCategoryFail,
    props<{ errors: any }>()
);

export const DeleteCategoryBeginAction = createAction(
    StoreCategoriesActionTypes.DeleteCategoryBegin,
    props<{ id: number }>()
);

export const DeleteCategorySuccessAction = createAction(
    StoreCategoriesActionTypes.DeleteCategorySuccess,
    props<{ payload: CategoryModel[] }>()
);

export const DeleteCategoryFailAction = createAction(
    StoreCategoriesActionTypes.DeleteCategoryFail,
    props<{ errors: any }>()
);

export const ClearCategoriesSuccessErrorDataAction = createAction(
    StoreCategoriesActionTypes.ClearCategoriesSuccessErrorData
);
