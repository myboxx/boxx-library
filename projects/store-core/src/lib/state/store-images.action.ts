import { createAction, props } from '@ngrx/store';
import { ImageModel } from '../models/Image.model';

export enum StoreImagesActionTypes {
    LoadImagesBegin = '[STORE] Load Images Begin',
    LoadImagesSuccess = '[STORE] Load Images Success',
    LoadImagesFail = '[STORE] Load Images Fail',

    LoadImageByIdBegin = '[STORE] Load Image By Id Begin',
    LoadImageByIdSuccess = '[STORE] Load Image By Id Success',
    LoadImageByIdFail = '[STORE] Load Image By Id Fail',

    CreateImageBegin = '[STORE] Create Image Begin',
    CreateImageSuccess = '[STORE] Create Image Success',
    CreateImageFail = '[STORE] Create Image Fail',

    DelteImageBegin = '[STORE] Load Delete  Begin',
    DelteImageSuccess = '[STORE] Load Delete Success',
    DelteImageFail = '[STORE] Delete Image Fail',

    ClearImagesSuccessErrorData = '[STORE] Clear Images Success/Error Data'
}


export const LoadImagesBeginAction = createAction(
    StoreImagesActionTypes.LoadImagesBegin
);

export const LoadImagesSuccessAction = createAction(
    StoreImagesActionTypes.LoadImagesSuccess,
    props<{ payload: ImageModel[] }>()
);

export const LoadImagesFailAction = createAction(
    StoreImagesActionTypes.LoadImagesFail,
    props<{ errors: any }>()
);

export const LoadImageByIdBeginAction = createAction(
    StoreImagesActionTypes.LoadImageByIdBegin,
    props<{ id: number }>()
);

export const LoadImageByIdSuccessAction = createAction(
    StoreImagesActionTypes.LoadImageByIdSuccess,
    props<{ payload: ImageModel }>()
);

export const LoadImageByIdFailAction = createAction(
    StoreImagesActionTypes.LoadImageByIdFail,
    props<{ errors: any }>()
);

export const CreateImageBeginAction = createAction(
    StoreImagesActionTypes.CreateImageBegin,
    props<{ payload: any }>()
);

export const CreateImageSuccessAction = createAction(
    StoreImagesActionTypes.CreateImageSuccess,
    props<{ payload: ImageModel[] }>()
);

export const CreateImageFailAction = createAction(
    StoreImagesActionTypes.CreateImageFail,
    props<{ errors: any }>()
);

export const DeleteImageBeginAction = createAction(
    StoreImagesActionTypes.DelteImageBegin,
    props<{ id: number }>()
);

export const DeleteImageSuccessAction = createAction(
    StoreImagesActionTypes.DelteImageSuccess,
    props<{ payload: ImageModel[] }>()
);

export const DeleteImageFailAction = createAction(
    StoreImagesActionTypes.DelteImageFail,
    props<{ errors: any }>()
);

export const ClearImagesSuccessErrorDataAction = createAction(
    StoreImagesActionTypes.ClearImagesSuccessErrorData
);
