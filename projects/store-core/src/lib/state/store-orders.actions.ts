import { createAction, props } from '@ngrx/store';
import { OrderModel } from '../models/Order.model';

export enum StoreOrdersActionTypes {
    LoadOrdersBegin = '[STORE] Load Orders Begin',
    LoadOrdersSuccess = '[STORE] Load Orders Success',
    LoadOrdersFail = '[STORE] Load Orders Fail',

    LoadOrderByIdBegin = '[STORE] Load Order By Id Begin',
    LoadOrderByIdSuccess = '[STORE] Load Order By Id Success',
    LoadOrderByIdFail = '[STORE] Load Order By Id Fail',

    UpdateOrderBegin = '[STORE] Update Order Begin',
    UpdateOrderSuccess = '[STORE] Update Order Success',
    UpdateOrderFail = '[STORE] Update Order Fail',

    ClearOrdersSuccessErrorData = '[STORE] Clear Orders Success/Error Data'
}

export const LoadOrdersBeginAction = createAction(
    StoreOrdersActionTypes.LoadOrdersBegin
);

export const LoadOrdersSuccessAction = createAction(
    StoreOrdersActionTypes.LoadOrdersSuccess,
    props<{ payload: OrderModel[] }>()
);

export const LoadOrdersFailAction = createAction(
    StoreOrdersActionTypes.LoadOrdersFail,
    props<{ errors: any }>()
);

export const LoadOrderByIdBeginAction = createAction(
    StoreOrdersActionTypes.LoadOrderByIdBegin,
    props<{ id: number }>()
);

export const LoadOrderByIdSuccessAction = createAction(
    StoreOrdersActionTypes.LoadOrderByIdSuccess,
    props<{ payload: OrderModel }>()
);

export const LoadOrderByIdFailAction = createAction(
    StoreOrdersActionTypes.LoadOrderByIdFail,
    props<{ errors: any }>()
);

export const UpdateOrderBeginAction = createAction(
    StoreOrdersActionTypes.UpdateOrderBegin,
    props<{ id: number, payload: any }>()
);

export const UpdateOrderSuccessAction = createAction(
    StoreOrdersActionTypes.UpdateOrderSuccess,
    props<{ payload: OrderModel[] }>()
);

export const UpdateOrderFailAction = createAction(
    StoreOrdersActionTypes.UpdateOrderFail,
    props<{ errors: any }>()
);

export const ClearOrdersSuccessErrorDataAction = createAction(
    StoreOrdersActionTypes.ClearOrdersSuccessErrorData
);
