import { createAction, props } from '@ngrx/store';
import { IUpsertProductPayload } from '../../public-api';
import { ProductModel } from '../models/Product.model';

export enum StoreProductsActionTypes {
    LoadProductsBegin = '[STORE] Load Products Begin',
    LoadProductsSuccess = '[STORE] Load Products Success',
    LoadProductsFail = '[STORE] Load Products Fail',

    LoadProductByIdBegin = '[STORE] Load Product By Id Begin',
    LoadProductByIdSuccess = '[STORE] Load Product By Id Success',
    LoadProductByIdFail = '[STORE] Load Product By Id Fail',

    CreateProductBegin = '[STORE] Create Product Begin',
    CreateProductSuccess = '[STORE] Create Product Success',
    CreateProductFail = '[STORE] Create Product Fail',

    UpdateProductBegin = '[STORE] Update Product Begin',
    UpdateProductSuccess = '[STORE] Update Product Success',
    UpdateProductFail = '[STORE] Update Product Fail',

    DeleteProductBegin = '[STORE] Delete Product Begin',
    DeleteProductSuccess = '[STORE] Delete Product Success',
    DeleteProductFail = '[STORE] Delete Product Fail',

    ClearProductsSuccessErrorData = '[STORE] Clear Products Success/Error Data'
}

export const LoadProductsBeginAction = createAction(
    StoreProductsActionTypes.LoadProductsBegin
);

export const LoadProductsSuccessAction = createAction(
    StoreProductsActionTypes.LoadProductsSuccess,
    props<{ payload: ProductModel[] }>()
);

export const LoadProductsFailAction = createAction(
    StoreProductsActionTypes.LoadProductsFail,
    props<{ errors: any }>()
);

export const LoadProductByIdBeginAction = createAction(
    StoreProductsActionTypes.LoadProductByIdBegin,
    props<{ id: number }>()
);

export const LoadProductByIdSuccessAction = createAction(
    StoreProductsActionTypes.LoadProductByIdSuccess,
    props<{ payload: ProductModel }>()
);

export const LoadProductByIdFailAction = createAction(
    StoreProductsActionTypes.LoadProductByIdFail,
    props<{ errors: any }>()
);

export const CreateProductBeginAction = createAction(
    StoreProductsActionTypes.CreateProductBegin,
    props<{ payload: IUpsertProductPayload }>()
);

export const CreateProductSuccessAction = createAction(
    StoreProductsActionTypes.CreateProductSuccess,
    props<{ payload: ProductModel[] }>()
);

export const CreateProductFailAction = createAction(
    StoreProductsActionTypes.CreateProductFail,
    props<{ errors: any }>()
);

export const UpdateProductBeginAction = createAction(
    StoreProductsActionTypes.UpdateProductBegin,
    props<{ id: number, payload: IUpsertProductPayload }>()
);

export const UpdateProductSuccessAction = createAction(
    StoreProductsActionTypes.UpdateProductSuccess,
    props<{ payload: ProductModel[] }>()
);

export const UpdateProductFailAction = createAction(
    StoreProductsActionTypes.UpdateProductFail,
    props<{ errors: any }>()
);

export const DeleteProductBeginAction = createAction(
    StoreProductsActionTypes.DeleteProductBegin,
    props<{ id: number }>()
);

export const DeleteProductSuccessAction = createAction(
    StoreProductsActionTypes.DeleteProductSuccess,
    props<{ payload: ProductModel[] }>()
);

export const DeleteProductFailAction = createAction(
    StoreProductsActionTypes.DeleteProductFail,
    props<{ errors: any }>()
);

export const ClearProductsSuccessErrorDataAction = createAction(
    StoreProductsActionTypes.ClearProductsSuccessErrorData
);
