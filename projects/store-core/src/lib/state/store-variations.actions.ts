import { createAction, props } from '@ngrx/store';
import { VariationModel } from '../models/Variation.model';
import { ICreateVariationPayload } from '../repositories/IStore.api';

export enum StoreVariationsActionTypes {
    LoadVariationsBegin = '[STORE] Load Variations Begin',
    LoadVariationsSuccess = '[STORE] Load Variations Success',
    LoadVariationsFail = '[STORE] Load Variations Fail',

    LoadVariationByIdBegin = '[STORE] Load Variation By Id Begin',
    LoadVariationByIdSuccess = '[STORE] Load Variation By Id Success',
    LoadVariationByIdFail = '[STORE] Load Variation By Id Fail',

    CreateVariationBegin = '[STORE] Create Variation Begin',
    CreateVariationSuccess = '[STORE] Create Variation Success',
    CreateVariationFail = '[STORE] Create Variation Fail',

    DeleteVariationBegin = '[STORE] Delete Variation Begin',
    DeleteVariationSuccess = '[STORE] Delete Variation Success',
    DeleteVariationFail = '[STORE] Delete Variation Fail',

    ClearVariationsSuccessErrorData = '[STORE] Clear Variations Success/Error Data'
}

export const LoadVariationsBeginAction = createAction(
    StoreVariationsActionTypes.LoadVariationsBegin
);

export const LoadVariationsSuccessAction = createAction(
    StoreVariationsActionTypes.LoadVariationsSuccess,
    props<{ payload: VariationModel[] }>()
);

export const LoadVariationsFailAction = createAction(
    StoreVariationsActionTypes.LoadVariationsFail,
    props<{ errors: any }>()
);

export const LoadVariationByIdBeginAction = createAction(
    StoreVariationsActionTypes.LoadVariationByIdBegin,
    props<{ id: number }>()
);

export const LoadVariationByIdSuccessAction = createAction(
    StoreVariationsActionTypes.LoadVariationByIdSuccess,
    props<{ payload: VariationModel }>()
);

export const LoadVariationByIdFailAction = createAction(
    StoreVariationsActionTypes.LoadVariationByIdFail,
    props<{ errors: any }>()
);

export const CreteVariationBeginAction = createAction(
    StoreVariationsActionTypes.CreateVariationBegin,
    props<{ payload: ICreateVariationPayload }>()
);

export const CreteVariationSuccessAction = createAction(
    StoreVariationsActionTypes.CreateVariationSuccess,
    props<{ payload: VariationModel[] }>()
);

export const CreteVariataionFailAction = createAction(
    StoreVariationsActionTypes.CreateVariationFail,
    props<{ errors: any }>()
);

export const DelteVariationBeginAction = createAction(
    StoreVariationsActionTypes.DeleteVariationBegin,
    props<{ payload: number }>()
);

export const DelteVariationSuccessAction = createAction(
    StoreVariationsActionTypes.DeleteVariationSuccess,
    props<{ payload: VariationModel[] }>()
);

export const DelteVariationFailAction = createAction(
    StoreVariationsActionTypes.DeleteVariationFail,
    props<{ errors: any }>()
);

export const ClearVariationsSuccessErrorDataAction = createAction(
    StoreVariationsActionTypes.ClearVariationsSuccessErrorData
);
