import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, flatMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
import { IStoreService, STORE_SERVICE } from '../services/IStore.service';
import * as fromCategoriesActions from './store-categories.actions';
import * as fromImagesActions from './store-images.action';
import * as fromOrdersActions from './store-orders.actions';
import * as fromProductsActions from './store-products.actions';
import * as fromVariationsActions from './store-variations.actions';
import { StorePageStore } from './store.store';

@Injectable()
export class StoreEffects {
    // --------------------------- CATEGORIES: ---------------------------------
    LoadCategories$ = createEffect(
        () => this.actions.pipe(
            ofType(fromCategoriesActions.LoadCategoriesBeginAction),
            switchMap(() => {
                return this.service.loadCategories().pipe(
                    flatMap(payload => [
                        fromCategoriesActions.LoadCategoriesSuccessAction({ payload }),
                        fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadCategories$ ERROR', errors);
                        return of([
                            fromCategoriesActions.LoadCategoriesFailAction({ errors }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    LoadCategoryById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromCategoriesActions.LoadCategorieByIdBeginAction),
            switchMap((action: any) => {
                return this.service.loadCategoryById(action.id).pipe(
                    flatMap(payload => [
                        fromCategoriesActions.LoadCategorieByIdSuccessAction({ payload }),
                        fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadCategoryById$ ERROR', errors);
                        return of([
                            fromCategoriesActions.LoadCategorieByIdFailAction({ errors }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    CreateCategory$ = createEffect(
        () => this.actions.pipe(
            ofType(fromCategoriesActions.CreateCategoryBeginAction),
            withLatestFrom(this.storePageStore.CategoriesData$),
            switchMap(([action, categoriesData]: [any, CategoryModel[]]) => {
                return this.service.createCategory((action as any).payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...categoriesData];
                        return [
                            fromCategoriesActions.CreateCategorySuccessAction({ payload }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.CreateCategory$ ERROR', errors);
                        return of([
                            fromCategoriesActions.CreateCategoryFailAction({ errors }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    DeleteCategory$ = createEffect(
        () => this.actions.pipe(
            ofType(fromCategoriesActions.DeleteCategoryBeginAction),
            withLatestFrom(this.storePageStore.CategoriesData$),
            switchMap(([action, categoriesData]: [any, CategoryModel[]]) => {
                return this.service.deleteCategory(action.id).pipe(
                    flatMap(() => {
                        const payload = categoriesData.filter(c =>  c.id !== action.id);
                        return [
                            fromCategoriesActions.DeleteCategorySuccessAction({ payload }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.DeleteCategory$ ERROR', errors);
                        return of([
                            fromCategoriesActions.DeleteCategoryFailAction({ errors }),
                            fromCategoriesActions.ClearCategoriesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    // --------------------------- ORDERS: ---------------------------------
    LoadOrders$ = createEffect(
        () => this.actions.pipe(
            ofType(fromOrdersActions.LoadOrdersBeginAction),
            switchMap(() => {
                return this.service.loadOrders().pipe(
                    flatMap(data => [
                        fromOrdersActions.LoadOrdersSuccessAction({ payload: data }),
                        fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadOrders$ ERROR', errors);
                        return of([
                            fromOrdersActions.LoadOrdersFailAction({ errors }),
                            fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    LoadOrderById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromOrdersActions.LoadOrderByIdBeginAction),
            switchMap((action: any) => {
                return this.service.loadOrderById(action.id).pipe(
                    flatMap(payload => [
                        fromOrdersActions.LoadOrderByIdSuccessAction({ payload }),
                        fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadOrderById$ ERROR', errors);
                        return of([
                            fromOrdersActions.LoadOrderByIdFailAction({ errors }),
                            fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    UpdateOrderById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromOrdersActions.UpdateOrderBeginAction),
            withLatestFrom(this.storePageStore.OrdersData$),
            switchMap(([action, ordersData]: [any, OrderModel[]]) => {
                return this.service.updateOrder(action.id, action.payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...ordersData.filter(o => o.id === action.id)];
                        return [
                            fromOrdersActions.UpdateOrderSuccessAction({ payload }),
                            fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.UpdateOrderById$ ERROR', errors);
                        return of([
                            fromOrdersActions.UpdateOrderFailAction({ errors }),
                            fromOrdersActions.ClearOrdersSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    // --------------------------- PRODUCTS: ---------------------------------
    LoadProducts$ = createEffect(
        () => this.actions.pipe(
            ofType(fromProductsActions.LoadProductsBeginAction),
            switchMap(() => {
                return this.service.loadProducts().pipe(
                    flatMap(payload => [
                        fromProductsActions.LoadProductsSuccessAction({ payload }),
                        fromProductsActions.ClearProductsSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadProducts$ ERROR', errors);
                        return of([
                            fromProductsActions.LoadProductsFailAction({ errors }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    LoadProductById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromProductsActions.LoadProductByIdBeginAction),
            switchMap((action: any) => {
                return this.service.loadProductById(action.id).pipe(
                    flatMap(payload => [
                        fromProductsActions.LoadProductByIdSuccessAction({ payload }),
                        fromProductsActions.ClearProductsSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadProductById$ ERROR', errors);
                        return of([
                            fromProductsActions.LoadProductByIdFailAction({ errors }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    CreateProduct$ = createEffect(
        () => this.actions.pipe(
            ofType(fromProductsActions.CreateProductBeginAction),
            withLatestFrom(this.storePageStore.ProductsData$),
            switchMap(([action, productsData]: [any, ProductModel[]]) => {
                return this.service.createProduct(action.payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...productsData];
                        return [
                            fromProductsActions.CreateProductSuccessAction({ payload }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.CreateProducts$ ERROR', errors);
                        return of([
                            fromProductsActions.CreateProductFailAction({ errors }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    UpdateProduct$ = createEffect(
        () => this.actions.pipe(
            ofType(fromProductsActions.UpdateProductBeginAction),
            withLatestFrom(this.storePageStore.ProductsData$),
            switchMap(([action, productsData]: [any, ProductModel[]]) => {
                return this.service.updateProduct(action.id, action.payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...productsData.filter(p => p.id !== action.id)];
                        return [
                            fromProductsActions.UpdateProductSuccessAction({ payload }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.UpdateProducts$ ERROR', errors);
                        return of([
                            fromProductsActions.UpdateProductFailAction({ errors }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    DeleteProduct$ = createEffect(
        () => this.actions.pipe(
            ofType(fromProductsActions.DeleteProductBeginAction),
            withLatestFrom(this.storePageStore.ProductsData$),
            switchMap(([action, productsData]: [any, ProductModel[]]) => {
                return this.service.deleteProduct(action.id).pipe(
                    flatMap(() => {
                        const payload = productsData.filter(p => p.id !== action.id);
                        return [
                            fromProductsActions.DeleteProductSuccessAction({ payload }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.DeleteProduct$ ERROR', errors);
                        return of([
                            fromProductsActions.DeleteProductFailAction({ errors }),
                            fromProductsActions.ClearProductsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    // --------------------------- PRODUCTS: ---------------------------------
    LoadVariations$ = createEffect(
        () => this.actions.pipe(
            ofType(fromVariationsActions.LoadVariationsBeginAction),
            switchMap(() => {
                return this.service.loadProductsVariations().pipe(
                    flatMap(data => [
                        fromVariationsActions.LoadVariationsSuccessAction({ payload: data }),
                        fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadVariations$ ERROR', errors);
                        return of([
                            fromVariationsActions.LoadVariationsFailAction({ errors }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    LoadVariationById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromVariationsActions.LoadVariationByIdBeginAction),
            switchMap((action: any) => {
                return this.service.loadProductVariationById(action.id).pipe(
                    flatMap(payload => [
                        fromVariationsActions.LoadVariationByIdSuccessAction({ payload }),
                        fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadVariationById$ ERROR', errors);
                        return of([
                            fromVariationsActions.LoadVariationByIdFailAction({ errors }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    CreateVariation$ = createEffect(
        () => this.actions.pipe(
            ofType(fromVariationsActions.CreteVariationBeginAction),
            withLatestFrom(this.storePageStore.VariationsData$),
            switchMap(([action, variationsData]) => {
                return this.service.createProductVariation(action.payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...variationsData];
                        return [
                            fromVariationsActions.CreteVariationSuccessAction({ payload }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.CreateVariation$ ERROR', errors);
                        return of([
                            fromVariationsActions.CreteVariataionFailAction({ errors }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    DeleteVariation$ = createEffect(
        () => this.actions.pipe(
            ofType(fromVariationsActions.DelteVariationBeginAction),
            withLatestFrom(this.storePageStore.VariationsData$),
            switchMap(([action, variationsData]: [any, VariationModel[]]) => {
                return this.service.deleteProductVariation(action.id).pipe(
                    flatMap(() => {
                        const payload = variationsData.filter(v => v.id !== action.id);
                        return [
                            fromVariationsActions.DelteVariationSuccessAction({ payload }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.DeleteVariation$ ERROR', errors);
                        return of([
                            fromVariationsActions.DelteVariationFailAction({ errors }),
                            fromVariationsActions.ClearVariationsSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    // --------------------------- IMAGES: ---------------------------------
    LoadImages$ = createEffect(
        () => this.actions.pipe(
            ofType(fromImagesActions.LoadImagesBeginAction),
            switchMap(() => {
                return this.service.loadSiteImages().pipe(
                    flatMap(payload => [
                        fromImagesActions.LoadImagesSuccessAction({ payload }),
                        fromImagesActions.ClearImagesSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadImages$ ERROR', errors);
                        return of([
                            fromImagesActions.LoadImagesFailAction({ errors }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    LoadImageById$ = createEffect(
        () => this.actions.pipe(
            ofType(fromImagesActions.LoadImageByIdBeginAction),
            switchMap((action: any) => {
                return this.service.loadSiteImageById(action.id).pipe(
                    flatMap(payload => [
                        fromImagesActions.LoadImageByIdSuccessAction({ payload }),
                        fromImagesActions.ClearImagesSuccessErrorDataAction()
                    ]),
                    catchError(errors => {
                        console.error('StoreEffects.LoadImageById$ ERROR', errors);
                        return of([
                            fromImagesActions.LoadImageByIdFailAction({ errors }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    CreateImage$ = createEffect(
        () => this.actions.pipe(
            ofType(fromImagesActions.CreateImageBeginAction),
            withLatestFrom(this.storePageStore.ImagesData$),
            switchMap(([action, imagesData]: [any, ImageModel[]]) => {
                return this.service.createSiteImage((action as any).payload).pipe(
                    flatMap(data => {
                        const payload = [data, ...imagesData];
                        return [
                            fromImagesActions.CreateImageSuccessAction({ payload }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.CreateImage$ ERROR', errors);
                        return of([
                            fromImagesActions.CreateImageFailAction({ errors }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    DeleteImage$ = createEffect(
        () => this.actions.pipe(
            ofType(fromImagesActions.DeleteImageBeginAction),
            withLatestFrom(this.storePageStore.ImagesData$),
            switchMap(([action, imagesData]: [any, ImageModel[]]) => {
                return this.service.deleteSiteImage(action.id).pipe(
                    flatMap(() => {
                        const payload = imagesData.filter(i => i.id !== action.id);
                        return [
                            fromImagesActions.DeleteImageSuccessAction({ payload }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ];
                    }),
                    catchError(errors => {
                        console.error('StoreEffects.DeleteImage$ ERROR', errors);
                        return of([
                            fromImagesActions.DeleteImageFailAction({ errors }),
                            fromImagesActions.ClearImagesSuccessErrorDataAction()
                        ]).pipe(switchMap(d => d));
                    })
                );
            })
        )
    );

    constructor(
        private actions: Actions,
        private storePageStore: StorePageStore,
        @Inject(STORE_SERVICE) private service: IStoreService
    ) { }
}
