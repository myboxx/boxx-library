import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import {
    IStoreCategoriesStateSuccess,
    IStoreCategoriesStateError,
    IStoreOrdersStateSuccess,
    IStoreOrdersStateError,
    IStoreProductsStateError,
    IStoreProductsStateSuccess,
    StoreCategoriesStateType,
    StoreOrdersStateType,
    StoreProductsStateType,
    IStoreVariationsStateError,
    IStoreVariationsStateSuccess,
    StoreVariationsStateType,
    StoreImagesStateType,
    IStoreImagesStateError,
    IStoreImagesStateSuccess,
} from '../core/IStateErrorSuccess';
import { CategoryModel } from '../models/Category.model';
import { ImageModel } from '../models/Image.model';
import { OrderModel } from '../models/Order.model';
import { ProductModel } from '../models/Product.model';
import { VariationModel } from '../models/Variation.model';
import * as fromCategoriesActions from './store-categories.actions';
import * as fromOrdersActions from './store-orders.actions';
import * as fromProductsActions from './store-products.actions';
import * as fromVariationsActions from './store-variations.actions';
import * as fromImagesActions from './store-images.action';

export interface IStoreState {
    categories: {
        isLoading: boolean;
        data: CategoryModel[];
        selectedCategory: CategoryModel;
        hasBeenFetched: boolean;
        error: IStoreCategoriesStateError;
        success: IStoreCategoriesStateSuccess;
    };
    orders: {
        isLoading: boolean;
        data: OrderModel[];
        selectedOrder: OrderModel;
        hasBeenFetched: boolean;
        error: IStoreOrdersStateError;
        success: IStoreOrdersStateSuccess;
    };
    products: {
        isLoading: boolean;
        data: ProductModel[];
        selectedProduct: ProductModel;
        hasBeenFetched: boolean;
        error: IStoreProductsStateError;
        success: IStoreProductsStateSuccess;
    };
    variations: {
        isLoading: boolean;
        data: VariationModel[];
        selectedVariation: VariationModel;
        hasBeenFetched: boolean;
        error: IStoreVariationsStateError;
        success: IStoreVariationsStateSuccess;
    };
    images: {
        isLoading: boolean;
        data: ImageModel[];
        selectedImage: ImageModel;
        hasBeenFetched: boolean;
        error: IStoreImagesStateError;
        success: IStoreImagesStateSuccess;
    };
}

export const initialState: IStoreState = {
    categories: {
        isLoading: false,
        data: [],
        selectedCategory: null,
        hasBeenFetched: false,
        error: null,
        success: null,
    },
    orders: {
        isLoading: false,
        data: [],
        selectedOrder: null,
        hasBeenFetched: false,
        error: null,
        success: null,
    },
    products: {
        isLoading: false,
        data: [],
        selectedProduct: null,
        hasBeenFetched: false,
        error: null,
        success: null,
    },
    variations: {
        isLoading: false,
        data: [],
        selectedVariation: null,
        hasBeenFetched: false,
        error: null,
        success: null,
    },
    images: {
        isLoading: false,
        data: [],
        selectedImage: null,
        hasBeenFetched: false,
        error: null,
        success: null,
    },
};

const reducer: ActionReducer<IStoreState> = createReducer(
    initialState,

    // ------------------ CATEGORIES: ---------------------
    on(
        fromCategoriesActions.LoadCategoriesBeginAction,
        fromCategoriesActions.LoadCategorieByIdBeginAction,
        fromCategoriesActions.CreateCategoryBeginAction,
        fromCategoriesActions.DeleteCategoryBeginAction,
        (state, action): IStoreState => ({
            ...state,
            categories: {
                ...state.categories,
                isLoading: true,
                error: null,
                success: null,
            },
        })
    ),
    on(
        fromCategoriesActions.LoadCategoriesSuccessAction,
        fromCategoriesActions.CreateCategorySuccessAction,
        fromCategoriesActions.DeleteCategorySuccessAction,
        (state, action): IStoreState => ({
            ...state,
            categories: {
                ...state.categories,
                isLoading: false,
                hasBeenFetched: true,
                data: action.payload,
                error: null,
                success: { after: getStoreCategoriesActionType(action.type) },
            },
        })
    ),
    on(
        fromCategoriesActions.LoadCategorieByIdSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            categories: {
                ...state.categories,
                isLoading: false,
                hasBeenFetched: true,
                selectedCategory: action.payload,
                error: null,
                success: { after: getStoreCategoriesActionType(action.type) },
            },
        })
    ),
    on(
        fromCategoriesActions.LoadCategoriesFailAction,
        fromCategoriesActions.LoadCategorieByIdFailAction,
        fromCategoriesActions.CreateCategoryFailAction,
        fromCategoriesActions.DeleteCategoryFailAction,
        (state, { type, errors }): IStoreState => ({
            ...state,
            categories: {
                ...state.categories,
                isLoading: false,
                error: {
                    after: getStoreCategoriesActionType(type),
                    error: errors,
                },
            },
        })
    ),
    on(
        fromCategoriesActions.ClearCategoriesSuccessErrorDataAction,
        (state) => ({
            ...state,
            categories: {
                ...state.categories,
                error: null,
                success: null,
            },
        })
    ),

    // ------------------ ORDERS: ---------------------
    on(
        fromOrdersActions.LoadOrdersBeginAction,
        fromOrdersActions.LoadOrderByIdBeginAction,
        fromOrdersActions.UpdateOrderBeginAction,
        (state, action): IStoreState => ({
            ...state,
            orders: {
                ...state.orders,
                isLoading: true,
                error: null,
                success: null,
            },
        })
    ),
    on(
        fromOrdersActions.LoadOrdersSuccessAction,
        fromOrdersActions.UpdateOrderSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            orders: {
                ...state.orders,
                isLoading: false,
                hasBeenFetched: true,
                data: action.payload,
                error: null,
                success: { after: getStoreOrdersActionType(action.type) },
            },
        })
    ),
    on(
        fromOrdersActions.LoadOrderByIdSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            orders: {
                ...state.orders,
                isLoading: false,
                hasBeenFetched: true,
                selectedOrder: action.payload,
                error: null,
                success: { after: getStoreOrdersActionType(action.type) },
            },
        })
    ),
    on(
        fromOrdersActions.LoadOrdersFailAction,
        fromOrdersActions.LoadOrdersFailAction,
        fromOrdersActions.UpdateOrderFailAction,
        (state, action): IStoreState => ({
            ...state,
            orders: {
                ...state.orders,
                isLoading: false,
                error: {
                    after: getStoreOrdersActionType(action.type),
                    error: action.errors,
                },
                success: null,
            },
        })
    ),
    on(fromOrdersActions.ClearOrdersSuccessErrorDataAction, (state) => ({
        ...state,
        orders: {
            ...state.orders,
            error: null,
            success: null,
        },
    })),

    // ------------------ PRODUCTS: ---------------------
    on(
        fromProductsActions.LoadProductsBeginAction,
        fromProductsActions.LoadProductByIdBeginAction,
        fromProductsActions.CreateProductBeginAction,
        fromProductsActions.UpdateProductBeginAction,
        fromProductsActions.DeleteProductBeginAction,
        (state, action): IStoreState => ({
            ...state,
            products: {
                ...state.products,
                isLoading: true,
                error: null,
                success: null,
            },
        })
    ),
    on(
        fromProductsActions.LoadProductsSuccessAction,
        fromProductsActions.CreateProductSuccessAction,
        fromProductsActions.UpdateProductSuccessAction,
        fromProductsActions.DeleteProductSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            products: {
                ...state.products,
                isLoading: false,
                hasBeenFetched: true,
                data: action.payload,
                error: null,
                success: { after: getStoreProductsActionType(action.type) },
            },
        })
    ),
    on(
        fromProductsActions.LoadProductByIdSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            products: {
                ...state.products,
                isLoading: false,
                selectedProduct: action.payload,
                error: null,
                success: { after: getStoreProductsActionType(action.type) },
            },
        })
    ),
    on(
        fromProductsActions.LoadProductsFailAction,
        fromProductsActions.LoadProductByIdFailAction,
        fromProductsActions.CreateProductFailAction,
        fromProductsActions.UpdateProductFailAction,
        fromProductsActions.DeleteProductFailAction,
        (state, action): IStoreState => ({
            ...state,
            products: {
                ...state.products,
                isLoading: false,
                error: {
                    after: getStoreProductsActionType(action.type),
                    error: action.errors,
                },
                success: null,
            },
        })
    ),
    on(fromProductsActions.ClearProductsSuccessErrorDataAction, (state) => ({
        ...state,
        products: {
            ...state.products,
            error: null,
            success: null,
        },
    })),

    // ------------------ VARIATIONS: ---------------------
    on(
        fromVariationsActions.LoadVariationsBeginAction,
        fromVariationsActions.LoadVariationByIdBeginAction,
        fromVariationsActions.CreteVariationBeginAction,
        fromVariationsActions.DelteVariationBeginAction,
        (state, action): IStoreState => ({
            ...state,
            variations: {
                ...state.variations,
                isLoading: true,
                error: null,
                success: null,
            },
        })
    ),
    on(
        fromVariationsActions.LoadVariationsSuccessAction,
        fromVariationsActions.CreteVariationSuccessAction,
        fromVariationsActions.DelteVariationSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            variations: {
                ...state.variations,
                isLoading: false,
                hasBeenFetched: true,
                data: action.payload,
                error: null,
                success: { after: getStoreVariationsActionType(action.type) },
            },
        })
    ),
    on(
        fromVariationsActions.LoadVariationByIdSuccessAction,
        (state, action) => ({
            ...state,
            variations: {
                ...state.variations,
                isLoading: false,
                hasBeenFetched: true,
                selectedVariation: action.payload,
                error: null,
                success: { after: getStoreVariationsActionType(action.type) },
            },
        })
    ),
    on(
        fromVariationsActions.LoadVariationsFailAction,
        fromVariationsActions.LoadVariationByIdFailAction,
        fromVariationsActions.CreteVariataionFailAction,
        fromVariationsActions.DelteVariationFailAction,
        (state, action): IStoreState => ({
            ...state,
            variations: {
                ...state.variations,
                isLoading: false,
                error: {
                    after: getStoreVariationsActionType(action.type),
                    error: action.errors,
                },
                success: null,
            },
        })
    ),
    on(
        fromVariationsActions.ClearVariationsSuccessErrorDataAction,
        (state) => ({
            ...state,
            variations: {
                ...state.variations,
                error: null,
                success: null,
            },
        })
    ),

    // ------------------ IMAGES: ---------------------
    on(
        fromImagesActions.LoadImagesBeginAction,
        fromImagesActions.LoadImageByIdBeginAction,
        fromImagesActions.CreateImageBeginAction,
        fromImagesActions.DeleteImageBeginAction,
        (state, action): IStoreState => ({
            ...state,
            images: {
                ...state.images,
                isLoading: true,
                error: null,
                success: null,
            },
        })
    ),
    on(
        fromImagesActions.LoadImagesSuccessAction,
        fromImagesActions.CreateImageSuccessAction,
        fromImagesActions.DeleteImageSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            images: {
                ...state.images,
                isLoading: false,
                hasBeenFetched: true,
                data: action.payload,
                error: null,
                success: { after: getStoreImagesActionType(action.type) },
            },
        })
    ),
    on(
        fromImagesActions.LoadImageByIdSuccessAction,
        (state, action): IStoreState => ({
            ...state,
            images: {
                ...state.images,
                isLoading: false,
                selectedImage: action.payload,
                error: null,
                success: { after: getStoreImagesActionType(action.type) },
            },
        })
    ),
    on(
        fromImagesActions.LoadImagesFailAction,
        fromImagesActions.LoadImageByIdFailAction,
        fromImagesActions.CreateImageFailAction,
        fromImagesActions.DeleteImageFailAction,
        (state, action): IStoreState => ({
            ...state,
            images: {
                ...state.images,
                isLoading: false,
                error: {
                    after: getStoreImagesActionType(action.type),
                    error: action.errors,
                },
                success: null,
            },
        })
    ),
    on(fromImagesActions.ClearImagesSuccessErrorDataAction, (state) => ({
        ...state,
        images: {
            ...state.images,
            error: null,
            success: null,
        },
    }))
);

function getStoreCategoriesActionType(
    type: fromCategoriesActions.StoreCategoriesActionTypes
) {
    let action: StoreCategoriesStateType;

    switch (type) {
        case fromCategoriesActions.StoreCategoriesActionTypes
            .LoadCategoriesSuccess:
        case fromCategoriesActions.StoreCategoriesActionTypes
            .LoadCategoriesFail:
            action = 'LOAD_CATEGORIES';
            break;
        case fromCategoriesActions.StoreCategoriesActionTypes
            .CreateCategorySuccess:
        case fromCategoriesActions.StoreCategoriesActionTypes
            .CreateCategoryFail:
            action = 'CREATE_CATEGORY';
            break;
        case fromCategoriesActions.StoreCategoriesActionTypes
            .DeleteCategorySuccess:
        case fromCategoriesActions.StoreCategoriesActionTypes
            .DeleteCategoryFail:
            action = 'DELETE_CATEGORY';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

function getStoreOrdersActionType(
    type: fromOrdersActions.StoreOrdersActionTypes
) {
    let action: StoreOrdersStateType;

    switch (type) {
        case fromOrdersActions.StoreOrdersActionTypes.LoadOrdersSuccess:
        case fromOrdersActions.StoreOrdersActionTypes.LoadOrdersFail:
            action = 'LOAD_ORDERS';
            break;
        case fromOrdersActions.StoreOrdersActionTypes.UpdateOrderSuccess:
        case fromOrdersActions.StoreOrdersActionTypes.UpdateOrderFail:
            action = 'UPDATE_ORDER';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

function getStoreProductsActionType(
    type: fromProductsActions.StoreProductsActionTypes
) {
    let action: StoreProductsStateType;

    switch (type) {
        case fromProductsActions.StoreProductsActionTypes.LoadProductsSuccess:
        case fromProductsActions.StoreProductsActionTypes.LoadProductsFail:
            action = 'LOAD_PRODUCTS';
            break;
        case fromProductsActions.StoreProductsActionTypes.CreateProductSuccess:
        case fromProductsActions.StoreProductsActionTypes.CreateProductFail:
            action = 'CREATE_PRODUCT';
            break;
        case fromProductsActions.StoreProductsActionTypes.UpdateProductSuccess:
        case fromProductsActions.StoreProductsActionTypes.UpdateProductFail:
            action = 'UPDATE_PRODUCT';
            break;
        case fromProductsActions.StoreProductsActionTypes.DeleteProductSuccess:
        case fromProductsActions.StoreProductsActionTypes.DeleteProductFail:
            action = 'DELETE_PRODUCT';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

function getStoreVariationsActionType(
    type: fromVariationsActions.StoreVariationsActionTypes
) {
    let action: StoreVariationsStateType;

    switch (type) {
        case fromVariationsActions.StoreVariationsActionTypes
            .LoadVariationsSuccess:
        case fromVariationsActions.StoreVariationsActionTypes
            .LoadVariationsFail:
            action = 'LOAD_VARIATIONS';
            break;
        case fromVariationsActions.StoreVariationsActionTypes
            .CreateVariationSuccess:
        case fromVariationsActions.StoreVariationsActionTypes
            .CreateVariationFail:
            action = 'CREATE_VARIATION';
            break;
        case fromVariationsActions.StoreVariationsActionTypes
            .DeleteVariationSuccess:
        case fromVariationsActions.StoreVariationsActionTypes
            .DeleteVariationFail:
            action = 'DELETE_VARIATION';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

function getStoreImagesActionType(
    type: fromImagesActions.StoreImagesActionTypes
) {
    let action: StoreImagesStateType;

    switch (type) {
        case fromImagesActions.StoreImagesActionTypes.LoadImagesSuccess:
        case fromImagesActions.StoreImagesActionTypes.LoadImagesFail:
        case fromImagesActions.StoreImagesActionTypes.LoadImageByIdSuccess:
        case fromImagesActions.StoreImagesActionTypes.LoadImageByIdFail:
            action = 'LOAD_IMAGES';
            break;
        case fromImagesActions.StoreImagesActionTypes.CreateImageBegin:
        case fromImagesActions.StoreImagesActionTypes.CreateImageFail:
            action = 'CREATE_IMAGE';
            break;
        case fromImagesActions.StoreImagesActionTypes.DelteImageSuccess:
        case fromImagesActions.StoreImagesActionTypes.DelteImageFail:
            action = 'DELETE_IMAGE';
            break;
        default:
            action = 'UNKNOWN';
    }

    return action;
}

export function storeReducer(state: IStoreState | undefined, action: Action) {
    return reducer(state, action);
}

export interface StoreState {
    store: IStoreState;
}
