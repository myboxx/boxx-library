import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReducer from './store.reducer';

export const getStoreState = createFeatureSelector<fromReducer.IStoreState>('store');

// ------- STORE GLOBAL STATE:
export const getStorePageState = createSelector(getStoreState, state => state);

// --------------------- CATEGORIES: -------------------------
export const getStoreCategoriesState = createSelector(getStorePageState, state => state.categories);
export const getCategoriesIsloading = createSelector(getStoreCategoriesState, categories => categories.isLoading);
export const getCategoriesData = createSelector(getStoreCategoriesState, categories => categories.data);
export const getCategoriesHasBeenFetched = createSelector(getStoreCategoriesState, categories => categories.hasBeenFetched);
export const getCategoriesError = createSelector(getStoreCategoriesState, categories => categories.error);
export const getCategoriesSuccess = createSelector(getStoreCategoriesState, categories => categories.success);

// --------------------- ORDERS: -------------------------
export const getStoreOrdersState = createSelector(getStorePageState, state => state.orders);
export const getOrdersIsLoading = createSelector(getStoreOrdersState, orders => orders.isLoading);
export const getOrdersData = createSelector(getStoreOrdersState, orders => orders.data);
export const getOrdersHasBeenFetched = createSelector(getStoreOrdersState, orders => orders.hasBeenFetched);
export const getOrdersError = createSelector(getStoreOrdersState, orders => orders.error);
export const getOrdersSuccess = createSelector(getStoreOrdersState, orders => orders.success);

// --------------------- PRODUCTS: -------------------------
export const getStoreProductsState = createSelector(getStorePageState, state => state.products);
export const getStoreProductsIsLoading = createSelector(getStoreProductsState, products => products.isLoading);
export const getStoreProductsData = createSelector(getStoreProductsState, products => products.data);
export const getStoreProductsHasBeenFetched = createSelector(getStoreProductsState, products => products.hasBeenFetched);
export const getStoreProductsError = createSelector(getStoreProductsState, products => products.error);
export const getStoreProductsSuccess = createSelector(getStoreProductsState, products => products.success);


// --------------------- VARIATIONS: -------------------------
export const getStoreVariationsState = createSelector(getStorePageState, state => state.variations);
export const getStoreVariationsIsLoading = createSelector(getStoreVariationsState, variations => variations.isLoading);
export const getStoreVariationsData = createSelector(getStoreVariationsState, variations => variations.data);
export const getStoreVariationsHasBeenFetched = createSelector(getStoreVariationsState, variations => variations.hasBeenFetched);
export const getStoreVariationsError = createSelector(getStoreVariationsState, variations => variations.error);
export const getStoreVariationsSuccess = createSelector(getStoreVariationsState, variations => variations.success);

// --------------------- IMAGES: -------------------------
export const getStoreImagesState = createSelector(getStorePageState, state => state.images);
export const getStoreImagesIsLoading = createSelector(getStoreImagesState, images => images.isLoading);
export const getStoreImagesData = createSelector(getStoreImagesState, images => images.data);
export const getStoreImagesHasBeenFetched = createSelector(getStoreImagesState, images => images.hasBeenFetched);
export const getStoreImagesError = createSelector(getStoreImagesState, images => images.error);
export const getStoreImagesSuccess = createSelector(getStoreImagesState, images => images.success);
