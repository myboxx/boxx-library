import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
    ICreateVariationPayload,
    IUpsertCategoryPayload,
    IUpsertProductPayload,
} from '../repositories/IStore.api';
import * as fromCategoriesActions from './store-categories.actions';
import * as fromOrdersActions from './store-orders.actions';
import * as fromProductsActions from './store-products.actions';
import * as fromImagesActions from './store-images.action';
import * as fromVariationsActions from './store-variations.actions';
import * as fromReducer from './store.reducer';
import * as fromSelectors from './store.selectors';

@Injectable()
export class StorePageStore {
    constructor(public store: Store<fromReducer.IStoreState>) {}

    // ----------- CATEGORIES METHODS:
    LoadCategories = () =>
        this.store.dispatch(fromCategoriesActions.LoadCategoriesBeginAction());
    loadCategoryById = (id: number) =>
        this.store.dispatch(
            fromCategoriesActions.LoadCategorieByIdBeginAction({ id })
        );
    CreateCategory = (payload: IUpsertCategoryPayload) =>
        this.store.dispatch(
            fromCategoriesActions.CreateCategoryBeginAction({ payload })
        );
    DeleteCategory = (id: number) =>
        this.store.dispatch(
            fromCategoriesActions.DeleteCategoryBeginAction({ id })
        );

    get IsLoadingCategories$() {
        return this.store.select(fromSelectors.getCategoriesIsloading);
    }
    get CategoriesData$() {
        return this.store.select(fromSelectors.getCategoriesData);
    }
    get CategoriesHasBeenFetched$() {
        return this.store.select(fromSelectors.getCategoriesHasBeenFetched);
    }
    get CategoriesError$() {
        return this.store.select(fromSelectors.getCategoriesError);
    }
    get CategoriesSuccess$() {
        return this.store.select(fromSelectors.getCategoriesSuccess);
    }

    // ----------- ORDERS METHODS:
    LoadOrders = () =>
        this.store.dispatch(fromOrdersActions.LoadOrdersBeginAction());
    loadOrderById = (id: number) =>
        this.store.dispatch(fromOrdersActions.LoadOrderByIdBeginAction({ id }));
    updateOrder = (id: number, payload: any) =>
        this.store.dispatch(
            fromOrdersActions.UpdateOrderBeginAction({ id, payload })
        );

    get IsLoadingOrders$() {
        return this.store.select(fromSelectors.getOrdersIsLoading);
    }
    get OrdersData$() {
        return this.store.select(fromSelectors.getOrdersData);
    }
    get OrdersHasBeenFetched$() {
        return this.store.select(fromSelectors.getOrdersHasBeenFetched);
    }
    get OrdersError$() {
        return this.store.select(fromSelectors.getOrdersError);
    }
    get OrdersSuccess$() {
        return this.store.select(fromSelectors.getOrdersSuccess);
    }

    // ----------- PRODUCTS METHODS:
    LoadProducts = () =>
        this.store.dispatch(fromProductsActions.LoadProductsBeginAction());
    CreateProduct = (payload: IUpsertProductPayload) =>
        this.store.dispatch(
            fromProductsActions.CreateProductBeginAction({ payload })
        );
    UpdateProduct = (id: number, payload: IUpsertProductPayload) =>
        this.store.dispatch(
            fromProductsActions.UpdateProductBeginAction({ id, payload })
        );
    DeleteProduct = (id: number) =>
        this.store.dispatch(
            fromProductsActions.DeleteProductBeginAction({ id })
        );

    get IsLoadingProducts$() {
        return this.store.select(fromSelectors.getStoreProductsIsLoading);
    }
    get ProductsData$() {
        return this.store.select(fromSelectors.getStoreProductsData);
    }
    get ProductsHasBeenFetched$() {
        return this.store.select(fromSelectors.getStoreProductsHasBeenFetched);
    }
    get ProductsError$() {
        return this.store.select(fromSelectors.getStoreProductsError);
    }
    get ProductsSuccess$() {
        return this.store.select(fromSelectors.getStoreProductsSuccess);
    }

    // ----------- VARIATIONS METHODS:
    LoadVariations = () =>
        this.store.dispatch(fromVariationsActions.LoadVariationsBeginAction());
    CreateVariation = (payload: ICreateVariationPayload) =>
        this.store.dispatch(
            fromVariationsActions.CreteVariationBeginAction({ payload })
        );
    DeleteVariation = (payload: number) =>
        this.store.dispatch(
            fromVariationsActions.DelteVariationBeginAction({ payload })
        );

    get IsLoadingVariations$() {
        return this.store.select(fromSelectors.getStoreVariationsIsLoading);
    }
    get VariationsData$() {
        return this.store.select(fromSelectors.getStoreVariationsData);
    }
    get VariationsHasBeenFetched$() {
        return this.store.select(
            fromSelectors.getStoreVariationsHasBeenFetched
        );
    }
    get VariationsError$() {
        return this.store.select(fromSelectors.getStoreVariationsError);
    }
    get VariationsSuccess$() {
        return this.store.select(fromSelectors.getStoreVariationsSuccess);
    }

    // ----------- IMAGE METHODS:
    LoadImages = () =>
        this.store.dispatch(fromImagesActions.LoadImagesBeginAction());
    LoadImageById = (id: number) =>
        this.store.dispatch(fromImagesActions.LoadImageByIdBeginAction({ id }));
    CreateImage = (payload: any) =>
        this.store.dispatch(
            fromImagesActions.CreateImageBeginAction({ payload })
        );
    DeleteImage = (id: number) =>
        this.store.dispatch(fromImagesActions.DeleteImageBeginAction({ id }));

    get IsLoadingImages$() {
        return this.store.select(fromSelectors.getStoreImagesIsLoading);
    }
    get ImagesData$() {
        return this.store.select(fromSelectors.getStoreImagesData);
    }
    get ImagesHasBeenFetched$() {
        return this.store.select(fromSelectors.getStoreImagesHasBeenFetched);
    }
    get ImagesError$() {
        return this.store.select(fromSelectors.getStoreImagesError);
    }
    get ImagesSuccess$() {
        return this.store.select(fromSelectors.getStoreImagesSuccess);
    }
}
