import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { STORE_REPOSITORY } from './repositories/IStore.repository';
import { StoreRepository } from './repositories/store.repository';
import { STORE_SERVICE } from './services/IStore.service';
import { StoreService } from './services/store.service';
import { StoreEffects } from './state/store.effects';
import { storeReducer } from './state/store.reducer';
import { StorePageStore } from './state/store.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('store', storeReducer),
        EffectsModule.forFeature([StoreEffects]),
    ],
    exports: []
})
export class StoreCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<StoreCoreModule> {
        return {
            ngModule: StoreCoreModule,
            providers: [
                { provide: STORE_SERVICE, useClass: StoreService },
                { provide: STORE_REPOSITORY, useClass: StoreRepository },
                ...config.providers,
                StorePageStore
            ]
        };
    }
}
