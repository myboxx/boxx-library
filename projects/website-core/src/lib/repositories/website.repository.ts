import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
import {
    IBusinessFormProps,
    IDeleteImageFormProps,
    IDeleteImageResponse,
    IWebsiteApiProps,
    IWebsiteDataProps,
    IWebsiteFormProps,
    IWebsiteRepository
} from './IWebsite.repository';


@Injectable()
export class WebsiteRepository implements IWebsiteRepository {

    constructor(
        @Inject(APP_CONFIG_SERVICE) private appSettings: AbstractAppConfigService,
        private httpClient: HttpClient,
    ) { }

    deleteImage(payload: IDeleteImageFormProps/*website_id: string, container: string, index: string, url_img: string*/) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();

        return this.httpClient.post<IHttpBasicResponse<IDeleteImageResponse>>(`${this.getBaseUrl()}/delete_image`, body);
    }

    getWebsiteData(): Observable<IHttpBasicResponse<Array<IWebsiteApiProps>>> {
        return this.httpClient.get<IHttpBasicResponse<Array<IWebsiteApiProps>>>(`${this.getBaseUrl()}/sites`);
    }

    updateWebsiteData(siteId: number, payload: IWebsiteFormProps): Observable<IHttpBasicResponse<IWebsiteDataProps>> {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();

        return this.httpClient.post<IHttpBasicResponse<IWebsiteDataProps>>(`${this.getBaseUrl()}/update_site/${siteId}`, body);
    }

    updateBusinessData(siteId: number, payload: IBusinessFormProps) {

        const urlSearchParams = new URLSearchParams();

        Object.keys(payload).forEach((key: string) => {
            (typeof payload[key] === 'object') ?
                urlSearchParams.append(key, JSON.stringify(payload[key]))
                :
                urlSearchParams.append(key, payload[key]);
        });

        const body = urlSearchParams.toString();

        return this.httpClient.post<IHttpBasicResponse<IWebsiteDataProps>>(`${this.getBaseUrl()}/update_business_info/${siteId}`, body);
    }

    getUploadImagesUrl() {
        return this.getBaseUrl() + '/upload_update_images';
    }

    uploadImage(fileToUpload: File, config: any) {

        const formData: FormData = new FormData();
        formData.append('img', fileToUpload, fileToUpload.name.replace('jpeg', 'jpg'));
        Object.keys(config).forEach(key => {
            formData.append(key, config[key]);
        });

        return this.httpClient.post<IHttpBasicResponse<IWebsiteDataProps>>(
            `${this.getUploadImagesUrl()}`,
            formData,
            {
                reportProgress: true,
                observe: 'events'
            }
        );
    }

    public getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/website`;
    }
}
