import { HttpEventType } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { WebsitePageModel } from '../models/website.model';
import { IBusinessFormProps, IDeleteImageFormProps, IWebsiteFormProps, IWebsiteRepository, WEBSITE_REPOSITORY } from '../repositories/IWebsite.repository';
import { IWebsiteService } from './IWebsite.service';

@Injectable({
    providedIn: 'root'
})
export class WebsiteService implements IWebsiteService {
    constructor(
        @Inject(WEBSITE_REPOSITORY) private repository: IWebsiteRepository
    ) { }
    deleteImage(payload: IDeleteImageFormProps) {
        return this.repository.deleteImage(payload).pipe(
            map((response) => {
                return response.data;
            }),
            catchError(error => {
                throw error;
            })
        );
    }

    getWebsiteData(): Observable<WebsitePageModel> {
        return this.repository.getWebsiteData().pipe(
            map((response) => {
                return WebsitePageModel.fromApiResponse(response.data);
            }),
            catchError(error => {
                throw error;
            })
        );
    }

    updateWebsiteData(siteId: number, payload: IWebsiteFormProps): Observable<WebsitePageModel> {
        return this.repository.updateWebsiteData(siteId, payload).pipe(
            map((response) => {
                return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
            }),
            catchError(error => {
                throw error;
            })
        );
    }

    updateBusinessData(siteId: number, payload: IBusinessFormProps): Observable<WebsitePageModel> {
        return this.repository.updateBusinessData(siteId, payload).pipe(
            map((response) => {
                return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
            }),
            catchError(error => {
                throw error;
            })
        );
    }

    uploadFileWebBrowser(
        file: File,
        params: any,
        onProgressEvent: (event: any) => void
    ): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.repository.uploadImage(file, params)
            .subscribe((event) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        if (onProgressEvent) {
                            onProgressEvent(event);
                        }
                        break;
                    case HttpEventType.Response:
                        resolve(true);
                }
            }, error => reject(error));
        });
    }
}
