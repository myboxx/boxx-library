import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { WEBSITE_REPOSITORY } from './repositories/IWebsite.repository';
import { WebsiteRepository } from './repositories/website.repository';
import { WEBSITE_SERVICE } from './services/IWebsite.service';
import { WebsiteService } from './services/website.service';
import { WebsiteEffects } from './state/website.effects';
import { websiteReducer } from './state/website.reducer';
import { WebsiteStore } from './state/website.store';

interface ModuleOptionsInterface {
    providers: Provider[];
}

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    StoreModule.forFeature('website', websiteReducer),
    EffectsModule.forFeature([WebsiteEffects]),
  ],
  exports: []
})
export class WebsiteCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<WebsiteCoreModule> {
        return {
          ngModule: WebsiteCoreModule,
          providers: [
            { provide: WEBSITE_SERVICE, useClass: WebsiteService },
            { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository },
            ...config.providers,
            WebsiteStore
          ]
        };
      }
}
